using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Medicine;
using System;
//Core Systems Libraries
using BigOS.SaveSystem;
using BigOS.Localization;
//Addons Libraries
#if UnityTweety
using BigOS.UnityTweety;
#endif
#if Paparazzi
using BigOS.Paparazzi;
#endif
#if GoogleFairy
using BigOS.GoogleFairy;
#endif

namespace BigOS
{
    [Register.Single]
    public class BigOSChef : MonoBehaviour
    {    
//Core Systems

        [Inject.Single] public SaveSystemCore saveSystemCore {get;}
        [Inject.Single] public LocalizationCore localizationCore {get;}
        //public DiagnosticsScreen diagnosticsScreen;
        public bool AllowSavingAtStartForDebug = false;
        
//Addons
#if UnityTweety
        [Inject.Single] UnityTweetyCore unityTweetyCore {get;}
#endif
#if Paparazzi
        [Inject.Single] PaparazziManager paparazziManager {get;}
#endif
#if GoogleFairy
        [Inject.Single] GoogleFairyCore googleFairyCore {get;}
#endif
        
        void Start(){}
        void Awake(){
             
            saveSystemCore.InitializeSave();    
            saveSystemCore.LoadCurrentSlot();
            if (AllowSavingAtStartForDebug == true) SaveSystemCore.SaveAllowed = true;
        }

//UnityTweety Methods
#if UnityTweety
        public void UnityTweetyCheck(){
            Debug.Log(unityTweetyCore.MethodTest().userName);
        }        
#endif

//Paparazzi Methods
#if Paparazzi

#endif

//SheetsFairy Methods
#if GoogleFairy

#endif


//Editor Methods
        public void CreateExtensionGameObject(string extName){
                GameObject go = new GameObject(extName);
                go.transform.SetParent(this.gameObject.transform);
                switch(extName){
                        case "UnityTweety":
                                if(Type.GetType("BigOS.UnityTweety.UnityTweetyCore, rympow.unitytweety",false)!=null)go.AddComponent(Type.GetType("BigOS.UnityTweety.UnityTweetyCore, rympow.unitytweety",false) as Type);
                                break;
                        case "Paparazzi":
                                if(Type.GetType("BigOS.Paparazzi.PaparazziManager, rympow.paparazzi",false)!=null)go.AddComponent(Type.GetType("BigOS.Paparazzi.PaparazziManager, rympow.paparazzi",false) as Type);
                                break;
                        case "GoogleFairy":
                                if(Type.GetType("BigOS.GoogleFairy.GoogleFairyCore, rympow.googlefairy",false)!=null)go.AddComponent(Type.GetType("BigOS.GoogleFairy.GoogleFairyCore, rympow.googlefairy",false) as Type);
                                break;
                        default:
                                Debug.LogError("Adding extension script error");
                                break;
                }
        }
    }
}
