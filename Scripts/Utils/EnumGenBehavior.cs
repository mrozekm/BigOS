﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class EnumGenBehavior : MonoBehaviour
{
    public abstract void GenerateEnums();
}
