using System.Collections;
using System.Collections.Generic;
//using SaveSystem;
using UnityEngine;
//using SaveSystemBasics;
using System.Reflection;
using System.Linq;
using Newtonsoft.Json;

namespace BigOS.SaveSystem
{
    
    [System.Serializable]
    public class DataContainerGameplay : DataContainer
    {
        // PROJECT SPECIFIC TYPES

        //public List<BotInstanceData> botInstanceData = new List<BotInstanceData>();
        
        
        // Start is called before the first frame update
        public DataContainerGameplay CopyGameplayAndBase()
        {
            Debug.Log("CopyGameplayAndBase");
            DataContainer rawDataContainer = Copy();
            // DOWNCASTING FROM PARENT CLASS
            var json = JsonConvert.SerializeObject(rawDataContainer);
            DataContainerGameplay toReturn = JsonConvert.DeserializeObject<DataContainerGameplay>(json); //rawDataContainer as DataContainerGameplay;
            return toReturn;
            //TODO: ADD GAMEPLAY CODE?
        }

        public void ResolveAll(DataContainerGameplay loadedData)
        {
            //FIRST RESOLVE BASE
            base.Resolve(loadedData as DataContainer);
            
            //SECOND GAME SPECIFIC
            //botInstanceData = ResolveIncomingProjectSpecific(botInstanceData, loadedData.botInstanceData);
        }
        
        /*public List<T> ResolveIncomingProjectSpecific<T>(List<T> home, List<T> incoming)  where T : ProjectSpecificPers
        {
            for (int i = 0; i < home.Count; i++)
            {
                ProjectSpecificPers lookedFor = incoming.FirstOrDefault(p => string.CompareOrdinal(p.key, home[i].key) == 0);

                if (lookedFor != null)
                {
                    switch (lookedFor)
                    {

                        // bool 2D Array
                        case BotInstanceData lookedForProjectSpecific1:
                            (home[i] as BotInstanceData).Resolve(lookedForProjectSpecific1);
                            break;

                        default:
                            int foundIndex = incoming.IndexOf(lookedFor as T);
                            home[i] = incoming[foundIndex];
                            break;
                    }
                }
            }

            return home;
        }*/

        public override void WipeSave()
        {
            base.WipeSave();
            int d = 1;
        }

        public override List<Pers> ReturnAllEntries()
        {
            // TODO COLLECT ALL ENTRIES IN GAMEPLAY VIA REFLECTION
          
            
            /*
            List<Pers> newPers = new List<Pers>();
            
            FieldInfo[] objectFields = this.GetType().GetFields(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
            for (int i = 0; i < objectFields.Length; i++)
            {
                List<Pers> persList = (List<Pers>) objectFields[i].GetValue(this) as List<Pers>;
            }
            
        */
            //----gamespecific
            
            List<Pers> returnPers = new List<Pers>();

            //for (int i = 0; i < botInstanceData.Count; i++)
            //{
            //    returnPers.Add(botInstanceData[i] as Pers);
            //}

            List<Pers> returnPers2 = base.ReturnAllEntries();
            returnPers2.AddRange(returnPers);
            
           return returnPers2;
        }


    }
}

