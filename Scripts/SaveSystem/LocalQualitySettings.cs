﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BigOS.SaveSystem{
    public class LocalQualitySettings : MonoBehaviour
    {
        public static int[] renderTextureSize  = {512,1024,2048,3000 };

        public static int ConvertFloatQualityToIntQuality(float floatQuality)
        {
            int toInt = (int)(floatQuality * 4f);
            toInt = Mathf.Clamp(toInt, 0, renderTextureSize.Length - 1);
            // also set texture quality
            //QualitySettings.currentLevel.


            return toInt;
        }

        public static int GetCurrentResolution()
        {
            int resNumber = ConvertFloatQualityToIntQuality(PersistenceManager.GetFloat("Quality"));

            return renderTextureSize[Mathf.Clamp(  resNumber,0, renderTextureSize.Length-1)];
        }
    }
}