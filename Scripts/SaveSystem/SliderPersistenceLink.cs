﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using BigOS.Localization;

namespace BigOS.SaveSystem{
    public class SliderPersistenceLink : MonoBehaviour
    {
        public enum DisplayType {None, Percent, Quality };
        public enum Datatype { Int, Float };

        public string entryName;
        public DisplayType displayType;
        //public Datatype datatype;
        [SerializeField] Text textComponent;
        [SerializeField] Slider slider;
        public bool playSoundAtSliderChange = false;
        bool hasStarted = false;


        [Header("Runtime")]
        PersFloat storageManagerLink;

        // Start is called before the first frame update
        void Start()
        {
            Debug.Log("registering Pers in " + this.GetType().ToString() + " " + gameObject.name);
            storageManagerLink = new PersFloat(entryName);
            PersistenceManager.RegisterPers(ref storageManagerLink);
            hasStarted = true;
            slider.value = storageManagerLink.value;
            SliderValueChanged();
        }

    

        // Update is called once per frame
        void Update()
        {
            
        }

        
        [ContextMenu("Refersh")]
        public void SliderValueChanged()
        {
        // if (playSoundAtSliderChange && Time.timeSinceLevelLoad > 1f)
        //  {
                // well this is kinda ugly but maybe noone will notice
        //      AudioSourceCollection.SetVolume(slider.value);
        //       AudioSourceCollection.PlaySound(AbilitySoundEnum.CancelAbilitySound);
        //  }

            if (displayType == DisplayType.Percent)
            {
                textComponent.text = StringUtils.FloatToPercent(slider.value);
            }

            if (displayType == DisplayType.Quality)
            { //TODO FIX
                int qualityInt = LocalQualitySettings.ConvertFloatQualityToIntQuality(storageManagerLink.value);
                string displayString = "XXX";
                if (qualityInt == 0) displayString = VocabularyCore.GetEntry(0, 15); //LocalizationAccess.GetString(LocEnum_UIGeneral.QualitySetting_1);
                else if (qualityInt == 1) displayString =  VocabularyCore.GetEntry(0, 16);// LocalizationAccess.GetString( LocEnum_UIGeneral.QualitySetting_2);
                else if (qualityInt == 2) displayString =  VocabularyCore.GetEntry(0, 17);// LocalizationAccess.GetString( LocEnum_UIGeneral.QualitySetting_3);
                else if (qualityInt == 3) displayString =  VocabularyCore.GetEntry(0, 18); //LocalizationAccess.GetString( LocEnum_UIGeneral.QualitySetting_4);

                textComponent.text = displayString;
            }
            storageManagerLink.value = slider.value;
        }
    }
}