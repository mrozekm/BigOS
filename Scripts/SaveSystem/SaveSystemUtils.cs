using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using System.IO;
using System.Linq;


namespace BigOS.SaveSystem
{
    public class SaveSystemUtils
    {
        static readonly string encryptedKey = "#k_83Dalowakf31(#($%0_+<]:#dDB'a";

        public static void SaveSlot<T>(string path, T currentSlot, bool encrypt)
        {


            if (File.Exists(path))
            {
                File.Delete(path);
            }

            var setting = new JsonSerializerSettings();
            setting.Formatting = Newtonsoft.Json.Formatting.Indented;
            setting.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            var json = JsonConvert.SerializeObject(currentSlot, setting);

            if (encrypt)
            {

                
                Rijndael crypto = new Rijndael();
                byte[] soup = crypto.Encrypt(json, encryptedKey);
                Debug.Log("Saving " + path);
                File.WriteAllBytes(path, soup);
            }
            else
            {

                File.WriteAllText(path, json);
            }
            
        
        }

        public static T LoadSlot<T>(string path, bool encrypt)
        {
            if (!File.Exists(path))
            {
                return  default(T);
            }


            // read

            var setting = new JsonSerializerSettings();
            setting.Formatting = Newtonsoft.Json.Formatting.Indented;
            setting.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;

            if (encrypt)
            {

                byte[] soup = File.ReadAllBytes(path);
                Rijndael crypto = new Rijndael();
                var json = crypto.Decrypt(soup, encryptedKey);
                var fromFile = JsonConvert.DeserializeObject<T>(json);
                if (fromFile == null)
                {
                    Debug.LogError("Save is corrupt");
                    return default(T);
                }

                return fromFile;
            }
            else
            {
                var fileContent = File.ReadAllText(path);
                return JsonConvert.DeserializeObject<T>(fileContent);
            }


        }

        public static string StringListToString(List<string> strings)
        {
            string finalString = "";
            foreach (string s in strings)
            {
                finalString += s + "|";
            }
            return finalString;
        }

        public static List<string> StringToStringList(string soup)
        {
            return soup.Split('|').ToList();
        }
    }
}

