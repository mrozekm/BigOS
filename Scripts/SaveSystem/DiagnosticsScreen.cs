using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Medicine;

namespace BigOS.SaveSystem
{
    [Register.Single]
    public class DiagnosticsScreen : MonoBehaviour
    {
        [SerializeField] GameObject canvas;
        //  [SerializeField] GameObject panel;
        [SerializeField] GameObject entryPrefab;
        [SerializeField] GameObject entryPrefabNumber;
        [SerializeField] GameObject entryPrefabLabel;
        [SerializeField]
        GameObject HeaderPrefab;
        // [SerializeField] List<DiagnosticsEntry> diagEntries;

        [SerializeField] List<Pers> capturedSaveEntries;
        [SerializeField] GameObject contentPanel;

        // [SerializeField] AchievementManager achievementManager;
        [SerializeField] List<bool> expanded = new List<bool>();
        [SerializeField] bool populated;

        [SerializeField]
        List<Pers> allPers;
        PersStringList expandedEntries;
        [SerializeField] AnimationCurve buttonPressCurve;
        [SerializeField]
        float pressCounter;

        int currentSlot;

        // [SerializeField] PersistenceManager persistenceManager;

        // Start is called before the first frame update
        void Start()
        {
            canvas.gameObject.SetActive(false);
            entryPrefab.SetActive(false);
            entryPrefabNumber.SetActive(false);
            entryPrefabLabel.SetActive(false);
            HeaderPrefab.SetActive(false);

        }

        public void PassPers(List<Pers> allPers)
        {
            this.allPers = allPers;
        }

        // Update is called once per frame
        void Update()
        {
            OpenUponHotkey();
        }

        public void OpenUponHotkey()
        {
            if (Input.GetKey(KeyCode.RightShift) || Input.GetKey(KeyCode.LeftShift))
            {
                if (Input.GetKeyDown(KeyCode.F10))
                {
                    ToggleVisibility();
                }
            }


        }

        public void PopulateEntries(List<Pers> allPers)
        {
            // UIHelper.PopulateTable<Pers, DiagnosticsEntry>(RefreshDiagnosticsEntries, allPers, contentPanel.GetComponent<RectTransform>(), entryPrefab, false);

            BIGosUtils.DeleteAllChildren(contentPanel.gameObject);

            entryPrefabNumber.SetActive(false);
            entryPrefab.SetActive(false);
            entryPrefabLabel.SetActive(false);
            int i = 0;
            {
                foreach (Pers pers in allPers)
                {
                    GameObject newOne;

                    switch (pers) //SETUP
                    {
                        case PersInt persintvar:
                            newOne = Instantiate(entryPrefabNumber, contentPanel.transform);
                            newOne.GetComponent<DiagnosticsEntry>().SetIndentation(false) ;
                            break;
                        case PersString perstringvar:
                            newOne = Instantiate(entryPrefab, contentPanel.transform);
                            newOne.GetComponent<DiagnosticsEntry>().SetIndentation(false);
                            break;
                        case PersBool persboolvar:
                            newOne = Instantiate(entryPrefabNumber, contentPanel.transform);
                            newOne.GetComponent<DiagnosticsEntry>().HideExpandButton();
                            newOne.GetComponent<DiagnosticsEntry>().SetIndentation(false);
                            break;
                        case PersIntList persintListvar:
                            newOne = Instantiate(entryPrefabLabel, contentPanel.transform);
                            newOne.GetComponent<DiagnosticsEntry>().SetIndentation(false);


                            for (int j = 0; j < persintListvar.value.Count; j++)
                            {
                                GameObject subEntry = Instantiate(entryPrefabNumber, contentPanel.transform);
                                subEntry.GetComponent<DiagnosticsEntry>().SetIndentation(true);
                                subEntry.name = "sub " + entryPrefab.name + " " + j;
                                subEntry.GetComponent<DiagnosticsEntry>().name = j.ToString();
                                subEntry.SetActive(true);
                                subEntry.GetComponent<RectTransform>().position = Vector3.zero;
                                subEntry.GetComponent<DiagnosticsEntry>().RefreshAndSetInheritanceIndex(pers, j);
                            }

                            break;

                        case PersBoolList persBoolListvar:
                            newOne = Instantiate(entryPrefabLabel, contentPanel.transform);
                            newOne.GetComponent<DiagnosticsEntry>().SetIndentation(false);
                        


                            for (int j = 0; j < persBoolListvar.value.Count; j++)
                            {
                                GameObject subEntry = Instantiate(entryPrefabNumber, contentPanel.transform);
                                subEntry.GetComponent<DiagnosticsEntry>().HideExpandButton();
                                subEntry.GetComponent<DiagnosticsEntry>().SetIndentation(true);
                                subEntry.name = "sub " + entryPrefab.name + " " + j;
                                subEntry.GetComponent<DiagnosticsEntry>().name = j.ToString();
                                subEntry.SetActive(true);
                                subEntry.GetComponent<RectTransform>().position = Vector3.zero;
                                subEntry.GetComponent<DiagnosticsEntry>().RefreshAndSetInheritanceIndex(pers, j);
                            }

                            break;

                        case PersStringList persStringListvar:
                            newOne = Instantiate(entryPrefabLabel, contentPanel.transform);
                            newOne.GetComponent<DiagnosticsEntry>().SetIndentation(false);



                            for (int j = 0; j < persStringListvar.value.Count; j++)
                            {
                                GameObject subEntry = Instantiate(entryPrefabNumber, contentPanel.transform);
                                subEntry.GetComponent<DiagnosticsEntry>().SetIndentation(true);
                                subEntry.name = "sub " + entryPrefab.name + " " + j;
                                subEntry.GetComponent<DiagnosticsEntry>().name = j.ToString();
                            subEntry.SetActive(true);
                                subEntry.GetComponent<RectTransform>().position = Vector3.zero;
                                subEntry.GetComponent<DiagnosticsEntry>().RefreshAndSetInheritanceIndex(pers, j);
                            }

                            break;

                        case PersIntStringList persIntStringListvar:
                            newOne = Instantiate(entryPrefabLabel, contentPanel.transform);
                            newOne.GetComponent<DiagnosticsEntry>().SetIndentation(false);



                            for (int j = 0; j < persIntStringListvar.value.Count; j++)
                            {
                                GameObject subEntry = Instantiate(entryPrefabNumber, contentPanel.transform);
                                subEntry.GetComponent<DiagnosticsEntry>().SetIndentation(true);
                                subEntry.name = "sub " + entryPrefab.name + " " + j;
                                subEntry.GetComponent<DiagnosticsEntry>().name = j.ToString();
                                subEntry.SetActive(true);
                                subEntry.GetComponent<RectTransform>().position = Vector3.zero;
                                subEntry.GetComponent<DiagnosticsEntry>().RefreshAndSetInheritanceIndex(pers, j);
                            }

                            break;


                        default:
                            newOne = Instantiate(entryPrefabNumber, contentPanel.transform);
                            newOne.GetComponent<DiagnosticsEntry>().SetIndentation(false);
                            break;

                    }



                    newOne.GetComponent<RectTransform>().position = Vector3.zero;
                    newOne.name = entryPrefab.name + " " + i;
                    newOne.SetActive(true);
                    i++;

                    DiagnosticsEntry diag = newOne.GetComponent<DiagnosticsEntry>();
                    if (diag)
                    {
                        diag.Refresh(pers);
                        diag.diagScreen = this;
                    }

                    
                }
            }


        }

        //private void RefreshDiagnosticsEntries( Pers saveEntry, DiagnosticsEntry diagEntry)
        // {
            //    diagEntry.Refresh(saveEntry);
        //  }

        public void ToggleVisibility()
        {
            if (!populated)
            {
                populated = true;
                PopulateEntries(allPers);

            }


            canvas.SetActive(!canvas.activeSelf);
        }

        public void ClearAchievements()
        {
        //    achievementManager.ResetAllAchievements();
        }

        public void GiveAllItems()
        {

        }

        public void Pressing()
        {
            pressCounter += Time.deltaTime;
        }

        public void Released()

        {
        // Debug.Log("Released");
            pressCounter = 0;
        }

        public float GetPressedMultiplier()
        {
            return buttonPressCurve.Evaluate( pressCounter);
        }
    }
}
