using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Medicine;
using System.IO;
using System.Linq;
using System.Reflection;
using System;


namespace BigOS.SaveSystem
{
    [Register.Single]
    public class SaveSystemCore : MonoBehaviour
    {
        //BASICS
        public DiagnosticsScreen diagnosticsScreen;

        [HideInInspector] public bool rijndaelEnabled = true;
        [HideInInspector] public string savedInFolder = "Saves";
        public static string NOTIFICATION_KEY = "Notification";
        public static string LANGUAGE_KEY = "Language";
        public static string SAVE_SLOT_KEY = "SaveSlot";
        [HideInInspector] public string tagOfObjectsWithPersistence = "SaveSystem";
        //KEY DATA
        public static bool SaveAllowed = false;
        //[HideInInspector] 
        public SaveInitializeData initializeData;
        [SerializeField] public DataContainerGameplay currentSlot;
        [SerializeField] PersistenceInitializationExtension persistenceInitializationExtension;

        

        public bool GetEncryption()
        {
            return rijndaelEnabled;
        }

        public DataContainerGameplay GetEntireCurrentSlotDataContainer()
        {
            return currentSlot;
        }

        public DataContainerGameplay GetCurrentSlot()
        {
            return currentSlot;
        }

        public void InitializeSave()
        {
             //   Debug.Log("Initializing Save");
            // INITIALIZING MAIN BASE SAVE
            currentSlot = initializeData.dataContainer.CopyGameplayAndBase();
            // INITIALIZING PROJECT SPECIFIC
            if ( persistenceInitializationExtension)
                persistenceInitializationExtension.InitializeExternal(currentSlot);
            // CHEATS
            // diagnosticsScreen.PopulateEntries(currentSlot, currentSlot.ReturnAllEntries());
            diagnosticsScreen.PassPers(currentSlot.ReturnAllEntries());
        }
        

        public void SaveSlot(){
            Debug.Log("SaveSlot");
            if (!SaveAllowed)
            {
                Debug.Log("Save is not allowed just yet");
                return;
            }

            SaveSystemUtils.SaveSlot(GetPath(), currentSlot, rijndaelEnabled);

        }
        public void LoadCurrentSlot(){
            Debug.Log("loading");
            // LOAD FROM FILE
            DataContainerGameplay incoming =
                SaveSystemUtils.LoadSlot<DataContainerGameplay>(GetPath(), rijndaelEnabled);
            // RESOLVE THE TWO
            if (incoming != null) currentSlot.ResolveAll(incoming);
            else
            {
                currentSlot.WipeSave();
                currentSlot.ResolveAll(initializeData.dataContainer);
            }

        }

        IEnumerator LoadCurrentSlotWithDelay()
        {
            yield return new WaitForEndOfFrame();
            LoadCurrentSlot();
        }

        public void ClearSlot(){
            if (File.Exists(GetPath()))
            {
                Debug.Log("file: " + GetPath() + " did exist - deleting it");
                File.Delete(GetPath());
            }
            WipeCurrentSlotAndSoftInitialize();
        }
        
        private void OnApplicationQuit()
        {
            SaveSlot();
        }

        public void WipeCurrentSlotAndSoftInitialize()
        {
            // currentSlot.WipeSave();
            StartCoroutine(WipeSaveAgainJustToBeSure());
            // We need to fill the save with correct data after we deleted it
        }

        IEnumerator WipeSaveAgainJustToBeSure()
        {
            // It doesn't work if not in coroutine
            yield return new WaitForEndOfFrame();
            currentSlot.WipeSave();
            currentSlot.ResolveAll(initializeData.dataContainer);
        }


        public string GetLogPath()
        {
            string readyString = Application.persistentDataPath + "/Player.log";
            return ReturnTheSameFileButWithUnderscore(readyString);
        }
        public string GetPreviousLogPath()
        {
            string readyString = Application.persistentDataPath + "/Player-prev.log";
            return ReturnTheSameFileButWithUnderscore(readyString);
        }
        private static string ReturnTheSameFileButWithUnderscore(string name)
        {
            if (File.Exists(name))
            {
                string underScoreName = name + ".txt";

                if (File.Exists(underScoreName))
                {
                    File.Delete(underScoreName);
                }

                File.Copy(name, underScoreName);
                return underScoreName;
            }
            else return "";
        }


        public string GetPath()
        {
            return GetPath(PlayerPrefs.GetInt(SAVE_SLOT_KEY, 0));
        }

        public string GetPath(int numberOfSlot)
        {
            string fileName = "save";

            if (!Directory.Exists(Application.persistentDataPath + "/" + savedInFolder))
                Directory.CreateDirectory(Application.persistentDataPath + "/" + savedInFolder);

            string path = Application.persistentDataPath + "/" + savedInFolder + "/" + fileName;
            // Debug.Log(" saving: Get SAVE_SLOT_KEY " + numberOfSlot);
            path = path + "_" + (numberOfSlot + 1).ToString();
            // add .sav
            return path + ".sav";
        }

        #region GET / SET Variabled
        //GET
        public int GetInt(string name)
        {
            PersInt source = currentSlot.intList.FirstOrDefault(p => string.CompareOrdinal(p.key, name) == 0);
            if (source != null)
                return source.value;
            else
            {
                Debug.LogError("No such int entry in save: " + name);
                return 0;
            }
        }

        public bool GetBool(string name)
        {
            PersBool source =
                currentSlot.boolList.FirstOrDefault(p => string.CompareOrdinal(p.key, name) == 0);
            if (source != null)
                return source.value;
            else
            {
                Debug.LogError("No such bool entry in save: " + name);
                return false;
            }
        }

        public float GetFloat(string name)
        {
            PersFloat source =
                currentSlot.floatList.FirstOrDefault(p => string.CompareOrdinal(p.key, name) == 0);
            if (source != null)
                return source.value;
            else
            {
                Debug.LogError("No such float entry in save: " + name);
                return 0f;
            }
        }

        public long GetLong(string name)
        {
            PersLong source =
                currentSlot.longList.FirstOrDefault(p => string.CompareOrdinal(p.key, name) == 0);
            if (source != null)
                return source.value;
            else
            {
                Debug.LogError("No such long entry in save: " + name);
                return 0;
            }
        }

        public string GetString(string name)
        {
            PersString source =
                currentSlot.stringList.FirstOrDefault(p => string.CompareOrdinal(p.key, name) == 0);
            if (source != null)
                return source.value;
            else
            {
                Debug.LogError("No such string entry in save: " + name);
                return "";
            }
        }
        //ADD VALUE
        public void AddToInt(string name, int value)
        {
            PersInt target = currentSlot.intList.FirstOrDefault(p => string.CompareOrdinal(p.key, name) == 0);
            if (target != null)
                target.value += value;
            else
            {
                Debug.LogError("No such int entry in save: " + name);
            }
        }
        public void AddToFloat(string name, float value)
        {
            PersFloat target =
                currentSlot.floatList.FirstOrDefault(p => string.CompareOrdinal(p.key, name) == 0);
            if (target != null)
                target.value += value;
            else
            {
                Debug.LogError("No such int entry in save: " + name);
            }
        }
        //SET
        public void SetInt(string name, int value)
        {
            PersInt target = currentSlot.intList.FirstOrDefault(p => string.CompareOrdinal(p.key, name) == 0);
            if (target != null)
                target.value = value;
            else
            {
                Debug.LogError("No such int entry in save: " + name);
            }
        }

        public void SetFloat(string name, float value)
        {
            PersFloat target =
                currentSlot.floatList.FirstOrDefault(p => string.CompareOrdinal(p.key, name) == 0);
            if (target != null)
                target.value = value;
            else
            {
                Debug.LogError("No such float entry in save: " + name);
            }
        }

        public void SetBool(string name, bool value)
        {
            PersBool target =
                currentSlot.boolList.FirstOrDefault(p => string.CompareOrdinal(p.key, name) == 0);
            if (target != null)
                target.value = value;
            else
            {
                Debug.LogError("No such bool entry in save: " + name);
            }
        }

        public void SetLong(string name, long value)
        {
            PersLong target =
                currentSlot.longList.FirstOrDefault(p => string.CompareOrdinal(p.key, name) == 0);
            if (target != null)
                target.value = value;
            else
            {
                Debug.LogError("No such long entry in save: " + name);
            }
        }

        public void SetString(string name, string value)
        {
            PersString target =
                currentSlot.stringList.FirstOrDefault(p => string.CompareOrdinal(p.key, name) == 0);
            if (target != null)
                target.value = value;
            else
            {
                Debug.LogError("No such string entry in save: " + name);
            }
        }
        #endregion
        public void RegPers(MonoBehaviour mono)
        {
            // Debug.Log("PERS PERS");
            FieldInfo[] objectFields = mono.GetType()
                .GetFields(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
            for (int i = 0; i < objectFields.Length; i++)
            {
                Debug.Log("Persistent check: " + objectFields[i].Name); // The name of the flagged variable.
                PersistentAttribute attribute =
                    Attribute.GetCustomAttribute(objectFields[i], typeof(PersistentAttribute)) as PersistentAttribute;
                if (attribute != null)
                {
                    Debug.Log("Making Persistent: " + objectFields[i].Name + " " +
                              (attribute as PersistentAttribute).name); // The name of the flagged variable.
                    //TODO: MAKE IT WORK

                    string name = (attribute as PersistentAttribute).name;

                    // Pers lookedfor = instance.currentSlot.intList.FirstOrDefault(p => string.CompareOrdinal(p.key, (attribute as PersistentAttribute).name) == 0);
                    // lookedfor.key = (attribute as PersistentAttribute).name;
                    // objectFields[i].SetValue(mono, lookedfor);

                    var lookedfor = objectFields[i].GetValue(mono) as Pers;
                    lookedfor.key=name;

                    ReplaceWithSourcePersInDataContainer(ref lookedfor);

                    if (lookedfor != null)
                    {
                        objectFields[i].SetValue(mono, lookedfor);
                    }
                }

                /*
                    TypedReference typedReference = __makeref(lookedfor);
                    //   objectFields[i].SetValueDirect(typedReference,lookedfor);

                    var retrieved = objectFields[i].GetValueDirect(typedReference); // GetValue(mono);
                    //var retrieved = objectFields[i].GetValue(mono);// GetValue(mono);
                    //   Debug.Log(retrieved);
                    Type type = retrieved.GetType();

                    MethodInfo method =
                        typeof(PersistenceManager).GetMethod(nameof(PersistenceManager.RegisterPers));
                    MethodInfo generic = method.MakeGenericMethod(type);

                    // var entTypConfig = generic.Invoke(modelBuilder, null);

                    // WE NEED TO RENAME THE KEY THE RETRIEVED PERS TO THE ATTRIBUTE!

                    Pers toBeRenamed = retrieved as Pers;
                    toBeRenamed.key = (attribute as PersistentAttribute).name;

                    generic.Invoke(instance, new object[] {retrieved});


                    // instance.RegPersNonStatic( objectFields[i], (attribute as PersistentAttribute).name);

                    //workingPers.key = 
                */
            }
        }
        public void RegisterPers<T>(ref T pers)
            where T : Pers
        {
            Debug.Log("registering " + pers.key + " " + pers.GetType().ToString());

            string name = pers.key;
            // if (instance == null) instance = GameObject.FindObjectOfType<PersistenceManager>();
            Pers lookedfor = pers;

            ReplaceWithSourcePersInDataContainer(ref lookedfor);

            if (lookedfor != null)
            {
                pers = lookedfor as T;
            }
        }
        public void ReplaceWithSourcePersInDataContainer<T>(ref T lookedfor)
            where T : Pers // THIS IS PRIVATE FOR A REASON
        {
            string name = (lookedfor as Pers).key;
            //Debug.Log("key: "+name);
            if ((lookedfor as CustomPers)!=null)
            {
                Debug.Log("PROJECT SPECIFIC PERSISTENCE");
                //List<CustomPers> customPers = currentSlot.customPers;

                switch (lookedfor)
                {
                    //case BotInstanceData lookedDataPacket:
                    //    Debug.Log("bot instance save");
                    //    lookedfor = instance.currentSlot.botInstanceData.FirstOrDefault(p =>
                    //        string.CompareOrdinal(p.key, name) == 0) as T;
                    //    // Debug.Log(lookedfor.ToString());
                    //    Debug.Assert(lookedDataPacket != null, name + "dataPacket  not found ");

                    //    break;

                    default:
                        Debug.LogError("That should not be registered in save");
                        break;
                }
            }
            else
            {
                switch (lookedfor)
                {
                    case PersInt lookedPersInt:
                        lookedfor = currentSlot.intList.FirstOrDefault(p =>
                            string.CompareOrdinal(p.key, name) == 0) as T;
                        Debug.Assert(lookedfor != null, name + " int not found ");
                        break;
                    case PersFloat lookedPersFloat:
                        Debug.Log("is float");
                        lookedfor = currentSlot.floatList.FirstOrDefault(p =>
                            string.CompareOrdinal(p.key, name) == 0) as T;
                        Debug.Assert(lookedfor != null, name + " float not found ");
                        break;
                    case PersBool lookedPersBool:
                        lookedfor = currentSlot.boolList.FirstOrDefault(p =>
                            string.CompareOrdinal(p.key, name) == 0) as T;
                        Debug.Assert(lookedfor != null, name + " bool not found ");
                        break;
                    case PersLong lookedPersLong:
                        lookedfor = currentSlot.longList.FirstOrDefault(p =>
                            string.CompareOrdinal(p.key, name) == 0) as T;
                        Debug.Assert(lookedfor != null, name + " long not found ");
                        break;
                    case PersString lookedPersString:
                        lookedfor = currentSlot.stringList.FirstOrDefault(p =>
                            string.CompareOrdinal(p.key, name) == 0) as T;
                        Debug.Assert(lookedfor != null, name + " string not found ");
                        break;
                    case PersIntList lookedPersIntList:
                        lookedfor = currentSlot.intListList.FirstOrDefault(p =>
                            string.CompareOrdinal(p.key, name) == 0) as T;
                        Debug.Assert(lookedfor != null, name + " int list not found ");
                        break;
                    case PersStringList lookedPersStringList:
                        lookedfor = currentSlot.stringListList.FirstOrDefault(p =>
                            string.CompareOrdinal(p.key, name) == 0) as T;
                        Debug.Assert(lookedfor != null, name + " string list not found ");
                        break;
                    case PersBoolList lookedPersBoolList:
                        lookedfor = currentSlot.boolListList.FirstOrDefault(p =>
                            string.CompareOrdinal(p.key, name) == 0) as T;
                        Debug.Assert(lookedfor != null, name + "bool list not found ");
                        break;
                    case PersIntStringList lookedPersIntStringList:
                        lookedfor = currentSlot.intStringListList.FirstOrDefault(p =>
                            string.CompareOrdinal(p.key, name) == 0) as T;
                        Debug.Assert(lookedfor != null, name + "int string dict list not found ");
                        break;
                    case PersInt2DArray lookedPersInt2DArray:
                        lookedfor = currentSlot.int2DArrays.FirstOrDefault(p =>
                            string.CompareOrdinal(p.key, name) == 0) as T;
                        Debug.Assert(lookedfor != null, name + "int 2D array not found ");
                        break;
                    case PersString2DArray lookedPersString2DArray:
                        lookedfor = currentSlot.string2DArrays.FirstOrDefault(p =>
                            string.CompareOrdinal(p.key, name) == 0) as T;
                        Debug.Assert(lookedfor != null, name + "string 2D array not found ");
                        break;
                    case PersBool2DArray lookedPersBool2DArray:
                        lookedfor = currentSlot.bool2DArrays.FirstOrDefault(p =>
                            string.CompareOrdinal(p.key, name) == 0) as T;
                        Debug.Assert(lookedfor != null, name + "bool 2D array not found ");
                        break;

                    default:
                        Debug.LogError("That should not be registered in save");
                        break;
                }
            }
        }
    }
}
