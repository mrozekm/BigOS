﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Linq;
using UnityEngine.UI;
using Medicine;
using BigOS;
using BigOS.Localization;

namespace BigOS.SaveSystem{
    [Register.Single]
    public class SaveSlotViewModel : MonoBehaviour
    {
        public const int amountOfSaveSlots = 4;

        [SerializeField] List<SaveSlot> saveSlots;
        [SerializeField] GameObject saveSlotPrefab;
        [SerializeField] GameObject parentOfSlots;
        [SerializeField] BigOS.UIAnimator animator;
        [SerializeField] GameObject prefabHome;
        [SerializeField] private BigOSMenusManager menusManager;
        [Header("Delete SaveSlotNotification")]
        [SerializeField] Text notificationSaveSlotText;
        [SerializeField] UIAnimator notificationAnimator;
        [Inject.Single]
        PersistenceManager persistenceManager { get; }
        [Inject.Single]
        SaveSystemCore saveSystemCore {get; }


        public int selectedSlot = 0;

        // Start is called before the first frame update
        void Start()
        {

        // InitListOfSlots();
        }

        public void HideSaveSlots()
        {
            animator.SetEnabled(false);
        }
        
        public void SetAnimator(bool active)
        {
            animator.SetEnabled(active);
            if (active)Refresh();
        }
        

        public void Refresh()
        {
        
            InitListOfSlots();
            animator.SetEnabled(true);
            animator.gameObject.SetActive(true);
        //  s.animator.SetEnabled(true);
        }

        [ContextMenu("Init list of slots")]
        public void InitListOfSlots()
        {
            //FIRST TAKE THE PREFAB FROM THE LIST PLACE
            saveSlotPrefab.transform.SetParent(prefabHome.transform);
            saveSlotPrefab.SetActive(false);
            saveSlots = new List<SaveSlot>();

            BIGosUtils.DeleteAllChildren(parentOfSlots);
            parentOfSlots.transform.localPosition = Vector3.zero;

            for (int i = 0; i < amountOfSaveSlots; i++)
            {
                GameObject newGameobject = Instantiate(saveSlotPrefab);
                newGameobject.SetActive(true);
                
                SaveSlot slot = newGameobject.GetComponent<SaveSlot>();
                slot.transform.SetParent(parentOfSlots.transform);
                newGameobject.transform.localScale = new Vector3(1f, 1f, 1f);
                newGameobject.transform.position = Vector3.zero;
                slot.Initialize( i);
                saveSlots.Add(slot);
            }
        }

        public void SelectSlot(int numberOfSlot )
        {
            Debug.Log("SAVE_SLOT_KEY selected slot " + numberOfSlot);

            //OBSOLETE PersistenceManager.SaveAllowed = true;
            SaveSystemCore.SaveAllowed = true;

            selectedSlot = numberOfSlot;
            PlayerPrefs.SetInt(SaveSystemCore.SAVE_SLOT_KEY, numberOfSlot);

            //OBSOLETE persistenceManager.LoadCurrentSlot();
            saveSystemCore.LoadCurrentSlot();
    Debug.Log("select slot and start game");
            menusManager.gameObject.SendMessageUpwards ("StartGameAfterSelectingSaveSlot"); //StartGameAfterSelectingSaveSlot();
            //well they have to be like that

            //OBSOLETE PersistenceManager.SetBool("LanguageSelected", true);
            saveSystemCore.SetBool("LanguageSelected", true);

        // AudioSourceCollection.PlaySound(AbilitySoundEnum.EnterSaveSlotSelect);

        }



        public void DeleteSlot()
        {
            //selectedSlot = numberOfSlot;
            animator.SetEnabled(true);
            animator.gameObject.SetActive(true);
            notificationAnimator.SetEnabled(false);

            //OBSOLETE string fileAdress = persistenceManager.GetPath(selectedSlot);
            string fileAdress = saveSystemCore.GetPath(selectedSlot);

            if (File.Exists(fileAdress))
            {
                // File.Delete(fileAdress);

                //OBSOLETE PlayerPrefs.SetInt(PersistenceManager.SAVE_SLOT_KEY, selectedSlot);
                PlayerPrefs.SetInt(SaveSystemCore.SAVE_SLOT_KEY, selectedSlot);

                //OBSOLETE persistenceManager.ClearSlot();
                saveSystemCore.ClearSlot();

                SetSaveSlotToFirstAvailable();
            // AudioSourceCollection.PlaySound(AbilitySoundEnum.DeleteSaveSlot);
                InitListOfSlots();
            }
            else
            {
                Debug.LogError("something went wrong ! !");
            }
        }



        #region Events from popup window

        public void CancelSlotDelete()
        {
            PopupSetActive(false);
            
        }



        public void PopupSetActive(bool value)
        {
            if (value == true)
            {
            // AudioSourceCollection.PlaySound(AbilitySoundEnum.CancelAbilitySound);
            }
            else
            {
            //   AudioSourceCollection.PlaySound(AbilitySoundEnum.CancelSound);
            }


            if (value == true)
                notificationAnimator.gameObject.SetActive(true);
            notificationAnimator.SetEnabled(value);

            if (value == false)
                animator.gameObject.SetActive(true);
            animator.SetEnabled(!value);
        }

        public void PopupRefresh()
        {
            bool exists;
            Sprite sprite;
            notificationSaveSlotText.text = GetSlotText(selectedSlot, out exists, out sprite);
        }



        #endregion

        #region Get Slot Text

        public string GetSlotText(int slot, out bool itExists, out Sprite lastPuzzleImage)
        {
            lastPuzzleImage = null; 
            itExists = false;

            //LETS CHECK IF SAVE FILE EXISTS - IF NOT, FORGET THE REST
            //OBSOLETE if (!File.Exists(persistenceManager.GetPath(slot)))
            if (!File.Exists(saveSystemCore.GetPath(slot)))
            {
                return (slot+1).ToString() + ". " +  IsEmpty();
            }
            // OK THE FILE EXISTS! GREAT, LETS GET DATA FROM IT
            DataContainer dataContainer;
            try
            {
                //OBSOLETE dataContainer = PersistenceUtils.LoadSlot<DataContainer>(persistenceManager.GetPath(slot), PersistenceManager.GetEncryption() );
                dataContainer = SaveSystemUtils.LoadSlot<DataContainer>(saveSystemCore.GetPath(slot), saveSystemCore.GetEncryption() );
                
                if (dataContainer == null)
                {
                    return SaveFileCorrupted();
                }
                
            }
            catch
            {
                Debug.LogError("data container broken");
                return SaveFileCorrupted();
            }

            PersInt timeEntry = dataContainer.intList.FirstOrDefault(x => x.key == "ElapsedTime");
            if (timeEntry == null) { Debug.LogError("no time entry"); return SaveFileCorrupted(); }
            

            //-----
            string gameTimeText = VocabularyCore.GetEntry(0, 4);// LocalizationAccess.GetString(LocEnum_UIGeneral.GameTime);
            string hourText = VocabularyCore.GetEntry(0, 6);//LocalizationAccess.GetString(LocEnum_UIGeneral.Hours);
            string minutesText = VocabularyCore.GetEntry(0, 8);// LocalizationAccess.GetString( LocEnum_UIGeneral.Minutes);
            //string puzzlesText = LocalizationManagerAccess.GetString(LocalizationCategoriesEnum.UI, EnumUtility.GetIndex(LocEnum_UI.Puzzles));
        // string levelText = LocalizationManagerAccess.GetString(LocalizationCategoriesEnum.UI, EnumUtility.GetIndex(LocEnum_UI.level));
            //---------
            int toMinutes = timeEntry.value / 60;
            int finalHours = toMinutes <= 60 ? 0 : toMinutes / 60;
            int finalMinutes = toMinutes - (finalHours * 60);
            //----

            string finaltext = (slot+1) + ". " + gameTimeText + " " + finalHours + " " + hourText + " " + finalMinutes + " " + minutesText
                //System.Environment.NewLine 
                //+ ": " + puzzlesFinishedEntry.value + " " + " "+ levelEntry.value
                ;
            itExists = true;
            // get sprite

        // string currentlySelected = dataContainer.stringList.FirstOrDefault(x => x.key == "CurrentlySelectedMosaique").value;
        
            // IMAGE ??


            return finaltext;
        }



        private static string IsEmpty()
        {

            return VocabularyCore.GetEntry(0, 18); // LocalizationAccess.GetString( LocEnum_UIGeneral.Empty);
        }
        private static string SaveFileCorrupted()
        {
            return VocabularyCore.GetEntry(0, 19); // LocalizationAccess.GetString(LocEnum_UIGeneral.Corrupted);
        
        }

        #endregion;


        // Update is called once per frame
        void Update()
        {
            
        }

        public void SetSaveSlotToFirstAvailable()
        {
            for (int i = 0; i < amountOfSaveSlots; i++)
            {
                //OBSOLETE if (File.Exists(persistenceManager.GetPath(i)))
                if (File.Exists(saveSystemCore.GetPath(i)))
                {
                    Debug.Log("SAVE_SLOT_KEY set to" + i);
                    //OBSOLETE PlayerPrefs.SetInt(PersistenceManager.SAVE_SLOT_KEY, i);
                    PlayerPrefs.SetInt(SaveSystemCore.SAVE_SLOT_KEY, i);
                }

            }
        }

    }
}