﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Newtonsoft.Json;

namespace BigOS.SaveSystem
{
    // This class links the save system with project specific data
    public class PersistenceInitializationExtension : MonoBehaviour
    {

        public virtual void InitializeExternal(DataContainerGameplay currentSlot)
        {
            // FORCE GAME SYSTEMS TO INIT SAVE DATA
            
        }
        
    }



}
