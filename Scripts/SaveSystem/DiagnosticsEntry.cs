using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BigOS.SaveSystem
{
    public class DiagnosticsEntry : MonoBehaviour
    {
        [SerializeField] InputField inputField;
        [SerializeField] Text text;
        [SerializeField] Pers persLink;
        [SerializeField] GameObject expandadButton;
        [SerializeField] GameObject indentation;
        public DiagnosticsScreen diagScreen;
        public int inheritFromIndex = -1;
        public float snapshotValue;
        public bool plusPressed;
        public bool minusPressed;

        public void BUTTON_plus(bool active)
        {
            plusPressed = active;
        }

        public void BUTTON_minus(bool active)
        {
            minusPressed = active;
        }

        // Start is called before the first frame update
        void Start()
        {
            
        }

        public void HideExpandButton()
        {
            expandadButton.SetActive(false);
        }

        public void SetIndentation(bool active)
        {
            indentation.SetActive(active);
        }

        // Update is called once per frame
        void Update()
        {
            if (persLink != null)
            {
                if (inheritFromIndex == -1) Refresh(persLink);
                else
                    RefreshAndSetInheritanceIndex(persLink, inheritFromIndex);
            }
            if (plusPressed)
            {
                PressingPlus();
            }
            if (minusPressed)
            {
            PressingMinus();
            }

        }


        public void Refresh(Pers pers)
        {
            inheritFromIndex = -1;
                persLink = pers;
                if (text)
                    text.text = pers.key;
                switch (pers)
                {
                    case PersInt persint:
                        inputField.contentType = InputField.ContentType.IntegerNumber;
                        inputField.text = persint.value.ToString();
                        break;
                    case PersBool persbool:
                        inputField.contentType = InputField.ContentType.Standard;
                        inputField.text = persbool.value.ToString();
                        break;
                    case PersFloat persfloat:
                        inputField.contentType = InputField.ContentType.DecimalNumber;
                        inputField.text = persfloat.value.ToString();
                        break;
                    case PersLong persLong:
                        inputField.contentType = InputField.ContentType.IntegerNumber;
                        inputField.text = persLong.value.ToString();
                        break;
                    case PersString persString:
                        inputField.contentType = InputField.ContentType.Standard;
                        inputField.text = persString.value.ToString();
                        break;
                    default:
                    if (inputField)
                        inputField.interactable = false;
                    break;
                    //  return;
                }
        }

        public void RefreshAndSetInheritanceIndex(Pers pers, int index)
        {
        
                
            inheritFromIndex = index;
            persLink = pers;
            if (text)
                text.text = index.ToString();
            switch (pers)
            {
                case PersIntList persint:
                    inputField.contentType = InputField.ContentType.IntegerNumber;
                    inputField.text = persint.value[index].ToString();
                    break;
                case PersBoolList persbool:
                    inputField.contentType = InputField.ContentType.Standard;
                    inputField.text = persbool.value[index].ToString();
                    break;
                case PersStringList persString:
                    inputField.contentType = InputField.ContentType.Standard;
                    inputField.text = persString.value[index].ToString();
                    break;
                case PersIntStringList persIntString:
                    inputField.contentType = InputField.ContentType.Standard;
                    inputField.text = persIntString.value[index].ToString();
                    break;
                default:
                
                    return;
            }
        }

        public void Refresh()
        {


            switch (persLink)
            {
                case PersInt persint:
                    int.TryParse(inputField.text, out persint.value);
                    break;
                case PersBool persbool:
                    bool.TryParse(inputField.text, out persbool.value);
                    break;
                case PersFloat persfloat:
                    float.TryParse(inputField.text, out persfloat.value);
                    break;
                case PersLong persLong:
                    long.TryParse(inputField.text, out persLong.value);
                    break;
                case PersString persString:
                    persString.value = inputField.text;
                    break;              
                case PersIntList persintList:
                    int val = persintList.value[inheritFromIndex];
                    int.TryParse(inputField.text, out val);
                    persintList.value[inheritFromIndex] = val;
                    break;
                case PersBoolList persboolList:
                    bool valbool = persboolList.value[inheritFromIndex];
                    bool.TryParse(inputField.text, out valbool);
                    persboolList.value[inheritFromIndex] = valbool;
                    break;
                case PersStringList persStringList:
                    string valstring = persStringList.value[inheritFromIndex];
                    bool.TryParse(inputField.text, out valbool);
                    persStringList.value[inheritFromIndex] = valstring;
                    break;
                case PersIntStringList persIntStringList:
                    PersInt persIntEntry = persIntStringList.value[inheritFromIndex];
                    int.TryParse(inputField.text, out persIntEntry.value);
                    break;
                default:              
                    return;
            }
        }

        public void PressSnapshot()
        {
            switch (persLink)
            {
                case PersInt persint:
                    snapshotValue = (float)persint.value;
                    break;
                case PersBool persbools:
                    persbools.value = !persbools.value;
                    break;
                case PersBoolList persboolslat:
                    persboolslat.value[inheritFromIndex] = !persboolslat.value[inheritFromIndex];
                    break;
                case PersFloat persfloat:
                    snapshotValue = persfloat.value;
                    break;
                case PersLong persLong:
                    snapshotValue = persLong.value;
                    break;
                case PersIntList persintList:
                    snapshotValue = persintList.value[inheritFromIndex] ;
                    break;
                case PersIntStringList persIntStringList:
                    snapshotValue = persIntStringList.value[inheritFromIndex].value;
                    break;
                default:
                    return;
            }
        }


        public void PressingPlus()
        {
            //Debug.Log("pressingPluss");
            diagScreen.Pressing();
            switch (persLink)
            {
                case PersInt persint:
                    persint.value = (int)diagScreen.GetPressedMultiplier() + (int)snapshotValue;
                    break;
                case PersFloat persfloat:
                    persfloat.value = (diagScreen.GetPressedMultiplier()/ 10f) + snapshotValue;
                    break;
                case PersLong persLong:
                    persLong.value = (int)diagScreen.GetPressedMultiplier() + (long)snapshotValue;
                    break;        
                case PersIntList persintList:
                    persintList.value[inheritFromIndex] = (int)diagScreen.GetPressedMultiplier() + (int)snapshotValue; ;
                    break;
                case PersIntStringList persIntStringList:
                    persIntStringList.value[inheritFromIndex].value = (int)diagScreen.GetPressedMultiplier() + (int)snapshotValue;
                    break;
                default:
                    return;
            }
        }

        public void PressingMinus()
        {
        // Debug.Log("pressingPluss");
            diagScreen.Pressing();
            switch (persLink)
            {
                case PersInt persint:
                    persint.value = -(int)diagScreen.GetPressedMultiplier() + (int)snapshotValue;
                    break;
                case PersFloat persfloat:
                    persfloat.value = -(diagScreen.GetPressedMultiplier() / 10f) + snapshotValue;
                    break;
                case PersLong persLong:
                    persLong.value = -(int)diagScreen.GetPressedMultiplier() + (long)snapshotValue;
                    break;
                case PersIntList persintList:
                    persintList.value[inheritFromIndex] = -(int)diagScreen.GetPressedMultiplier() + (int)snapshotValue; ;
                    break;
            
                case PersIntStringList persIntStringList:
                    persIntStringList.value[inheritFromIndex].value = -(int)diagScreen.GetPressedMultiplier() + (int)snapshotValue;
                    break;
                default:
                    return;
            }
        }

        public void PressExpand()
        {
            diagScreen.Pressing();
            //  Debug.Log("pressingExpand");
        }

        public void Unpress()
        {
            diagScreen.Released();
            plusPressed = false;
            minusPressed = false;
        // Debug.Log("unpress");
        }
    }
}
