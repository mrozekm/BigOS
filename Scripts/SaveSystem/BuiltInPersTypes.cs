using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

namespace BigOS.SaveSystem
{
        [System.Serializable]
        public class Pers
        {
            public string key;
        }
        [System.Serializable]
        public class CustomPers : Pers
        {
            
        }
        [System.Serializable]
        public class PersInt: Pers
        {
            public int value;
            [JsonConstructor]
            public PersInt(string key, int value)
            {
                this.key = key;
                this.value = value;
            }
    
            public PersInt(string key)
            {
                this.key = key;
            }
            
            public PersInt()
            {
            }
    
        }
        [System.Serializable]
        public class PersBool : Pers
        {
            public bool value;
            [JsonConstructor]
            public PersBool(string key, bool value)
            {
                this.key = key;
                this.value = value;
            }
    
            public PersBool(string key)
            {
                this.key = key;
            }
            public PersBool()
            {
            }
            
        }
        [System.Serializable]
        public class PersFloat : Pers
        {
            public float value;
            [JsonConstructor]
            public PersFloat(string key , float value)
            {
                this.key = key;
                this.value = value;
            }
    
            public PersFloat(string key)
            {
                this.key = key;
            }
            
            public PersFloat()
            {
            }
        }
        [System.Serializable]
        public class PersLong : Pers
        {
            public long value;
            [JsonConstructor]
            public PersLong(string key, long value)
            {
                this.key = key;
                this.value = value;
            }
    
            public PersLong(string key)
            {
                this.key = key;
            }
            
            public PersLong()
            {
            }
        }
        [System.Serializable]
        public class PersString : Pers
        {
            public string value;
            [JsonConstructor]
            public PersString(string key, string value)
            {
                this.key = key;
                this.value = value;
            }
    
            public PersString(string key)
            {
                this.key = key;
            }
            
            public PersString()
            {

            }
        }
    
        //----------------- LISTS OF LIST
       // [System.Serializable]
       // public class PersList : Pers
      //  {
    
      //  }
        //---------
    
            [System.Serializable]
        public class PersStringList : Pers
        {
            public List<string> value;
            [JsonConstructor]
            public PersStringList(string key, List<string> value)
            {
                this.key = key;
                this.value = value;
            }
    
            public PersStringList(string key)
            {
                this.key = key;
            }
            public PersStringList()
            {
                
            }
            
    
            public PersStringList Copy()
            {
                List<string> newList = new List<string>();
                foreach (var v in value)
                {
                    newList.Add(v);
                }
                return new PersStringList(key, newList);
            }
        }
        //-------
    
        [System.Serializable]
        public class PersIntList : Pers
        {
            public List<int> value;
            [JsonConstructor]
            public PersIntList(string key, List<int> value)
            {
                this.key = key;
                this.value = value;
            }
    
            public PersIntList(string key)
            {
                this.key = key;
            }
            
            public PersIntList()
            {
              
            }
    
    
            public PersIntList Copy()
            {
                List<int> newList = new List<int>();
                foreach (var v in value)
                {
                    newList.Add(v);
                }
                return new PersIntList(key, newList);
            }
        }
    
        //-----------------------------------------------------------------------------
    
        [System.Serializable]
        public class PersBoolList : Pers
        {
            public List<bool> value;
            [JsonConstructor]
            public PersBoolList(string key, List<bool> value)
            {
                this.key = key;
                this.value = value;
            }
    
            public PersBoolList(string key)
            {
                this.key = key;
            }
    
            public PersBoolList()
            {
            }
            
    
            public PersBoolList Copy()
            {
              //  StringUtils.RaportHeader("copy bool list: " + key);
    
                List<bool> newList = new List<bool>();
                foreach (bool b in value)
                {
    
                    newList.Add(b);
                //    StringUtils.Raport("+");
                }
               // StringUtils.FinishRaportAndMute();
                return new PersBoolList(key, newList);
    
            }
        }
    
        //-----------
    
    
        //--------------------StringIntDict
    
    
        [System.Serializable]
        public class PersIntStringList : Pers
        {
            public List<PersInt> value;
            [JsonConstructor]
            public PersIntStringList(string key, List<PersInt> value)
            {
                this.key = key;
                this.value = value;
            }
    
            public PersIntStringList(string key)
            {
                this.key = key;
            }
            
            public PersIntStringList()
            {
              
            }
    
    
            public PersIntStringList Copy()
            {
                List<PersInt> newList = new List<PersInt>();
                foreach (var v in value)
                {
                    PersInt newPersInt = new PersInt(v.key, v.value);
    
                    newList.Add(newPersInt);
                }
                return new PersIntStringList(key, newList);
            }
    
        }
        //--------------------2D ARRAY
    
        [System.Serializable]
        public class Pers2DArray : Pers
        {
            protected int width;
            protected int height;
    
            public int Width { get => width; }
            public int Height { get => height; }
    
            protected T[] ResizeArray<T>(int newWidth, int newHeight, T[] value)
            {
                int oldWidth = Width;
                int oldHeight = Height;
    
                StringUtils.RaportHeader("copy pers2d: " + key);
    
                if (newWidth <= 0 || newWidth <= 0) Debug.LogError("Wrong Persistance Array Resize information");
    
                T[] newArray = new T[newWidth * newHeight];
    
                for (int x = 0; x < newWidth; x++)
                {
                    for (int y = 0; y < newHeight; y++)
                    {
                        StringUtils.Raport("x " + x + " y " + y);
                        var assing = MaybeGetTemplateFromArray(oldWidth, oldHeight, value, x, y);
                        int index = x + (y * newHeight);
                        newArray[index] = assing;
                    }
                }
                width = newWidth;
                height = newHeight;
    
                StringUtils.Raport("old: x " + oldWidth + " y " + oldHeight + "/nNew: x " + Width + " y " + Height);
    
                StringUtils.FinishRaport();
    
                return newArray;
            }
    
            public static T MaybeGetTemplateFromArray<T>(int width, int height, T[] value, int targetX, int targetY)
            {
                if (targetX < width && targetY < height)
                {
                    return (T)value[targetX + (targetY * height)];
                }
                else return default(T);
            }
    
            protected T GetElement<T>(T[] value, int targetX, int targetY)
            {
                return MaybeGetTemplateFromArray(Width, Height, value, targetX, targetY); //value[targetX + (targetY * height)];
            }
            protected void SetElement<T>(T[] value, int targetX, int targetY, T element)
            {
                value[targetX + (targetY * Height)] = element;
            }
    
            public void ResolveDimensions(Pers2DArray incoming)
            {
                width = incoming.Width;
                height = incoming.Height;
            }
    
            protected void InsertToRowAndPushRight<T>(int insertRow, int insertColumn, T[] value, T insertedValue)
            {
                if (insertRow >= Width || insertColumn >= Height)
                {
                    Debug.LogError("Failed to insert to 2D array - wrong dimensions\n" +
                        "insert row: " + insertRow+
                        "\ninsert column: " + insertColumn +
                        "\nwidth: " + width +
                        "\nheight: " + height +
                        "\ninsertedvalue = " + insertedValue.ToString());
                   // return value;
                }
                else
                {
                    // WE KNOW WE ARE WITHIN THE DIMENSIONS OF THE ARRAY
                    // LET US DUPLICATE 
                    //
                    //                        TOTAL WIDTH         MAX IS  (width - 1) 
                    //                               |            |
                    int insertionReverseIndex = (Width - 1) - insertRow;
                    // INSERTION REVERSE INDEX SAYS HOW MANY COPIES OF THE REVERSE INDEX HAVE TO BE DONE
                    // 0 IS 0
                    for (int i = 0; i < insertionReverseIndex; i++) // -1 because we need one extra room to copy - otherwise copy is pointless
                    {
                        int copyIndex = (Width - 2)-i;
                        T foundElement = GetElement(value, copyIndex, insertColumn);
                       // Debug.Log("index =" + copyIndex + " found =" + foundElement.ToString());
                        SetElement(value, copyIndex + 1, insertColumn, foundElement);
                    }
                    // OK NOW LETS SET THE ELEMENT - REGERDLES OF WHETHER WE COPIED ANYTHING OR NOT
                    SetElement(value, insertRow, insertColumn, insertedValue);
                   // return value;
                }
            }
    
            protected void Wipe<T>(T[] value)
            {
                for (int i = 0; i < value.Length; i++)
                {
                    value[i] = default(T);
                }
            }
            
            public string ToString<T>( T[] value)
            {
                string endString = "";
                foreach (var VARIABLE in value)
                {
                    endString += VARIABLE + " ";
                }
    
                return endString;
            }
        }
    
    
            [System.Serializable]
        public class PersString2DArray : Pers2DArray
        {
            public string[] value;
    
    
            [JsonConstructor]
            public PersString2DArray(string key, string[] value, int width, int height)
            {
                this.width = width;
                this.height = height;
                this.key = key;
                this.value = value;
            }
    
            public PersString2DArray(string key)
            {
                this.key = key;
            }
            
            public PersString2DArray()
            {
                
            }
       
    
            public PersString2DArray Copy()
            {
                string[] newValue = new string[value.Length];
                for (int i = 0; i < value.Length; i++)
                {
                    newValue[i] = value[i];
                }
                return new PersString2DArray(key, newValue, Width, Height);
            }
    
            public string GetElement(int targetX, int targetY)
            {
                return base.GetElement(value, targetX, targetY);
            }
    
            public void SetElement(int targetX, int targetY, string element)
            {
                 base.SetElement(value, targetX, targetY, element);
            }
    
            public void Resize(int newWidth, int newHeight)
            {
                value = ResizeArray(newWidth, newHeight, value);
            }
    
            public void Wipe()
            {
                base.Wipe(value);
            }
    
    
    
        }
    
        [System.Serializable]
        public class PersInt2DArray : Pers2DArray
        {
            public int[] value;
    
    
            [JsonConstructor]
            public PersInt2DArray(string key, int[] value, int width, int height)
            {
                this.width = width;
                this.height = height;
                this.key = key;
                this.value = value;
            }
    
            public PersInt2DArray(string key)
            {
                this.key = key;
            }
            
            public PersInt2DArray()
            {
                
            }
    
    
            public PersInt2DArray Copy()
            {
                int[] newValue = new int[value.Length];
                for (int i = 0; i < value.Length; i++)
                {
                    newValue[i] = value[i];
                }
                return new PersInt2DArray(key, newValue, Width, Height);
            }
    
            public int GetElement(int targetX, int targetY)
            {
                return base.GetElement(value, targetX, targetY);
            }
    
            public void SetElement(int targetX, int targetY, int element)
            {
                base.SetElement(value, targetX, targetY, element);
            }
    
    
            public void Resize(int newWidth, int newHeight)
            {
                value = ResizeArray(newWidth, newHeight, value);
            }
    
            public void InsertToRowAndPushRight(int insertRow, int insertColumn, int insertedValue)
            {
                base.InsertToRowAndPushRight(insertRow, insertColumn, value, insertedValue);
            }
    
            public void Wipe()
            {
                base.Wipe(value);
            }
    
            public override string ToString()
            {
                return base.ToString(value);
            }
        }
    
        [System.Serializable]
        public class PersBool2DArray : Pers2DArray
        {
            public bool[] value;
    
            [JsonConstructor]
            public PersBool2DArray(string key, bool[] value, int width, int height)
            {
                this.width = width;
                this.height = height;
                this.key = key;
                this.value = value;
            }
    
            public PersBool2DArray(string key)
            {
                this.key = key;
            }
    
    
            public PersBool2DArray Copy()
            {
                bool[] newValue = new bool[value.Length];
                for (int i = 0; i < value.Length; i++)
                {
                    newValue[i] = value[i];
                }
                return new PersBool2DArray(key, newValue, Width, Height);
            }
    
            public bool GetElement(int targetX, int targetY)
            {
                return base.GetElement(value, targetX, targetY);
            }
    
            public void SetElement(int targetX, int targetY, bool element)
            {
                base.SetElement(value, targetX, targetY, element);
            }
    
            public void Resize(int newWidth, int newHeight)
            {
                value = ResizeArray(newWidth, newHeight, value);
            }
        }
        
        //--------------------

        public class ProjectSpecificPers : Pers
        {
            
        }

        /*
        //--------------------3D ARRAY
    
        [System.Serializable]
        public class Pers3DArray : Pers
        {
            public int width;
            public int height;
            public int depth;
    
            public T[] ResizeArray<T>(int newWidth, int newHeight, T[] value)
            {
                int oldWidth = width;
                int oldHeight = height;
                int o
    
                StringUtils.RaportHeader("copy pers2d: " + key);
    
                if (newWidth <= 0 || newWidth <= 0) Debug.LogError("Wrong Persistance Array Resize information");
    
                T[] newArray = new T[newWidth * newHeight];
    
                for (int x = 0; x < newWidth; x++)
                {
                    for (int y = 0; y < newHeight; y++)
                    {
                        StringUtils.Raport("x " + x + " y " + y);
                        var assing = MaybeGetTemplateFromArray(oldWidth, oldHeight, value, x, y);
                        int index = x + (y * newHeight);
                        newArray[index] = assing;
                    }
                }
                width = newWidth;
                height = newHeight;
    
                StringUtils.Raport("old: x " + oldWidth + " y " + oldHeight + "/nNew: x " + width + " y " + height);
    
                StringUtils.FinishRaport();
    
                return newArray;
            }
    
            public static T MaybeGetTemplateFromArray<T>(int width, int height, T[] value, int targetX, int targetY)
            {
                if (targetX < width && targetY < height)
                {
                    return (T)value[targetX + (targetY * height)];
                }
                else return default(T);
            }
    
            protected T GetElement<T>(T[] value, int targetX, int targetY)
            {
                return MaybeGetTemplateFromArray(width, height, value, targetX, targetY); //value[targetX + (targetY * height)];
            }
            protected void SetElement<T>(T[] value, int targetX, int targetY, T element)
            {
                value[targetX + (targetY * height)] = element;
            }
        }
        */
}
