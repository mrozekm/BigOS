using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Newtonsoft.Json;

namespace BigOS.SaveSystem
{
    [System.Serializable]
    public class DataContainer
    {
        // BASIC TYPES
        [Header("Basic Types")]
        public List<PersInt> intList = new List<PersInt>();
        public List<PersFloat> floatList = new List<PersFloat>();
        public List<PersBool> boolList = new List<PersBool>();
        public List<PersLong> longList = new List<PersLong>();
        public List<PersString> stringList = new List<PersString>();
        // LIST TYPES
        [Header("List Types")]
        public List<PersIntList> intListList = new List<PersIntList>();
        public List<PersStringList> stringListList = new List<PersStringList>();
        public List<PersBoolList> boolListList = new List<PersBoolList>();
        // DICT TYPES
        [Header("Dict Types")]
        public List<PersIntStringList> intStringListList = new List<PersIntStringList>();
        // 2D ARRAYS
        [Header("2D ARRAYS")]
        public List<PersInt2DArray> int2DArrays = new List<PersInt2DArray>();
        public List<PersString2DArray> string2DArrays = new List<PersString2DArray>();
        public List<PersBool2DArray> bool2DArrays = new List<PersBool2DArray>();

        [Header("CUSTOM PERS")]
        public List<CustomPers> customPersList = new List<CustomPers>();
        //TODO
        //Copy, Resolve, etc



        /// <summary>
        /// This command just copies the basic types, ignores list types
        /// </summary>
        /// <returns></returns>
        public DataContainer Copy()
        {
            DataContainer newCon = new DataContainer();
            // copy ints
            newCon.intList = new List<PersInt>();
            for (int i = 0; i < intList.Count; i++)
            {
                newCon.intList.Add(new PersInt(intList[i].key, intList[i].value));
            }
            // copy bools
            newCon.boolList = new List<PersBool>();
            for (int i = 0; i < boolList.Count; i++)
            {
                newCon.boolList.Add(new PersBool(boolList[i].key, boolList[i].value));
            }

            // copy float
            newCon.floatList = new List<PersFloat>();
            for (int i = 0; i < floatList.Count; i++)
            {
                newCon.floatList.Add(new PersFloat(floatList[i].key, floatList[i].value));
            }
            // copy long
            newCon.longList = new List<PersLong>();
            for (int i = 0; i < longList.Count; i++)
            {
                newCon.longList.Add(new PersLong(longList[i].key, longList[i].value));
            }
            // copy string
            newCon.stringList = new List<PersString>();
            for (int i = 0; i < stringList.Count; i++)
            {
                newCon.stringList.Add(new PersString(stringList[i].key, stringList[i].value));
            }
            // list types
            // int list list

            newCon.intListList = new List<PersIntList>();
            for (int i = 0; i < intListList.Count; i++)
            {
                newCon.intListList.Add(intListList[i].Copy());
            }
            // string list list
            newCon.stringListList = new List<PersStringList>();
            for (int i = 0; i < stringListList.Count; i++)
            {
                newCon.stringListList.Add(stringListList[i].Copy());
            }
            // bool list list
            newCon.boolListList = new List<PersBoolList>();
            for (int i = 0; i < boolListList.Count; i++)
            {
                newCon.boolListList.Add(boolListList[i].Copy());
            }
            // dict
            newCon.intStringListList = new List<PersIntStringList>();
            for (int i = 0; i < intStringListList.Count; i++)
            {
                newCon.intStringListList.Add(intStringListList[i].Copy());
            }
            // int 2d array 
            newCon.int2DArrays = new List<PersInt2DArray>();
            for (int i = 0; i < int2DArrays.Count; i++)
            {
                newCon.int2DArrays.Add(int2DArrays[i].Copy());
            }
            // string 2d array
            newCon.string2DArrays = new List<PersString2DArray>();
            for (int i = 0; i < string2DArrays.Count; i++)
            {
                newCon.string2DArrays.Add(string2DArrays[i].Copy());
            }
            // bool 2d array
            newCon.bool2DArrays = new List<PersBool2DArray>();
            for (int i = 0; i < bool2DArrays.Count; i++)
            {
                newCon.bool2DArrays.Add(bool2DArrays[i].Copy());
            }


            return newCon;
        }

        /// <summary>
        /// This has to be done with loaded data. Resolving Affects All Types, even those non basic
        /// </summary>
        /// <param name="loadedData"></param>
        public virtual void Resolve(DataContainer loadedData)
        {
            // Override Ints
            intList = ResolveIncoming(intList, loadedData.intList);
            // Override Bools
            boolList = ResolveIncoming(boolList, loadedData.boolList);
            // Override Floats
            floatList = ResolveIncoming(floatList, loadedData.floatList);
            // Override Longs
            longList = ResolveIncoming(longList, loadedData.longList);
            // Override Strings
            stringList = ResolveIncoming(stringList, loadedData.stringList);

            //----
            // Override Lists
            // intList
            intListList = ResolveIncoming(intListList, loadedData.intListList);
            //stringlist
            stringListList = ResolveIncoming(stringListList, loadedData.stringListList);
            //boolList
            boolListList = ResolveIncoming(boolListList, loadedData.boolListList);
            // Override saveDataOfExampleSystem

            //--dict
            intStringListList = ResolveIncoming(intStringListList, loadedData.intStringListList);
            // 2D ints
            int2DArrays = ResolveIncoming(int2DArrays, loadedData.int2DArrays);
            string2DArrays = ResolveIncoming(string2DArrays, loadedData.string2DArrays);
            bool2DArrays = ResolveIncoming(bool2DArrays, loadedData.bool2DArrays);
            


        }

        public virtual List<T> ResolveIncoming<T>(List<T> home, List<T> incoming)
            where T : Pers
        {
            for (int i = 0; i < home.Count; i++)
            {
                Pers lookedFor = incoming.FirstOrDefault(p => string.CompareOrdinal(p.key, home[i].key) == 0);

                if (lookedFor != null)
                {
                    switch (lookedFor)
                    {
                        case PersInt lookedPersInt:
                            (home[i] as PersInt).value = lookedPersInt.value;
                            break;
                        case PersFloat lookedPersFloat:
                            (home[i] as PersFloat).value = lookedPersFloat.value;
                            break;
                        case PersBool lookedPersBool:
                            (home[i] as PersBool).value = lookedPersBool.value;
                            break;
                        case PersLong lookedPersLong:
                            (home[i] as PersLong).value = lookedPersLong.value;
                            break;
                        case PersString lookedPersString:
                            (home[i] as PersString).value = lookedPersString.value;
                            break;
                        case PersIntList lookedPersIntList:
                            //(home[i] as PersIntList).value = lookedPersIntList.Copy().value;
                            (home[i] as PersIntList).value = ResolveList((home[i] as PersIntList).value, lookedPersIntList.value);
                            break;
                        case PersStringList lookedPersStringList:
                            //(home[i] as PersStringList).value = lookedPersStringList.Copy().value;
                            (home[i] as PersStringList).value = ResolveList((home[i] as PersStringList).value, lookedPersStringList.value);
                            break;
                        case PersBoolList lookedPersBoolList:
                            //Debug.Log("!!!!!!!!!!!!!!!!!!!!!!! Persistence: Resolving bool list " + home[i].key + " with entries " + (home[i] as PersBoolList).value.Count);
                            var value = ResolveList((home[i] as PersBoolList).value, lookedPersBoolList.value);
                            (home[i] as PersBoolList).value = value;
                            //(home[i] as PersBoolList).value =  lookedPersBoolList.Copy().value;
                            break;
                        case PersIntStringList lookedForIntStringDict:
                            (home[i] as PersIntStringList).value = ResolveIntStringList((home[i] as PersIntStringList).value, lookedForIntStringDict.value); ;
                            break;
                        // int 2D Array
                        case PersInt2DArray lookedFor2dIntArray:
                            (home[i] as PersInt2DArray).value = Resolve2DArray((home[i] as PersInt2DArray).value, (lookedFor2dIntArray as PersInt2DArray).Copy().value);
                            (home[i] as PersInt2DArray).ResolveDimensions(lookedFor2dIntArray as PersInt2DArray);
                            break;
                        // string 2D Array
                        case PersString2DArray lookedFor2dStringArray:
                            (home[i] as PersString2DArray).value = Resolve2DArray((home[i] as PersString2DArray).value, (lookedFor2dStringArray as PersString2DArray).Copy().value);
                            (home[i] as PersString2DArray).ResolveDimensions(lookedFor2dStringArray as PersString2DArray);
                            break;
                        // bool 2D Array
                        case PersBool2DArray lookedFor2dboolArray:
                            (home[i] as PersBool2DArray).value = Resolve2DArray((home[i] as PersBool2DArray).value, (lookedFor2dboolArray as PersBool2DArray).Copy().value);
                            (home[i] as PersBool2DArray).ResolveDimensions(lookedFor2dboolArray as PersBool2DArray);
                            break;

                        default:
                            int foundIndex = incoming.IndexOf(lookedFor as T);
                            home[i] = incoming[foundIndex];
                            break;
                    }
                }
            }
            // TODO ADD MISSING ENTRIES FROM INCOMIN

            return home;
        }

        public static T[] Resolve2DArray<T>(T[] listCreatedByGame, T[] incomingList)
        {
            //   Debug.Log(" resolving template list home:" + listCreatedByGame.Count + " incoming:" + incomingList.Count);
            return incomingList;
        }

        public static List<PersInt> ResolveIntStringList(List<PersInt> listCreatedByGame, List<PersInt> incomingList)
        {
            PersInt[] arr = new PersInt[listCreatedByGame.Count];
            for (int i = 0; i < arr.Length; i++)
            {
                if (i < incomingList.Count)
                {
                    arr[i] = new PersInt(incomingList[i].key, incomingList[i].value);
                }
                else
                {
                    arr[i] = new PersInt(listCreatedByGame[i].key, listCreatedByGame[i].value) ;
                }
            }
            List<PersInt> boolListToReturn = arr.ToList();
            return boolListToReturn;
        }

        public static List<T> ResolveList<T>(List<T> listCreatedByGame, List<T> incomingList)
        {
         //   Debug.Log(" resolving template list home:" + listCreatedByGame.Count + " incoming:" + incomingList.Count);
            T[] arr = new T[listCreatedByGame.Count];


            for (int i = 0; i < arr.Length; i++)
            {
               // Debug.Log(i + " --");

                if (i < incomingList.Count)
                {
                    arr[i] = incomingList[i];
                //    Debug.Log(i + " incoming");
                }
                else
                {
                  //  Debug.Log(i + " createdByGame");
                    arr[i] = listCreatedByGame[i];
                }
            }

            List<T> boolListToReturn = arr.ToList();

           // Debug.Log(" returning list with count: " + boolListToReturn.Count);
            return boolListToReturn;
        }

        public int ReturnTotalAmountOfEntries()
        {
            //TODO USE REFLECTION
            return ReturnAllEntries().Count; //intArray.Count + boolArray.Count + floatArray.Count + longArray.Count + stringArray.Count + saveDataOfExampleSystem.Length;
        }

        public virtual List<Pers> ReturnAllEntries()
        {// TODO USE REFLECTION
            List<Pers> returnPers = new List<Pers>();
     
            for (int i = 0; i < intList.Count; i++)
            {
                returnPers.Add(intList[i] as Pers);
            }
            //  bools
            for (int i = 0; i < boolList.Count; i++)
            {
                returnPers.Add(boolList[i] as Pers);
            }

            // copy float      
            for (int i = 0; i < floatList.Count; i++)
            {
                returnPers.Add(floatList[i] as Pers);
            }
            // copy long
            for (int i = 0; i < longList.Count; i++)
            {
                returnPers.Add(longList[i] as Pers);
            }
            // copy string
            
            for (int i = 0; i < stringList.Count; i++)
            {
                returnPers.Add(stringList[i] as Pers);
            }

            // lists

            // copy int list

            for (int i = 0; i < intListList.Count; i++)
            {
                returnPers.Add(intListList[i] as Pers);
            }

            // copy string list

            for (int i = 0; i < stringListList.Count; i++)
            {
                returnPers.Add(stringListList[i] as Pers);
            }

            // copy bool list

            for (int i = 0; i < boolListList.Count; i++)
            {
                returnPers.Add(boolListList[i] as Pers);
            }

            return returnPers;
        }

        public virtual void WipeSave()
        {
            // ints
            for (int i = 0; i < intList.Count; i++)
            {
                intList[i].value = 0;
            }
            //  bools
            for (int i = 0; i < boolList.Count; i++)
            {
                boolList[i].value = false;
            }

            //  float      
            for (int i = 0; i < floatList.Count; i++)
            {
                floatList[i].value = 0f;
            }
            //  long
            for (int i = 0; i < longList.Count; i++)
            {
                longList[i].value = 0;
            }
            //  string


            for (int i = 0; i < stringList.Count; i++)
            {
                if (stringList[i] != null)
                stringList[i].value = "";
            }

            // lists

            // int list

            for (int i = 0; i < intListList.Count; i++)
            {
                if (intListList[i] != null && intListList[i].value != null)
                {
                    for (int j = 0; j < intListList[i].value.Count; j++)
                    {
                        intListList[i].value[j] = 0;
                    }
                }
                   
            }

            // string list

            for (int i = 0; i < stringListList.Count; i++)
            {
                if (stringListList[i] != null && stringListList[i].value != null)
                {
                    for (int j = 0; j < stringListList[i].value.Count; j++)
                    {
                        stringListList[i].value[j] = "";
                    }
                }
                   
            }

            //  bool list

            for (int i = 0; i < boolListList.Count; i++)
            {
                if (boolListList[i] != null && boolListList[i].value != null)
                {
                    for (int j = 0; j < boolListList[i].value.Count; j++)
                    {
                        boolListList[i].value[j] = false;
                    }
                }
                    
            }

            //  int string dict

            for (int i = 0; i < intStringListList.Count; i++)
            {
                if (intStringListList[i] != null && intStringListList[i].value != null)
                {
                    for (int j = 0; j < intStringListList[i].value.Count; j++)
                    {
                        intStringListList[i].value[j].value = 0;
                    }
                }
                
            }

            // int 2Darray
            for (int i = 0; i < int2DArrays.Count; i++)
            {
                if (int2DArrays[i] != null && int2DArrays[i].value != null)
                {
                    for (int j = 0; j < int2DArrays[i].value.Length; j++)
                    {
                        int2DArrays[i].value[j] = 0;
                    }
                }
                
            }

            // string 2Darray
            for (int i = 0; i < string2DArrays.Count; i++)
            {
                if (string2DArrays[i] != null && string2DArrays[i].value != null)
                {
                    for (int j = 0; j < string2DArrays[i].value.Length; j++)
                    {
                        string2DArrays[i].value[j] = "";
                    }
                }
                
            }

            // bool 2Darray
            for (int i = 0; i < bool2DArrays.Count; i++)
            {
                if (bool2DArrays[i] != null && bool2DArrays[i].value != null)
                {
                    for (int j = 0; j < bool2DArrays[i].value.Length; j++)
                    {
                        bool2DArrays[i].value[j] = false;
                    }
                }
                //boolListList[i].value = new List<bool>();
            }

        }

        
        // ------TYPES DEFINITIONS
        // BASIC
        /*[System.Serializable]
        public class Pers
        {
            public string key;
        }*/
        /*[System.Serializable]
        public class PersInt: Pers
        {
            public int value;
            [JsonConstructor]
            public PersInt(string key, int value)
            {
                this.key = key;
                this.value = value;
            }

            public PersInt(string key)
            {
                this.key = key;
            }
        }*/
        /*[System.Serializable]
        public class PersBool : Pers
        {
            public bool value;
            [JsonConstructor]
            public PersBool(string key, bool value)
            {
                this.key = key;
                this.value = value;
            }
            public PersBool(string key)
            {
                this.key = key;
            }
        }*/
        /*[System.Serializable]
        public class PersFloat : Pers
        {
            public float value;
            [JsonConstructor]
            public PersFloat(string key , float value)
            {
                this.key = key;
                this.value = value;
            }
            public PersFloat(string key)
            {
                this.key = key;
            }
        }*/
        /*[System.Serializable]
        public class PersLong : Pers
        {
            public long value;
            [JsonConstructor]
            public PersLong(string key, long value)
            {
                this.key = key;
                this.value = value;
            }
            public PersLong(string key)
            {
                this.key = key;
            }
        }*/
        /*[System.Serializable]
        public class PersString : Pers
        {
            public string value;
            [JsonConstructor]
            public PersString(string key, string value)
            {
                this.key = key;
                this.value = value;
            }
            public PersString(string key)
            {
                this.key = key;
            }
        }*/
        // LISTS
        /*
        [System.Serializable]
        public class PersStringList : Pers
        {
            public List<string> value;
            [JsonConstructor]
            public PersStringList(string key, List<string> value)
            {
                this.key = key;
                this.value = value;
            }

            public PersStringList(string key)
            {
                this.key = key;
            }

            public PersStringList Copy()
            {
                List<string> newList = new List<string>();
                foreach (var v in value)
                {
                    newList.Add(v);
                }
                return new PersStringList(key, newList);
            }
        }
        [System.Serializable]
        public class PersIntList : Pers
        {
            public List<int> value;
            [JsonConstructor]
            public PersIntList(string key, List<int> value)
            {
                this.key = key;
                this.value = value;
            }

            public PersIntList(string key)
            {
                this.key = key;
            }


            public PersIntList Copy()
            {
                List<int> newList = new List<int>();
                foreach (var v in value)
                {
                    newList.Add(v);
                }
                return new PersIntList(key, newList);
            }
        }
        [System.Serializable]
        public class PersBoolList : Pers
        {
            public List<bool> value;
            [JsonConstructor]
            public PersBoolList(string key, List<bool> value)
            {
                this.key = key;
                this.value = value;
            }

            public PersBoolList(string key)
            {
                this.key = key;
            }


            public PersBoolList Copy()
            {
            //  StringUtils.RaportHeader("copy bool list: " + key);

                List<bool> newList = new List<bool>();
                foreach (bool b in value)
                {

                    newList.Add(b);
                //    StringUtils.Raport("+");
                }
            // StringUtils.FinishRaportAndMute();
                return new PersBoolList(key, newList);

            }
        }
        // DICTIONARY
        [System.Serializable]
        public class PersIntStringList : Pers
        {
            public List<PersInt> value;
            [JsonConstructor]
            public PersIntStringList(string key, List<PersInt> value)
            {
                this.key = key;
                this.value = value;
            }

            public PersIntStringList(string key)
            {
                this.key = key;
            }


            public PersIntStringList Copy()
            {
                List<PersInt> newList = new List<PersInt>();
                foreach (var v in value)
                {
                    PersInt newPersInt = new PersInt(v.key, v.value);

                    newList.Add(newPersInt);
                }
                return new PersIntStringList(key, newList);
            }

        }
        // 2D ARRAY
        [System.Serializable]
        public class Pers2DArray : Pers
        {
            protected int width;
            protected int height;

            public int Width { get => width; }
            public int Height { get => height; }

            protected T[] ResizeArray<T>(int newWidth, int newHeight, T[] value)
            {
                int oldWidth = Width;
                int oldHeight = Height;

                StringUtils.RaportHeader("copy pers2d: " + key);

                if (newWidth <= 0 || newWidth <= 0) Debug.LogError("Wrong Persistance Array Resize information");

                T[] newArray = new T[newWidth * newHeight];

                for (int x = 0; x < newWidth; x++)
                {
                    for (int y = 0; y < newHeight; y++)
                    {
                        StringUtils.Raport("x " + x + " y " + y);
                        var assing = MaybeGetTemplateFromArray(oldWidth, oldHeight, value, x, y);
                        int index = x + (y * newHeight);
                        newArray[index] = assing;
                    }
                }
                width = newWidth;
                height = newHeight;

                StringUtils.Raport("old: x " + oldWidth + " y " + oldHeight + "/nNew: x " + Width + " y " + Height);

                StringUtils.FinishRaport();

                return newArray;
            }

            public static T MaybeGetTemplateFromArray<T>(int width, int height, T[] value, int targetX, int targetY)
            {
                if (targetX < width && targetY < height)
                {
                    return (T)value[targetX + (targetY * height)];
                }
                else return default(T);
            }

            protected T GetElement<T>(T[] value, int targetX, int targetY)
            {
                return MaybeGetTemplateFromArray(Width, Height, value, targetX, targetY); //value[targetX + (targetY * height)];
            }
            protected void SetElement<T>(T[] value, int targetX, int targetY, T element)
            {
                value[targetX + (targetY * Height)] = element;
            }

            public void ResolveDimensions(Pers2DArray incoming)
            {
                width = incoming.Width;
                height = incoming.Height;
            }

            protected void InsertToRowAndPushRight<T>(int insertRow, int insertColumn, T[] value, T insertedValue)
            {
                if (insertRow >= Width || insertColumn >= Height)
                {
                    Debug.LogError("Failed to insert to 2D array - wrong dimensions\n" +
                        "insert row: " + insertRow+
                        "\ninsert column: " + insertColumn +
                        "\nwidth: " + width +
                        "\nheight: " + height +
                        "\ninsertedvalue = " + insertedValue.ToString());
                // return value;
                }
                else
                {
                    // WE KNOW WE ARE WITHIN THE DIMENSIONS OF THE ARRAY
                    // LET US DUPLICATE 
                    //
                    //                        TOTAL WIDTH         MAX IS  (width - 1) 
                    //                               |            |
                    int insertionReverseIndex = (Width - 1) - insertRow;
                    // INSERTION REVERSE INDEX SAYS HOW MANY COPIES OF THE REVERSE INDEX HAVE TO BE DONE
                    // 0 IS 0
                    for (int i = 0; i < insertionReverseIndex; i++) // -1 because we need one extra room to copy - otherwise copy is pointless
                    {
                        int copyIndex = (Width - 2)-i;
                        T foundElement = GetElement(value, copyIndex, insertColumn);
                    // Debug.Log("index =" + copyIndex + " found =" + foundElement.ToString());
                        SetElement(value, copyIndex + 1, insertColumn, foundElement);
                    }
                    // OK NOW LETS SET THE ELEMENT - REGERDLES OF WHETHER WE COPIED ANYTHING OR NOT
                    SetElement(value, insertRow, insertColumn, insertedValue);
                // return value;
                }
            }

            protected void Wipe<T>(T[] value)
            {
                for (int i = 0; i < value.Length; i++)
                {
                    value[i] = default(T);
                }
            }
            
            public string ToString<T>( T[] value)
            {
                string endString = "";
                foreach (var VARIABLE in value)
                {
                    endString += VARIABLE + " ";
                }

                return endString;
            }
        }
        [System.Serializable]
        public class PersInt2DArray : Pers2DArray
        {
            public int[] value;


            [JsonConstructor]
            public PersInt2DArray(string key, int[] value, int width, int height)
            {
                this.width = width;
                this.height = height;
                this.key = key;
                this.value = value;
            }

            public PersInt2DArray(string key)
            {
                this.key = key;
            }


            public PersInt2DArray Copy()
            {
                int[] newValue = new int[value.Length];
                for (int i = 0; i < value.Length; i++)
                {
                    newValue[i] = value[i];
                }
                return new PersInt2DArray(key, newValue, Width, Height);
            }

            public int GetElement(int targetX, int targetY)
            {
                return base.GetElement(value, targetX, targetY);
            }

            public void SetElement(int targetX, int targetY, int element)
            {
                base.SetElement(value, targetX, targetY, element);
            }


            public void Resize(int newWidth, int newHeight)
            {
                value = ResizeArray(newWidth, newHeight, value);
            }

            public void InsertToRowAndPushRight(int insertRow, int insertColumn, int insertedValue)
            {
                base.InsertToRowAndPushRight(insertRow, insertColumn, value, insertedValue);
            }

            public void Wipe()
            {
                base.Wipe(value);
            }

            public override string ToString()
            {
                return base.ToString(value);
            }
        }
        [System.Serializable]
        public class PersString2DArray : Pers2DArray
        {
            public string[] value;


            [JsonConstructor]
            public PersString2DArray(string key, string[] value, int width, int height)
            {
                this.width = width;
                this.height = height;
                this.key = key;
                this.value = value;
            }

            public PersString2DArray(string key)
            {
                this.key = key;
            }
    

            public PersString2DArray Copy()
            {
                string[] newValue = new string[value.Length];
                for (int i = 0; i < value.Length; i++)
                {
                    newValue[i] = value[i];
                }
                return new PersString2DArray(key, newValue, Width, Height);
            }

            public string GetElement(int targetX, int targetY)
            {
                return base.GetElement(value, targetX, targetY);
            }

            public void SetElement(int targetX, int targetY, string element)
            {
                base.SetElement(value, targetX, targetY, element);
            }

            public void Resize(int newWidth, int newHeight)
            {
                value = ResizeArray(newWidth, newHeight, value);
            }

            public void Wipe()
            {
                base.Wipe(value);
            }



        }
        [System.Serializable]
        public class PersBool2DArray : Pers2DArray
        {
            public bool[] value;

            [JsonConstructor]
            public PersBool2DArray(string key, bool[] value, int width, int height)
            {
                this.width = width;
                this.height = height;
                this.key = key;
                this.value = value;
            }

            public PersBool2DArray(string key)
            {
                this.key = key;
            }


            public PersBool2DArray Copy()
            {
                bool[] newValue = new bool[value.Length];
                for (int i = 0; i < value.Length; i++)
                {
                    newValue[i] = value[i];
                }
                return new PersBool2DArray(key, newValue, Width, Height);
            }

            public bool GetElement(int targetX, int targetY)
            {
                return base.GetElement(value, targetX, targetY);
            }

            public void SetElement(int targetX, int targetY, bool element)
            {
                base.SetElement(value, targetX, targetY, element);
            }

            public void Resize(int newWidth, int newHeight)
            {
                value = ResizeArray(newWidth, newHeight, value);
            }
        }
        */
    }
    
}
