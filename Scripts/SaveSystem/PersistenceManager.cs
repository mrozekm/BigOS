using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Medicine;
using System.Linq;
using System.Reflection;
using System;

namespace BigOS.SaveSystem
{
    public class PersistenceManager
    {
        [Inject.Single] private static SaveSystemCore saveSystemCore { get;}

        #region GET / SET Variabled
        //GET VALUE
        public static int GetInt(string name)
        {
            PersInt source = saveSystemCore.currentSlot.intList.FirstOrDefault(p => string.CompareOrdinal(p.key, name) == 0);
            if (source != null)
                return source.value;
            else
            {
                Debug.LogError("No such int entry in save: " + name);
                return 0;
            }
        }

        public static bool GetBool(string name)
        {
            PersBool source =
                saveSystemCore.currentSlot.boolList.FirstOrDefault(p => string.CompareOrdinal(p.key, name) == 0);
            if (source != null)
                return source.value;
            else
            {
                Debug.LogError("No such bool entry in save: " + name);
                return false;
            }
        }

        public static float GetFloat(string name)
        {
            PersFloat source =
                saveSystemCore.currentSlot.floatList.FirstOrDefault(p => string.CompareOrdinal(p.key, name) == 0);
            if (source != null)
                return source.value;
            else
            {
                Debug.LogError("No such float entry in save: " + name);
                return 0f;
            }
        }

        public static long GetLong(string name)
        {
            PersLong source =
                saveSystemCore.currentSlot.longList.FirstOrDefault(p => string.CompareOrdinal(p.key, name) == 0);
            if (source != null)
                return source.value;
            else
            {
                Debug.LogError("No such long entry in save: " + name);
                return 0;
            }
        }

        public static string GetString(string name)
        {
            PersString source =
                saveSystemCore.currentSlot.stringList.FirstOrDefault(p => string.CompareOrdinal(p.key, name) == 0);
            if (source != null)
                return source.value;
            else
            {
                Debug.LogError("No such string entry in save: " + name);
                return "";
            }
        }
        //ADD TO VALUE
        public static void AddToInt(string name, int value)
        {
            PersInt target = saveSystemCore.currentSlot.intList.FirstOrDefault(p => string.CompareOrdinal(p.key, name) == 0);
            if (target != null)
                target.value += value;
            else
            {
                Debug.LogError("No such int entry in save: " + name);
            }
        }

        public static void AddToFloat(string name, float value)
        {
            PersFloat target =
                saveSystemCore.currentSlot.floatList.FirstOrDefault(p => string.CompareOrdinal(p.key, name) == 0);
            if (target != null)
                target.value += value;
            else
            {
                Debug.LogError("No such int entry in save: " + name);
            }
        }

        //SET VALUE
        public static void SetInt(string name, int value)
        {
            PersInt target = saveSystemCore.currentSlot.intList.FirstOrDefault(p => string.CompareOrdinal(p.key, name) == 0);
            if (target != null)
                target.value = value;
            else
            {
                Debug.LogError("No such int entry in save: " + name);
            }
        }

        public static void SetFloat(string name, float value)
        {
            PersFloat target =
                saveSystemCore.currentSlot.floatList.FirstOrDefault(p => string.CompareOrdinal(p.key, name) == 0);
            if (target != null)
                target.value = value;
            else
            {
                Debug.LogError("No such float entry in save: " + name);
            }
        }

        public static void SetBool(string name, bool value)
        {
            PersBool target =
                saveSystemCore.currentSlot.boolList.FirstOrDefault(p => string.CompareOrdinal(p.key, name) == 0);
            if (target != null)
                target.value = value;
            else
            {
                Debug.LogError("No such bool entry in save: " + name);
            }
        }

        public static void SetLong(string name, long value)
        {
            PersLong target =
                saveSystemCore.currentSlot.longList.FirstOrDefault(p => string.CompareOrdinal(p.key, name) == 0);
            if (target != null)
                target.value = value;
            else
            {
                Debug.LogError("No such long entry in save: " + name);
            }
        }

        public static void SetString(string name, string value)
        {
            PersString target =
                saveSystemCore.currentSlot.stringList.FirstOrDefault(p => string.CompareOrdinal(p.key, name) == 0);
            if (target != null)
                target.value = value;
            else
            {
                Debug.LogError("No such string entry in save: " + name);
            }
        }
        #endregion

        #region PERSISTENCE REGISTRATION

        //CONSTRUCTOR REGISTRATION
        public static void RegisterPers<T>(ref T pers)
            where T : Pers
        {
            Debug.Log("registering " + pers.key + " " + pers.GetType().ToString());

            string name = pers.key;
            // if (instance == null) instance = GameObject.FindObjectOfType<PersistenceManager>();
            Pers lookedfor = pers;

            saveSystemCore.ReplaceWithSourcePersInDataContainer(ref lookedfor);

            if (lookedfor != null)
            {
                T obj = (T) (object) lookedfor;
                pers = obj ;// as T;
            }
        }

        //ATTRIBUTE REGSTRATION
        public static void RegPers(MonoBehaviour mono)
        {
            // Debug.Log("PERS PERS");
            FieldInfo[] objectFields = mono.GetType()
                .GetFields(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
            for (int i = 0; i < objectFields.Length; i++)
            {
                Debug.Log("Persistent check: " + objectFields[i].Name); // The name of the flagged variable.

                PersistentAttribute attribute =
                    Attribute.GetCustomAttribute(objectFields[i], typeof(PersistentAttribute)) as PersistentAttribute;
                if (attribute != null)
                {
                    Debug.Log("Making Persistent: " + objectFields[i].Name + " " +
                              (attribute as PersistentAttribute).name); 

                    string name = (attribute as PersistentAttribute).name;

                    Pers lookedfor = objectFields[i].GetValue(mono) as Pers;
                    lookedfor.key = (attribute as PersistentAttribute).name;
                    objectFields[i].SetValue(mono, lookedfor);


                   // var lookedfor = objectFields[i].GetValue(mono) as Pers;

                    Debug.Log(lookedfor.GetType().ToString());
                    

                    saveSystemCore.ReplaceWithSourcePersInDataContainer(ref lookedfor);

                    if (lookedfor != null)
                    {
                        objectFields[i].SetValue(mono, lookedfor);
                    }
                }
            }
        }
        #endregion
    }
}
