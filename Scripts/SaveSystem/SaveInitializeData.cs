using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace BigOS.SaveSystem
{
    [CreateAssetMenu(fileName = "SaveInitializeData", menuName = "BigOS/SaveInitializeData", order = 1)]
    public class SaveInitializeData : ScriptableObject
    {
        public DataContainerGameplay dataContainer;
    }
}
