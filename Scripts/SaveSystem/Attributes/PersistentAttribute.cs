using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static System.AttributeTargets;
using static JetBrains.Annotations.ImplicitUseTargetFlags;
using System;

namespace BigOS.SaveSystem
{
    [AttributeUsage(AttributeTargets.Field)]
    public class PersistentAttribute : Attribute
    {
        public string name;

        public PersistentAttribute(string name)
        {
            this.name = name;
        }

    }
    
    public interface IPersRegistration
    {
        /// <summary> Calls the generated initialization method for given component type. </summary>
        void RegisterPersistence();
    }
}
