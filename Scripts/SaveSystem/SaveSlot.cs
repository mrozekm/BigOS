﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Medicine;

namespace BigOS.SaveSystem{
    public class SaveSlot : MonoBehaviour
    {
        [Inject.Single]
        [SerializeField] SaveSlotViewModel saveSlotViewMode { get; }
        [SerializeField] int slotNumber;
        [SerializeField] bool thisSlotExists = false;
        [SerializeField] Text textOfThisSaveSlot;
        [SerializeField] RectTransform mainRectTransform;
        [SerializeField] GameObject deleteButton;
        [SerializeField] Image saveImage;


        public void Initialize( int slotNumber)
        {
            gameObject.transform.localPosition = Vector3.zero;
        //   this.saveSlotManager = saveSlotManager;
            this.slotNumber = slotNumber;
            Sprite outSprite;
            textOfThisSaveSlot.text = saveSlotViewMode.GetSlotText(slotNumber, out thisSlotExists, out outSprite);
            if (outSprite != null)
            {
                saveImage.sprite = outSprite;
            }
            else
            {
                saveImage.gameObject.SetActive(false);
            }
            // what was the las


            if (!thisSlotExists)
            {
                mainRectTransform.anchorMax = new Vector2(1f,1f);
                deleteButton.SetActive(false);
                saveImage.gameObject.SetActive(false);
            }
            
        }

        public void Select()
        {
            saveSlotViewMode.SelectSlot(slotNumber);
        }
        //public void Delete()
    // {
    //     saveSlotManager.DeleteSlot();
    // }

        public void DeletePopup()
        {
            saveSlotViewMode.PopupSetActive(true);
            saveSlotViewMode.selectedSlot = slotNumber;
            saveSlotViewMode.PopupRefresh();
        }
    }
}
