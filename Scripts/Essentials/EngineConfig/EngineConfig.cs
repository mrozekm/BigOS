﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BigOS{
    public class EngineConfig : MonoBehaviour
    {
        static EngineConfig instance;
        public AnimationCurveAsset curves;

        private void Awake()
        {
            instance = this;
        }

        public static AnimationCurveAsset Curves
        {
            get
            {
                if (instance == null)
                    return GameObject.FindObjectOfType<EngineConfig>().curves;
                else
                    return instance.curves;
            }
        }

    
    }
}