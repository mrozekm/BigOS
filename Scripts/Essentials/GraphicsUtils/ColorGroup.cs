﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/// <summary>
/// This class changes color on ui elements and their immediate children
/// </summary>
namespace BigOS{
    public class ColorGroup : MonoBehaviour {

        public bool updateColorFromColorWorker;
        

        // Use this for initialization
        void Start () {
        
            
        //   GetComponent<ColorWorker>().UpdateFinalColor();
        //  GetComponent<ColorWorker>().tweenedValue.isWorking = true;
            SetColorOnObjectAndChildren(gameObject, GetComponent<ColorWorker>().finalColor);

        }
        
        // Update is called once per frame
        void Update () {
            UpdateEverything();
        }

        public void UpdateEverything()
        {
        //   GetComponent<ColorWorker>().UpdateColorWorker();
            if (updateColorFromColorWorker && GetComponent<ColorWorker>() != null )//&& GetComponent<ColorWorker>().tweenedValue.isWorking)
            {            
                SetColorOnObjectAndChildren(gameObject, GetComponent<ColorWorker>().finalColor);
            }
        }


        public void SetColorOnObjectAndChildren(GameObject targetObject, Color coloro)
        {
            SetColorOnObject(targetObject, coloro);
            foreach (Transform t in targetObject.transform) SetColorOnObject(t.gameObject,coloro);
        }

        public void SetColorOnObject(GameObject targetObject, Color coloro)
        {
            Image potentialImage = targetObject.GetComponent<Image>();
            Text potentialText = targetObject.GetComponent<Text>();
            if (potentialImage) SetColor(potentialImage,coloro);
            if (potentialText) SetColor(potentialText, coloro);
        }


        public static void SetColor( Image image, Color color)
        {
            image.color = color;
        }

        public static void SetColor(Text text, Color color)
        {
            text.color = color;
        }

    }
}