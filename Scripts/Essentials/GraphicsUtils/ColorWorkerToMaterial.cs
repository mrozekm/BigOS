﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BigOS{
    public class ColorWorkerToMaterial : MonoBehaviour {

    
        public string colorName = "_Color";

        private Material material;

        private void Awake()
        {
            material = GetComponent<Renderer>().material;
        }

        public void SetColorize(bool active)
        {
            GetComponent<ColorWorker>().subWorkers[0].tweenedValue.active = active;
        }
        
        // Update is called once per frame
        void Update () {
            material.SetColor(colorName, GetComponent<ColorWorker>().finalColor);
        }
    }
}