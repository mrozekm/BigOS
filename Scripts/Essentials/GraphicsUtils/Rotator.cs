﻿using UnityEngine;
using System.Collections;

namespace BigOS{
	public class Rotator : MonoBehaviour {

		public Vector3 direction;
		public float speed;

		//--------------------------------------------------------
		void Update () {
			transform.Rotate (direction * speed * Time.deltaTime);
		}
	}
}