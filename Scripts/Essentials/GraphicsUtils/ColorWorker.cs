﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BigOS{
    public class ColorWorker : MonoBehaviour {

        public bool autoupdate;
        public bool unscaledTime;
        [ColorUsage(true, true)]
        public Color finalColor;
        public bool additiveColorContribution = true;
        public List<SubWorker> subWorkers;
        [HideInInspector]
        public AnimationCurve curve;
        [SerializeField] bool dontInitCurves;
        [SerializeField] Image image;
        [SerializeField] Text text;
        [SerializeField] Material imageHDRMaterial;
        [SerializeField] bool autoupdateImage = false;
        [SerializeField] bool autoupdateText = false;
        [SerializeField] bool autoupdateImageMaterial = false;
        [SerializeField]
        bool autoupdateMaterial;
    [SerializeField] bool updateThisObjectsCanvasGroup;
        private CanvasGroup canvGroup;
        [SerializeField] string textureName;
        [SerializeField] Material mat;


        public Color underColor;


        private void Awake()
        {
            if (mat == null && GetComponent<MeshRenderer>() != null)
                mat = GetComponent<MeshRenderer>().material;

            image = GetComponent<Image>();
            if (image == null) autoupdateImage = false;
            text = GetComponent<Text>();
            if (text == null) autoupdateText = false;
            if (image != null)
            {
                imageHDRMaterial = image.material;
                if (imageHDRMaterial == null)
                {
                    autoupdateImageMaterial = false;
                }
                else
                {
                    imageHDRMaterial = Instantiate(imageHDRMaterial);
                }

            }
            canvGroup = GetComponent<CanvasGroup>();
        }

        // Use this for initialization
        void Start () {
        

            InitSubWorkers();

        }

        public void InitSubWorkers()
        {
            if (dontInitCurves) return;

            CreateBasicCurve();
            foreach (SubWorker s in subWorkers)
            {
                s.Init(curve);
            }
        }

        [ContextMenu("create basic curve")]
        public void CreateBasicCurve()
        {
            curve = new AnimationCurve();
            Keyframe k1 = new Keyframe(0f, 0f, 0f, 0f);
            Keyframe k2 = new Keyframe(1f, 1f, 0f, 0f);
            curve.AddKey(k1);
            curve.AddKey(k2);
        }

        // Update is called once per frame
        void Update () {
            UpdateOfColorWorker();


        }

        public void UpdateOfColorWorker()
        {
            if (autoupdate)
            {
                foreach (SubWorker s in subWorkers)
                {
                    s.Update();
                }
            }

            if (image && autoupdateImage)
                image.color = finalColor;
            if (text && autoupdateText)
                text.color = finalColor;
            if (imageHDRMaterial && autoupdateImageMaterial)
                imageHDRMaterial.SetColor("_Color", finalColor);
            if (updateThisObjectsCanvasGroup)
            {
                canvGroup.alpha = finalColor.a;
            }
            if (autoupdateMaterial)
            {
                mat.SetColor(textureName, finalColor);
            }

            CreateColorComposite();
            //UpdateColorWorker();
        }

        public void CreateColorComposite()
        {
            Color final = underColor;
            foreach (SubWorker s in subWorkers)
            {
                final += (s.color * s.tweenedValue.value);
                if (!additiveColorContribution && GetSumOfAllTweenTimers() > 0f)
                {
                    final /= GetSumOfAllTweenTimers();
                    final = new Color(final.r, final.g, final.b, 1f);
                }
                
            }
            finalColor = final;
        }

        public float GetSumOfAllTweenTimers()
        {
            float retVal = 1f;
            foreach (SubWorker s in subWorkers)
            {
                retVal +=  s.tweenedValue.timer;
            }
            return retVal;
        }

        public void SetDefaultWorker(bool active)
        {
            if (subWorkers[0] != null)
            subWorkers[0].tweenedValue.active = active;
        }

        /*

        public void UpdateColorWorker()
        {
            tweenedValue.Update();
            if (!tweenedValue.isWorking) return;
            UpdateFinalColor();
            
        }

        public void UpdateFinalColor()
        {
            finalColor = Color.Lerp(color1, color2, tweenedValue.value);
        }

        public void SetActive(bool active)
        {
            tweenedValue.active = active;
        }
        */

        [System.Serializable]
        public class SubWorker
        {
            [ColorUsage(true, true)]
            public Color color;
            public OnOffCounter tweenedValue;

            public void Init(AnimationCurve c)
            {
                tweenedValue.curveEnter = c;
                tweenedValue.curveLeave = c;
            }

            public void Update()
            {
                tweenedValue.Update();
                if (!tweenedValue.isWorking) return;
            //   UpdateFinalColor();
            }
        }
    }
}