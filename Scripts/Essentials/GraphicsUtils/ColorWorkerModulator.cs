﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BigOS{
    public class ColorWorkerModulator : MonoBehaviour
    {
        public float cycleTime = 1f;
        ColorWorker colorWorker;
        public int numberOfColorSubWorker = 0;
        float timer;

        // Start is called before the first frame update
        void Start()
        {
            colorWorker = GetComponent<ColorWorker>();
            
        }

        // Update is called once per frame
        void Update()
        {
            timer -= Time.deltaTime;
            if (timer < 0)
            {
                timer = cycleTime;
                colorWorker.subWorkers[numberOfColorSubWorker].tweenedValue.active = !colorWorker.subWorkers[numberOfColorSubWorker].tweenedValue.active;
            }
        }
    }
}