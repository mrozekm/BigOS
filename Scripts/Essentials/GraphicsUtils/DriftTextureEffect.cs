﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BigOS{
    public class DriftTextureEffect : MonoBehaviour {

        public float timeScale = 1f;
        public float effectTimeX;
        public float effectTimeY;
        public AnimationCurve xAnim;
        public AnimationCurve yAnim;
        public Vector2 driftTimeMult = new Vector2(1f,1f);
        public Vector2 startOffset = new Vector2(0f, 0f);
        public bool playing;
        public float loopTime;
        public string textureName = "_MainTex";
        public bool looping;
        public bool autoSetActiveFalse;

        //---

        private Material material;


        // Use this for initialization
        void Start () {
            material = GetComponent<Renderer>().material;
        }
        
        // Update is called once per frame
        void Update () {
            UpdateTime(ref effectTimeX, driftTimeMult.x);
            UpdateTime(ref effectTimeY, driftTimeMult.y);
            UpdateOffset();

        }

        public void UpdateOffset()
        {
            float offsetxValue = xAnim.Evaluate(effectTimeX);
            float offsetyValue = yAnim.Evaluate(effectTimeY);
            material.SetTextureOffset(textureName,new Vector2(startOffset.x+offsetxValue, startOffset.y+offsetyValue));
        }

        public void UpdateTime(ref float effectTime, float mult)
        {
            if (playing)
                effectTime += (Time.deltaTime * timeScale)/mult;
            else
                effectTime = 0;

            if (effectTime > loopTime)
            {
                if (looping)
                {

                }
                else
                {
                    playing = false;
                }
                effectTime = 0;

                if (autoSetActiveFalse)
                    gameObject.SetActive(false);
            }
        }

        public void ApplyColor(Color color, string name)
        {
            GetComponent<Renderer>().material.SetColor(name,color);

        }
    }
}