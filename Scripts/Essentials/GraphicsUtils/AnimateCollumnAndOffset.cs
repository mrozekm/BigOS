﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BigOS{
    public class AnimateCollumnAndOffset : MonoBehaviour {

        public int amountOfRows = 16;
        public string textureName = "_MainTex";
        public float flipbookSpeed = 0.1f;
        public float xOffsetSpeed = 0.2f;

        private Material workingMaterial;
        private float timer;
        private float currentXoffset;
        private float currentYoffset;



        // Use this for initialization
        void Start () {
            workingMaterial = GetComponent<Renderer>().material; 
        }

        public void UpdateOffsets()
        {
            currentXoffset += Time.deltaTime * xOffsetSpeed;
            float fraction = 1f / amountOfRows;
            timer += Time.deltaTime * flipbookSpeed;
            if (timer > fraction)
            {
                timer -= fraction;
                currentYoffset += fraction;
                currentYoffset %= 1f;
            }


            workingMaterial.SetTextureOffset(textureName, new Vector2(currentXoffset, currentYoffset));
        }

        // Update is called once per frame
        void Update () {
            UpdateOffsets();

        }
    }
}