﻿using UnityEngine;
using System.Collections;

namespace BigOS{
    public class AnimatedTextureExtendedUV : MonoBehaviour
    {
        public enum AnimationType { looped, oneTime };

        //vars for the whole sheet
        public int colCount = 8;
        public int rowCount = 1;

        //vars for animation
        public int rowNumber = 0; //Zero Indexed
        public int colNumber = 0; //Zero Indexed
        public int totalCells = 4;
        public int fps = 10;
        public float timer;
        public AnimationType animationType;
        public AnimationCurve curve;
        public AnimationCurve rotationZcurve;
        //Maybe this should be a private var
        private Vector2 offset;
        public float additionalZRotationMultiplier;
        public float loopTime;
        //Update

        void Start()
        {
            SetSpriteAnimation(colCount, rowCount, rowNumber, colNumber, totalCells, fps);
        }

        void Update() {
            SetSpriteAnimation(colCount, rowCount, rowNumber, colNumber, totalCells, fps);
            
        }

        public void ProgressAnimationBy(float timeAmount)
        {
            timer += timeAmount;
            UpdateZRotation(timeAmount);
        }

        //SetSpriteAnimation
        void SetSpriteAnimation(int colCount, int rowCount, int rowNumber, int colNumber, int totalCells, int fps) {

            

            // Calculate index
            int index = CalculateOneTime();//animationType == AnimationType.looped ? CalculateLoop() : CalculateOneTime();
            if (animationType == AnimationType.looped && timer > loopTime)
                timer = 0;

            // Repeat when exhausting all cells
            index = index % totalCells;

            // Size of every cell
            float sizeX = 1.0f / colCount;
            float sizeY = 1.0f / rowCount;
            Vector2 size = new Vector2(sizeX, sizeY);

            // split into horizontal and vertical index
            var uIndex = index % colCount;
            var vIndex = index / colCount;

            // build offset
            // v coordinate is the bottom of the image in opengl so we need to invert.
            float offsetX = (uIndex + colNumber) * size.x;
            float offsetY = (1.0f - size.y) - (vIndex + rowNumber) * size.y;
            Vector2 offset = new Vector2(offsetX, offsetY);

            //// Check if this object contains a projector

            Projector projector = GetComponent<Projector>();
            if (projector == null)
            {
                GetComponent<Renderer>().material.SetTextureOffset("_MainTex", offset);
                GetComponent<Renderer>().material.SetTextureScale("_MainTex", size);
            }
            else
            {
                projector.material.SetTextureOffset("_ShadowTex", offset);
                projector.material.SetTextureScale("_ShadowTex", size);
            }

        
        }

        public int CalculateLoop()
        {
            return (int)(Time.time * fps);
        }

        public int CalculateOneTime()
        {
            int finalTime = (int)(curve.Evaluate(timer) * fps);
        // finalTime = Mathf.Clamp(finalTime, 0, totalCells);
            return finalTime;
        }



        [ContextMenu("ResetAnim")]
        public void ResetAnim()
        {
            timer = 0;
        }

        public void UpdateZRotation(float timeAmount)
        {
            float amount = rotationZcurve.Evaluate(timer)* timeAmount * additionalZRotationMultiplier;
            this.transform.Rotate(0f,0f,amount);
        }

    }
}