﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BigOS{
    public class UIHelper : MonoBehaviour
    {
        private static void PopulateTable<T>(GameObject tableEntryPrefab, RectTransform parent, int spacing, List<T> list, string nameOfObject, bool horizontal, bool reverse)
        {
            BIGosUtils.DeleteAllChildren(parent.gameObject);
            tableEntryPrefab.SetActive(false);
            int i = 0;
            if (!reverse)
            {
                foreach (var element in list)
                {
                    GameObject newOne = Instantiate(tableEntryPrefab, parent.transform);
                    newOne.name = nameOfObject + " " + i;
                    newOne.SetActive(true);
                    i++;
                }
            }
            else
            {
                for (i = list.Count - 1; i >= 0; i--)
                {
                    GameObject newOne = Instantiate(tableEntryPrefab, parent.transform);
                    newOne.name = nameOfObject + " " + i;
                    newOne.SetActive(true);
                }
            }


            if (!horizontal)
            {
                parent.sizeDelta = new Vector2(parent.sizeDelta.x, (i + 1) * spacing);
            }
            else
            {
                parent.sizeDelta = new Vector2((i + 1) * spacing, parent.sizeDelta.y);
            }
        }

        /// <summary>
        /// Describe the 2 things you want to be using, the second one is the return one
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <param name="tableEntryPrefab"></param>
        /// <param name="parent"></param>
        /// <param name="spacing"></param>
        /// <param name="list"></param>
        /// <param name="nameOfObject"></param>
        /// <param name="horizontal"></param>
        /// <param name="reverse"></param>
        /// <returns></returns>
        public static List<T2> PopulateTableAndReturn<T1, T2>(GameObject tableEntryPrefab, RectTransform parent, int spacing, List<T1> list, string nameOfObject, bool horizontal, bool reverse)
        {
            PopulateTable(tableEntryPrefab, parent, spacing, list, nameOfObject, horizontal, reverse);
            List<T2> newListT = new List<T2>();
            foreach (Transform t in parent.transform)
            {
                var thingWeWant = t.GetComponent<T2>();
                if (t != parent.transform && thingWeWant != null)
                    newListT.Add(thingWeWant);
            }
            return (newListT);
        }

        /// <summary>
        /// T1 is list entry T2 in prefab entry
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entryInitCallback"></param>
        /// <param name="inGoingList"></param>
        /// <param name="parent"></param>
        /// <param name="horizontal"></param>
        public static void PopulateTable<T1, T2>(System.Action<T1, T2> entryInitCallback, List<T1> inGoingList, RectTransform parent,
            GameObject entryPrefab, bool horizontal) // out List<T1> outGoingList)
        {
            BIGosUtils.DeleteAllChildren(parent.gameObject);
            entryPrefab.SetActive(false);
            int i = 0;
            {
                foreach (var element in inGoingList)
                {
                    GameObject newOne = Instantiate(entryPrefab, parent.transform);
                    newOne.GetComponent<RectTransform>().position = Vector3.zero;
                    newOne.name = entryPrefab.name + " " + i;
                    newOne.SetActive(true);
                    i++;
                    entryInitCallback(element, newOne.GetComponent<T2>());
                }
            }
            if (!horizontal)
            {
                // Debug.Log(entryPrefab.GetComponent<RectTransform>().sizeDelta.x);
                parent.sizeDelta = new Vector2(parent.sizeDelta.x, (i + 1) * entryPrefab.GetComponent<RectTransform>().sizeDelta.y);
            }
            else
            {
                parent.sizeDelta = new Vector2((i + 1) * entryPrefab.GetComponent<RectTransform>().sizeDelta.x, parent.sizeDelta.y);
            }
        }
    }
}