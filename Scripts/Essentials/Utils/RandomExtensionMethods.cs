using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace BigOS
{
    public static class RandomExtensionMethods
    {
        
        public static long NextLong(this System.Random random,long min, long max){
            ulong uRange = (ulong)(max-min);
            ulong ulongRand;
            do{
                byte[] buf = new byte[8];
                random.NextBytes(buf);
                ulongRand = (ulong)BitConverter.ToInt64(buf, 0);
            }while(ulongRand>ulong.MaxValue-((ulong.MaxValue%uRange)+1)%uRange);
            return (long)(ulongRand%uRange)+min;
        }

        public static long NextLong(this System.Random random, long max)
        {
            return random.NextLong(0, max);
        }

        public static long NextLong(this System.Random random)
        {
            return random.NextLong(long.MinValue, long.MaxValue);
        }
        
    }

}
