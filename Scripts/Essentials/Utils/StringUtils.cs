using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.IO;
using UnityEngine.UI;

//using System.Text.RegularExpressions;

public static class StringUtils //: MonoBehaviour
{
    static List<string> raport = new List<string>();

    public static List<string> GenerateStringListFromMultilineString(string source)
    {
         return source.Split('\n' ).ToList() ;
    }

    public static string  StringFromStringArray(string[] stringArr)
    {

        string returnString = "";
        foreach (string str in stringArr)
        {
            returnString = returnString + "\n" + str;
        }

        return returnString;
    }

    public static string ShrinkString(string inputString, int maxLength)
    {
        return string.Concat(inputString.Take(maxLength));
    }

    /*
    public static bool IsMultiline(string source)
    {
        if ()
    }
    */



    public static bool IsEmpty(string source) // todo make better empty detection
    {

        if (source.Length < 2 || source == System.Environment.NewLine)
            return true;
        else
            return false;
    }

    public static bool DoesItBeginWithAnOpeningBracket(string source)
    {
        if (source[0] == '{')
            return true;
        else return false;
    }

    public static int DetectAndProcessIdForDialogue(ref string source, string name, string id)
    {
       


        // if "{" is the first then all is good
        if (!DoesItBeginWithAnOpeningBracket(source))
        {
            Debug.LogError("the dialogue is messed up, it should start with \"{\" but it does not: " + source + " taken from " + name);
            return 0;
        }
        int closingBracket = source.IndexOf( source.FirstOrDefault(p => p == '}'));
        Debug.Log("closing bracket at " + closingBracket);
        string everythingBetweenBrackets = source.Substring(1, closingBracket-1);
        Debug.Log("found id " + everythingBetweenBrackets);

        int numval = 0;
        int.TryParse(everythingBetweenBrackets, out numval);
        // process

        DeleteBracketsAndDashInFront(ref source, name + " " + id);

        return numval;
    }

    public static void DeleteBracketsAndDashInFront(ref string source, string name)
    {
        int indexOfXlosingBracket = source.IndexOf('}');
        source= source.Remove(0,indexOfXlosingBracket+1);
        Debug.Log(name + " // " + source);
        if (source == null || source.Length == 0)
        {
            Debug.LogError(name + " has an empty substring");
        }
        while (source[0] == ' ' || source[0] == '-')
        {
            source= source.Remove(0, 1);
        }
    }

    public static void DeleteSpaceAndReturnInFrontandBack(ref string source, string name)
    {
        if (source == null || source.Length < 2) return;
        //Debug.Log(source + "...");
        while (source[0] == ' ' || source[0] == '\r' || source[0] == '\n')
        {
            if (source.Length < 2) return;
            source = source.Remove(0, 1);
           // Debug.LogError(name + " trimming front");
        }

        while (source[source.Length-1] == ' ' || source[source.Length - 1] == '\r' || source[source.Length - 1] == '\n')
        {
            if (source.Length < 2) return;
            source = source.Remove(source.Length - 1, 1);
          //  Debug.LogError(name + " trimming back");
        }


    }




    public static void RemoveEmptyEntriesInStringList(ref List<string> stringList)
    {
        for (int i = stringList.Count - 1; i >= 0; i--)
        {
            if (IsEmpty(stringList[i]))
            {
                stringList.Remove(stringList[i]);
            }
        }
    }

    public static int CountAmountOfSymbolsInString(string source)
    {
        return source.Length;
    }

    public static string StringRevealTag(int revealTill, string rawString, Color revealColor)
    {
        string revealColorHash = ColorUtility.ToHtmlStringRGB(revealColor);
        string beginString = "<color=#" + revealColorHash + ">";
        string endString = "</color>";
        revealTill = Mathf.Clamp(revealTill, 0, rawString.Length);
        rawString = rawString.Insert(revealTill, endString);
        rawString = rawString.Insert(0, beginString);
        return rawString;

    }

    public static void FillText(int amountOfSymbolsRevealed, ref Text text, Color fillColor, string stashedString)
    {
        text.color = new Color(0f, 0f, 0f, 0f);
        string stringToShow = StringRevealTag(amountOfSymbolsRevealed, stashedString, fillColor);
        text.text = stringToShow;
    }

    public static void RaportHeader(string raportString)
    {
        raport.Add("RAPORT: " + raportString.ToUpper() + "");
        raport.Add("-----------------------------------------");
    }

    public static void Raport(string raportString)
    {
        raport.Add(raportString);
    }

    public static void FinishRaportAndMute()
    {
        raport = new List<string>();
    }

    public static void FinishRaport()
    {
        raport.Add("");
        raport.Add("END");
        raport.Add("");

        Debug.Log(string.Join(System.Environment.NewLine, raport.ToArray()));
        raport = new List<string>();

    }

    public static string FloatToPercent(float targetFloat)
    {
        return ((int)(targetFloat * 100f)) + "%";
    }


    public static string NormalizeLength(string value, int maxLength)
    {
        return value.Length <= maxLength ? value : value.Substring(0, maxLength);
    }

    #region reading files




    public static List<string> GetEnumAndValue(ref int i, List<string> sourceList, out string entryName)
    {
        int lastAnchor = i;



        List<string> returnValue = new List<string>();
        string firstValueOfReturn = GetStringAfterChar('=', sourceList[i]);
        returnValue.Add(RemoveWhitespace(firstValueOfReturn));
        entryName = RemoveWhitespace(GetStringBeforeChar('=', sourceList[i]));


        while (i < sourceList.Count)
        {
            i++; // WE START WITH SEARCHING THE NEXT LINE - THIS ONE IS NOT RELEVANT
            if (i < sourceList.Count)
            {
                if (DoesThisStringContainChar('=', sourceList[i]))
                {
                    //WE HAVE FOUND 
                    break;
                }
                else
                {
                    returnValue.Add(sourceList[i]);
                }
            }
        }

        return returnValue;
    }


    public static bool DoesThisStringContainChar(char searchedChar, string searchedString)
    {
        if (searchedString.Contains(searchedChar)) return true;
        else return false;
    }

    public static string GetStringBeforeChar(char searchedChar, string testString)
    {
        return testString.Substring(0, testString.IndexOf(searchedChar));
    }

    public static string GetStringAfterChar(char searchedChar, string testString)
    {
        return testString.Substring(testString.IndexOf(searchedChar) + 1, testString.Length - 1);
    }

    public static string RemoveWhitespace(this string str)
    {
        return string.Join("", str.Split(default(string[]), System.StringSplitOptions.RemoveEmptyEntries));
    }

    //TODO
    public static void RemoveWhiteSpaceFromList()
    {

    }


    //Dialogues string methods
    public static List<string> GetStringListFromCurrentEntry(string source){
        List<string> parts=new List<string>();
        source.Replace('＃','#');
        for(int i=1;i<source.Split('#').Length;i++){
            parts.Add(RemoveDialoguePartTagNumber(source.Split('#')[i]));
        }
        return parts;
    }

    public static string RemoveDialoguePartTagNumber(string source){
        string returnString ="";
        returnString=source.Substring(source.IndexOf("\n")+1);
        return returnString;
    }

    public static bool DoesItContainOnlyNumericalChars(string source){
        string noBrackets;
        bool onlyNums=false;
        noBrackets=source.Replace("{","").Replace("}","").Trim();
        //Debug.Log("noBrackets: "+noBrackets);
        if(int.TryParse(noBrackets,out int num))onlyNums=true;
        return onlyNums;
    }
    public static int GetValueFromFirstBracket(string source){
        string returnString=source.Split('{')[1].Split('}')[0];
        //Debug.Log(returnString);
        return int.Parse(returnString);
    }
    public static int GetValueFromSecondBracket(string source){
        string returnString=source.Split('{')[2].Split('}')[0];
        //Debug.Log(returnString);
        return int.Parse(returnString);
    }
    public static string GetDialogueOrQuestionSentence(string source){
        //Debug.Log(source);
        
        //string returnString=source.Split('-')[1];
        string returnString=source.Replace(source.Split('-')[0],"").Remove(0,1);
        if(returnString.Substring(0,1)==" "){
            returnString=returnString.Remove(0,1);
        }
        //Debug.Log(returnString);
        return returnString;
    }

    #endregion
}
