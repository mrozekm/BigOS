using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Text.RegularExpressions;
using System.Linq;
#if UNITY_EDITOR
using UnityEditor;
#endif

public enum CompassDir
{
    E = 0, NE = 1,
    N = 2, NW = 3,
    W = 4, SW = 5,
    S = 6, SE = 7
};
namespace BigOS
{
    public class BIGosUtils : MonoBehaviour
    {
            #region childOperations

        public static void DeleteAllChildren(GameObject target)
        {
            foreach (Transform child in target.transform)
            {
                Destroy(child.gameObject);
            }
        }

        public static List<GameObject> GetImmediateChildren( Transform sourceTransform)
        {
            List<GameObject> gameobjects = new List<GameObject>();
            foreach (Transform child in sourceTransform)
            {
                gameobjects.Add(child.gameObject);
            }
            return gameobjects;
        }

        public static List<T> KeepOnlyObjectsWithComponent<T>(List<GameObject> gameobjects)
        {
            List<T> returnList = new List<T>();
            foreach (GameObject g in gameobjects)
            {         
                if (g.GetComponent<T>() != null)
                {
                    returnList.Add(g.GetComponent<T>());
                }
            }
            return returnList;
        }

        public static List<T> KeepOnlyObjectsWithComponent<T>(List<Transform> gameobjects)
        {
            List<T> returnList = new List<T>();
            foreach (Transform g in gameobjects)
            {
                if (g.GetComponent<T>() != null)
                {
                    returnList.Add(g.GetComponent<T>());
                }
            }
            return returnList;
        }

        public static List<Transform> GetAllChildrenToTheSecondGeneration(Transform aParent)
        {
            List<Transform> transforms = new List<Transform>();
            for (int i = 0; i < aParent.childCount; i++)
            {
                for (int j = 0; j < aParent.GetChild(i).childCount; j++)
                {
                    transforms.Add(aParent.GetChild(i).GetChild(j));
                
                }
                transforms.Add(aParent.GetChild(i));
            }
            return transforms;
        }

        public static Transform FindInSecondGenerationChild(Transform aParent, string aName)
        {
            List<Transform> transforms = GetAllChildrenToTheSecondGeneration(aParent);
            return transforms.FirstOrDefault(p => string.CompareOrdinal(p.gameObject.name, aName) == 0);
        }

        public static Transform FindDeepChild( Transform aParent, string aName)
        {
            Transform[] allTransforms = aParent.GetComponentsInChildren<Transform>(true);
            return allTransforms.FirstOrDefault(p => string.CompareOrdinal(p.gameObject.name,  aName) == 0);

            /*
            Queue<Transform> queue = new Queue<Transform>();
            queue.Enqueue(aParent);
            while (queue.Count > 0)
            {
                var c = queue.Dequeue();
                if (string.CompareOrdinal(c.name, aName) == 0)
                    return c;
                foreach (Transform t in c)
                    queue.Enqueue(t);
            }
            return null;
            */
        }

        public static List<T> GetAllComponentsInAllChildren<T>(Transform aParent)
        {
            Transform[] allTransforms = aParent.GetComponentsInChildren<Transform>(true);
            return KeepOnlyObjectsWithComponent<T>(allTransforms.ToList()).ToList();
        }

        #endregion

        #region tweens

        public static float SimpleTween(float hardValue, float softValue, float speed)
        {
            return Mathf.Lerp(softValue, hardValue, speed * Time.deltaTime);

        // float difference = hardValue - softValue;
        //   difference *= Time.deltaTime * speed;
        //   return softValue += difference;
        }

        public static Vector3 SimpleTween(Vector3 hardValue, Vector3 softValue, float speed)
        {
            return Vector3.Lerp(softValue, hardValue, speed * Time.deltaTime);

        // Vector3 difference = hardValue - softValue;
        // difference *= Time.deltaTime * speed;
        //  difference = Vector3.ClampMagnitude(difference, (hardValue - softValue).magnitude);
        //  return softValue += difference;
        }

        public static Quaternion SimpleTween(Quaternion hardValue, Quaternion softValue, float speed)
        {
            Quaternion differenceQuat = hardValue * Quaternion.Inverse(softValue);
            float differenceFloat = Time.deltaTime * speed;
            return Quaternion.Lerp(hardValue, softValue, 1f - differenceFloat);
        }

        public static Color SimpleTween(Color hardColor, Color softColor, float speed)
        {
            Color difference = hardColor - softColor;    //new Color(hardColor.r - softValue.r, hardValue.g - softValue.g, hardValue.b - softValue.b, hardValue.a - softValue.a);
            difference *= Time.deltaTime * speed;
            return softColor += difference;
        }

    #endregion

    #region saveHelp

        public static string GetPath( string fileName, string ending = ".json")
        {
            string path = "";
            if (fileName.Length < 2)
                fileName = "save";
    #if UNITY_EDITOR
            path = Application.persistentDataPath + "/" + fileName + ending;
            //#endif
    #elif UNITY_ANDROID
            path = Application.persistentDataPath + "/" + fileName + ending;
    #else
        path =System.Environment.GetFolderPath(System.Environment.SpecialFolder.ApplicationData) + "/" + fileName + ending;  

    #endif
            return path;
        }

    #endregion

    #region shuffle

        public static List<T> Randomize<T>(List<T> list)
        {
            List<T> randomizedList = new List<T>();
            while (list.Count > 0)
            {
                int index = Random.Range(0, list.Count); //pick a random item from the master list
                randomizedList.Add(list[index]); //place it at the end of the randomized list
                list.RemoveAt(index);
            }
            return randomizedList;
        }

    #endregion

    #region loading assets

        public static T[] GetAtPath<T>(string path)
        {
    #if UNITY_EDITOR


            ArrayList al = new ArrayList();
            string[] fileEntries = Directory.GetFiles(Application.dataPath + "/" + path);

            foreach (string fileName in fileEntries)
            {
                string temp = fileName.Replace("\\", "/");
                int index = temp.LastIndexOf("/");
                string localPath = "Assets/" + path;

                if (index > 0)
                    localPath += temp.Substring(index);

                Object t = AssetDatabase.LoadAssetAtPath(localPath, typeof(T));

                if (t != null)
                    al.Add(t);
            }

            T[] result = new T[al.Count];

            for (int i = 0; i < al.Count; i++)
                result[i] = (T)al[i];

            return result;
    #else
            return null;
    #endif
        }

        public static string[] GetAllDirectoriesAtPath(string path)
        {

            string[] fileEntries = Directory.GetDirectories(Application.dataPath + "/" + path);

            foreach (string f in fileEntries)
            {
                string temp = f.Replace("\\", "/");
            // Debug.Log(temp);
            // Debug.Log(GetJustFolderName(temp));


                //   FileInfo fInfo = new FileInfo(temp);
                //  string dirName = fInfo.Directory.Name;
                // Debug.Log(dirName);
            }

            return fileEntries;
        }



        public static string GetJustFolderName(string fullAdress)
        {
            return Path.GetFileName(fullAdress);
            // return (fullAdress.Replace(Path.GetDirectoryName(fullAdress) + Path.DirectorySeparatorChar, ""));
        }

        public static string ConvertAdressToAssetsTruncated(string fullAdress)
        {
            string[] split = Regex.Split(fullAdress, "Assets");
            return "Assets" + split[1];
        }


        #endregion

        #region angles and cardinal directions

        public static float AngleTo(Vector2 thisVector, Vector2 to)
        {
            Vector2 direction = to - thisVector;
            float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
            if (angle < 0f) angle += 360f;
            return angle;
        }

        public static CompassDir ConvertAnglesToCardinalDirection(float angle)
        {
            float divAngle = angle / 45f;
            return (CompassDir)((int)divAngle);
        }

        public static Vector2 ConvertCardinalDirectionToVector(CompassDir compasDirection)
        {
            switch (compasDirection)
            {
                case CompassDir.E :
                    return new Vector2(-1, 0);
                
                case CompassDir.NE :
                    return new Vector2(-1, -1);
                
                case CompassDir.N:
                    return new Vector2(0, -1);
                
                case CompassDir.NW:
                    return new Vector2(1, -1);
            
                case CompassDir.W:
                    return new Vector2(1, 0);
                
                case CompassDir.SW:
                    return new Vector2(1, 1);
                
                case CompassDir.S:
                    return new Vector2(0, 1);
                
                case CompassDir.SE:
                    return new Vector2(-1, 1);
                
                default:
                    return new Vector2(0, 0);
                
            }
        }

        #endregion

    #if UNITY_EDITOR

        public static string WhereIs(string _file)
        {
    #if UNITY_SAMSUNGTV
                return "";
    #else
            string[] assets = { Path.DirectorySeparatorChar + "Assets" + Path.DirectorySeparatorChar };
            FileInfo[] myFile = new DirectoryInfo("Assets").GetFiles(_file, SearchOption.AllDirectories);
            string[] temp = myFile[0].ToString().Split(assets, 2, System.StringSplitOptions.None);
            return "Assets" + Path.DirectorySeparatorChar + temp[1];
    #endif
        }

    #endif
        /*
        public static List<string> ListToStringList<T>(List<T> list)
        {
            List<string> names = new List<string>();
            //foreach (< T > thing in list)
            {

            }

        }
        */

        public static float TruncateFloat (float target )
        {
            target *= 10f;
            target = (float)((int)target);
            target /= 10f;
            return target;
        }

        #region collections from project folder

    #if UNITY_EDITOR

        public static List<Material> GatherMaterialsFromGameFolder(string materialCollectionAdress)
        {

            //  string formattedFolder = Application.dataPath + "/" + materialCollectionAdress + "/";
            //  Debug.Log(formattedFolder);

            //  string truncatedFolder  = formattedFolder.Replace("\\", "/");
            //  truncatedFolder = Utils.ConvertAdressToAssetsTruncated(truncatedFolder);

            Debug.Log(materialCollectionAdress);

            List<string> materialAdress = AssetDatabase.FindAssets("t: material", new[] { materialCollectionAdress }).ToList();
            List<Material>  backgroundMaterials = new List<Material>();
            foreach (string s in materialAdress)
            {
                string assetAdress = AssetDatabase.GUIDToAssetPath(s);
                Material mat = (Material)AssetDatabase.LoadAssetAtPath(assetAdress, typeof(Material));
                backgroundMaterials.Add(mat);
            }
            return backgroundMaterials;
        }

    #endif

        #endregion


        #region generic cycling

        public static int GetNextWrapperIndex<T>(List<T> collection, int currentIndex)
        {
            if (collection.Count < 1) return 0;
            return (currentIndex + 1) % collection.Count;
        }

        public static int GetPreviousWrapperIndex<T>(List<T> collection, int currentIndex)
        {
            if (collection.Count < 1) return 0;
            if ((currentIndex - 1) < 0) return collection.Count - 1;
            return (currentIndex - 1) % collection.Count;

        }

        #endregion

        // does the superset contain all of the subset?

        //public static bool ContainsAll<T>(this IEnumerable<T> source, IEnumerable<T> values)
    // {
    //     return values.All(value => source.Contains(value));
    // }
    }
    
    //public T ReturnWeighted<T>(int seed, List<T>)
}

