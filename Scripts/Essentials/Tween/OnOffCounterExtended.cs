﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BigOS{
    [System.Serializable]
    public class OnOffCounterExtended : OnOffCounter
    {
        public AnimationCurve modulationCurve;
        public float modulationSpeed =1f;
        public float modulationValue;
        void Start()
        {
            
        }

        // Update is called once per frame
        public override void Update()
        {
            base.Update();
            UpdateModulation();
        }

        public void UpdateModulation()
        {
            modulationValue = value * modulationCurve.Evaluate(modulationSpeed * (Time.timeSinceLevelLoad % 1f));
        }

    }
}