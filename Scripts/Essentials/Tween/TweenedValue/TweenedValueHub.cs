﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BigOS{
    public class TweenedValueHub : MonoBehaviour {

        public List<TweenedValue> tweenedValues;

        // Use this for initialization
        void Start () {
            
        }
        
        // Update is called once per frame
        void Update () {
            for (int i = 0; i < tweenedValues.Count; i++)
            {
                if (tweenedValues[i] != null)
                {
                    tweenedValues[i].Update();
                }
            }
        }

        public void SetValue(int number, bool onOff)
        {
            tweenedValues[number].active = onOff;
        }

        public void SetActiveFalse(int number)
        {
            tweenedValues[number].active = false;
        }

        public void SetActiveTrue(int number)
        {
            tweenedValues[number].active = true;
        }
    }
}