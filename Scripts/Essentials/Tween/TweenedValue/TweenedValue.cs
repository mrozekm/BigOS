﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BigOS{
    [System.Serializable]
    public class TweenedValue {

        /*
        TweenedValue()
        {
            if (valueAsset != null)
                Debug.Log("created Tween Value with " + valueAsset != null ? valueAsset.name : "nothing");
            else
            {
                Debug.Log("created Tween Value");
            }
        }
        */
        public string name;
        public bool active;
        public int curveNumber;
        private bool deltaActive;
        public float value;
        public float rawTimer;
        public float speed = 1f;
        public float attackMult = 1f;
    



        // Use this for initialization
        void Start () {
            
        }

        public void Reset()
        {
            rawTimer = 0f;
        }

        // Update is called once per frame
        public void Update () {
            //if (valueAsset == null) return;

            if (active && deltaActive == false)
            {
                // just turned on
            }
            if (active == false && deltaActive == true)
            {
                // just turned off
                //if (rawTimer > valueAsset.animationTime)
            //     rawTimer = valueAsset.animationTime;
            }

            if (active)
                rawTimer += Time.deltaTime * speed * attackMult;
            else
                rawTimer -= Time.deltaTime * speed;
            rawTimer = Mathf.Clamp(rawTimer, 0f, 1f);
            //-----
            value = TweenHolder.EvaluateCurve(curveNumber,rawTimer); // valueAsset.curve.Evaluate(rawTimer / valueAsset.animationTime);


            deltaActive = active;
        }

        void Init()
        {

        }


    }
}