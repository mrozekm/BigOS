﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BigOS{
    public class TweenSingleton : MonoBehaviour
    {
        static TweenSingleton instance;

        private void Awake()
        {
            instance = this;
        }


        public static void RunCoroutine(IEnumerator c)
        {
            instance.StartCoroutine(c);
        }


    }
}