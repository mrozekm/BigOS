using UnityEngine;
using System.Collections;

namespace BigOS{
    [System.Serializable]
    public class OnOffCounter {

        // main thing we are getting out of this
        public float value;

        public bool active;
        private bool deltaActive;
        public float maxTimer =1f;
        public float timerSpeed = 1f;
        public float timerMutatedMultiplier = 1f;

        public float timer;
        

        public float timerMutatedInversed;
        float timerLastFrame;
        public AnimationCurve curveEnter;
        public AnimationCurve curveLeave;
        [SerializeField]
        private float lastSinceStartupTime;
        [SerializeField]
        private float deltaStartup;
        public bool timeScaleZeroProof;
        [SerializeField]
        public bool matured;
        [SerializeField]
        private bool dontMindMatured;
        private bool deltaOn;
        public bool isWorking;
        public float completionTimer;
        [SerializeField] float enterCurveNewTimer;
        [SerializeField] float leaveCurveNewTimer;

        [SerializeField] int enterCurveNewCount;
        [SerializeField] int leaveCurveNewCount;


        public OnOffCounter()
        {
            maxTimer = 1f;
            timerSpeed = 1f;
            timerMutatedMultiplier = 1f;
        }

        public OnOffCounter ReturnCopy()
        {
            OnOffCounter ret = new OnOffCounter();
            ret.maxTimer = this.maxTimer;
            ret.timerSpeed = this.timerSpeed;
            ret.timerMutatedMultiplier = 1f;
            ret.timer = timer;
            ret.value = value;
            ret.timerMutatedInversed = timerMutatedInversed;
            ret.curveEnter = curveEnter;
            ret.curveLeave = curveLeave;
            ret.timeScaleZeroProof = timeScaleZeroProof;
            return ret;
    }

        public void DisableGuideUpdate()
        {
            if (timer == 0)
                isWorking = false;
            else
                isWorking = true;
        }

        public virtual void Update()
        {
            if (!dontMindMatured)
                OldenUpdate();
            else
                NewUpdate();

        }

        public void NewUpdate()
        {
            DetectDeltaActive();
            float maxValue = Mathf.Max(leaveCurveNewTimer, enterCurveNewTimer);
            float maxValueMinusOne = maxValue - 1f;

            value = Mathf.Clamp01(curveEnter.Evaluate(enterCurveNewTimer - maxValueMinusOne) - curveLeave.Evaluate(leaveCurveNewTimer - maxValueMinusOne));

            deltaActive = active;

            if (enterCurveNewCount == leaveCurveNewCount)
            {
                enterCurveNewTimer = 0;
                leaveCurveNewTimer = 0;
            }
        }

        public void DetectDeltaActive()
        {
            if (active && !deltaActive)
            {
                // just been activated
                TweenSingleton.RunCoroutine(PlayOutEnter());
            //  leaveCurveNewTimer = 0f;
            }
            if (!active && deltaActive)
            {
                // just been deactivated
                TweenSingleton.RunCoroutine(PlayOutLeave());
                //
            // enterCurveNewTimer = 0f;
            }

        
        }

        IEnumerator PlayOutEnter()
        {
            float duration = 1f / timerSpeed; // 
            enterCurveNewCount++;         
            float normalizedTime = 0;
            while (normalizedTime <= 1f)
            {
                enterCurveNewTimer += Time.deltaTime / duration;
            // enterCurveNewTimer = normalizedTime;
                normalizedTime += Time.deltaTime / duration;
                yield return null;
            }
        }

        IEnumerator PlayOutLeave()
        {
            float duration = 1f / timerSpeed; // 
        

            float normalizedTime = 0;
            while (normalizedTime <= 1f)
            {
                leaveCurveNewTimer += Time.deltaTime / duration;
                //leaveCurveNewTimer = normalizedTime;
                normalizedTime += Time.deltaTime / duration;
                yield return null;
            }
            leaveCurveNewCount++;
        }







        public void OldenUpdate()
        {
            deltaStartup = Time.realtimeSinceStartup - lastSinceStartupTime;
            lastSinceStartupTime = Time.realtimeSinceStartup;
            value = matured == false ? curveEnter.Evaluate(timer * timerMutatedMultiplier) : curveLeave.Evaluate(timer * timerMutatedMultiplier);
            timerMutatedInversed = 1f - value;

            if (active)
            {

                matured = false;
                if (timeScaleZeroProof)
                {
                    timer += deltaStartup * (timerSpeed);
                }
                else
                {
                    timer += Time.deltaTime * (timerSpeed);
                }
            }
            else
            {
                if (timer >= maxTimer - 0.17f)
                {
                    matured = true;
                }

                if (timeScaleZeroProof)
                {
                    timer -= deltaStartup * (timerSpeed);
                }
                else
                {
                    timer -= Time.deltaTime * (timerSpeed);

                }
            }



            if (timer < 0)
            {
                timer = 0;
            }


            if (timer > maxTimer)
            {
                timer = maxTimer;
            }
            timerLastFrame = timer;
            DisableGuideUpdate();
        }


        public void UpdateDeltaOn()
        {
            if (active && !deltaOn)
            {
                JustEnabled();

            }
            else if (!active && deltaOn)
            {
                JustDisabled();
            }

            deltaOn = active;
        }

        public void JustEnabled()
        {

        }

        public void JustDisabled()
        {

        }

    }
}


