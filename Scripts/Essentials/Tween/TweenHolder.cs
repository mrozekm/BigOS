﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BigOS{
    public class TweenHolder : MonoBehaviour {

        public static TweenHolder instance;
        public List<AnimationCurve> curves;

        private void Awake()
        {
            instance = this;
            Initialize();
        }

        // Use this for initialization
        void Start () {
            
        }

        [ContextMenu("Initialize")]
        public void Initialize()
        {
            if (curves == null || curves.Count == 0 )
            {
                curves = new List<AnimationCurve>();
                AddSimpleCurve( new Vector4( 0f,0f,0f,0f));
                AddSimpleCurve(new Vector4(0f, 0f, 2f, 0f));
                AddSimpleCurve(new Vector4(0, 2f, 0f, 0f));
            }

        }

        public void AddSimpleCurve(Vector4 data)
        {
            AnimationCurve curve = new AnimationCurve();
            Keyframe k1 = new Keyframe(0f, 0f, data.x, data.y);
            Keyframe k2 = new Keyframe(1f, 1f, data.z, data.w);
            curve.AddKey(k1);
            curve.AddKey(k2);
            curves.Add(curve);
        }

        // Update is called once per frame
        void Update () {
            
        }

        public static float EvaluateCurve(int curveNumber, float rawValue)
        {
            if (instance == null) return 0f;
            return instance.curves[curveNumber].Evaluate(rawValue);
        }
    }
}