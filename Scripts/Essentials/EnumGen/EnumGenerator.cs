﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace BigOS{
public class EnumGenerator : MonoBehaviour
{

#if UNITY_EDITOR
    // [UnityEditor.MenuItem("Tools/Localization/Generate Localization Keys")]
    public static void GenerateEnums(IEnumerable<EnumGenParent> enumPrefabs, string enumName, bool StoreInPlugins = false) // string writeFolder
    {
        GenerateEnums(ConvertToStringList(enumPrefabs), enumName, StoreInPlugins);
    }

    public static void GenerateEnums(IEnumerable<GameObject> enumGameObjects, string enumName, bool StoreInPlugins = false) // string writeFolder
    {
        GenerateEnums(ConvertToStringList(enumGameObjects), enumName, StoreInPlugins);
    }

    public static void GenerateEnums(IEnumerable<ParticleSystem> enumGameObjects, string enumName, bool StoreInPlugins = false) // string writeFolder
    {
        GenerateEnums(ConvertToStringList(enumGameObjects), enumName, StoreInPlugins);
    }

    public static void GenerateEnums(IEnumerable<AudioSource> enumGameObjects, string enumName, bool StoreInPlugins = false) // string writeFolder
    {
        GenerateEnums(ConvertToStringList(enumGameObjects), enumName, StoreInPlugins = false);
    }

    /*
    public static void GenerateEnums(IEnumerable<Object> enumGameObjects, string enumName, bool StoreInPlugins = false) // string writeFolder
    {
        GenerateEnums(ConvertToStringList(enumGameObjects), enumName, StoreInPlugins);
    }
    */

    public static void GenerateEnums(IEnumerable<ScriptableObject> enumGameObjects, string enumName, bool StoreInPlugins = false) // string writeFolder
    {
        GenerateEnums(ConvertToStringList(enumGameObjects), enumName, StoreInPlugins);
    }



    public static void GenerateEnumsFromObjectChildren(GameObject parent, string enumName, bool StoreInPlugins = false)
    {
        List<string> childNames = new List<string>();

        foreach (Transform child in parent.transform)
        {
            childNames.Add(child.name);
        }
        GenerateEnums(childNames, enumName, StoreInPlugins);

    }


    #region TODO: substitute with templates

    public static IEnumerable<string> ConvertToStringList(IEnumerable<EnumGenParent> enumGameObjects)
    {
        List<string> listOfString = new List<string>();
        foreach (EnumGenParent g in enumGameObjects)
        {
            listOfString.Add(g.name);
        }
        return listOfString as IEnumerable<string>;
    }

    public static IEnumerable<string> ConvertToStringList(IEnumerable<ScriptableObject> enumGameObjects)
    {
        List<string> listOfString = new List<string>();
        foreach (ScriptableObject g in enumGameObjects)
        {
            listOfString.Add(g.name);
        }
        return listOfString;
    }


    public static IEnumerable<string> ConvertToStringList(IEnumerable<GameObject> enumGameObjects)
    {
        List<string> listOfString = new List<string>();
        foreach (GameObject g in enumGameObjects)
        {
            listOfString.Add(g.name);
        }
        return listOfString as IEnumerable<string>;
    }

    public static IEnumerable<string> ConvertToStringList(IEnumerable<AudioSource> enumGameObjects)
    {
        List<string> listOfString = new List<string>();
        foreach (AudioSource g in enumGameObjects)
        {
            listOfString.Add(g.gameObject.name);
        }
        return listOfString as IEnumerable<string>;
    }

    public static IEnumerable<string> ConvertToStringList(IEnumerable<ParticleSystem> enumGameObjects)
    {
        List<string> listOfString = new List<string>();
        foreach (ParticleSystem g in enumGameObjects)
        {
            listOfString.Add(g.gameObject.name);
        }
        return listOfString as IEnumerable<string>;
    }
    /*
    public static IEnumerable<string> ConvertToStringList(IEnumerable<Object> objects)
    {
        List<string> listOfString = new List<string>();
        foreach (Object g in objects)
        {
            listOfString.Add(g.name);
        }
        return listOfString as IEnumerable<string>;
    }
    */

    #endregion
    public static void GenerateEnums(IEnumerable<string> strings, string enumName, bool StoreInPlugins) // string writeFolder
    {

        string writeFolder = (Application.dataPath + "/EnumGen/Generated");
        if (StoreInPlugins)
        {
            writeFolder = (Application.dataPath + "/Plugins/LeanEngine/GeneratedByEnumGen");
        }
        var script = new List<string>();
        script.Add("// AUTO-GENERATED SCRIPT");
        script.Add("// See EnumGenerator.cs to see how it works\n");
        script.Add("public enum " + enumName);
        script.Add(" {");
        // ENUMS
        int i = 0;
        foreach (string obj in strings)
        {
            string finalString = obj.Replace(" ", "_");
            finalString = finalString.Replace("-", "_");
            //script.Add("   "+ finalString + " = " + i + ","); //script.Add(value + " = " + value.GetHashCode() + ",");
            script.Add("    " + finalString + " = " + finalString.GetHashCode() + ","); //script.Add(value + " = " + value.GetHashCode() + ",");
            i++;
        }

        script.Add("}");

        script.Add(" ");
        // ENUM UTILITY

        if (!StoreInPlugins)
        {
            script.Add("public static partial class EnumUtility");
        }
        else
        {
            script.Add("public static partial class EnumUtil");
        }


        // GET INDEX
        script.Add(" {");
        script.Add("    public static int GetIndex(this " + enumName + " a )");
        script.Add("    {");
        script.Add("    switch (a)");
        script.Add("        {");
        i = 0;
        foreach (string obj in strings)
        {
            string finalString = obj.Replace(" ", "_");
            finalString = finalString.Replace("-", "_");
            script.Add("            case " + enumName + "." + finalString + ":");
            script.Add("                return " + i + ";");
            i++;
        }
        script.Add("            default:");
        script.Add("                return 0;");
        script.Add("        }");
        script.Add("    }");
        script.Add("");
        // CONVERT TO
        script.Add("    public static " + enumName + " ConvertTo_" + enumName + "(int value)");
        script.Add("    {");
        script.Add("    switch (value)");
        script.Add("        {");
        i = 0;
        foreach (string obj in strings)
        {
            string finalString = obj.Replace(" ", "_");
            finalString = finalString.Replace("-", "_");
            script.Add("            case " + i + ":");
            script.Add("                return " + enumName + "." + finalString + ";");
            i++;
        }
        script.Add("            default:");
        script.Add("                return 0;");
        script.Add("        }");
        script.Add("    }");
        script.Add("");
        // COUNT
        script.Add("    public static int GetCount(" + enumName + " en)");
        script.Add("    {");
        script.Add("            return " + i + ";");
        script.Add("    }");
        script.Add("}");

        System.IO.File.WriteAllLines(writeFolder + "/" + enumName + ".cs", script.ToArray());
    }

    public static void GenerateLocalizationCategoryDictionary(List<string> categoryNames, List<string> enumNames)
    {
        string GeneratedDictiornaryName = "EnumUtility";
        var script = new List<string>();

        //for (int i = 0; i < categoryNames.Count; i++)
       // {
       //     Debug.LogError(string.Format("{0,-10} {1,10}", categoryNames[i], enumNames[i]));
       // }


        script.Add("// AUTO-GENERATED SCRIPT");
        script.Add("// See EnumGenerator.cs to see how it works\n");
        script.Add("public static partial class EnumUtility ");
        script.Add(" {");
        script.Add("    public static LocalizationCategoriesEnum LocalizationEnumBelongsToCategory<T>(T anyLocEnum)");
        script.Add("    {");
        script.Add("         switch (anyLocEnum)");
        script.Add("        {");
        for (int i = 0; i < categoryNames.Count; i++)
        {
        script.Add("            case " + enumNames[i] + " enum" + i+":");
        script.Add("               return LocalizationCategoriesEnum."+ categoryNames[i]+";");
        }
        script.Add("        default:");
        script.Add("            return 0;");
        script.Add("        }");
        script.Add("    }");
        script.Add("    ");
        script.Add("    public static int ReturnIndexOfUnknownLocEnum<T>(T testEnum)");
        script.Add("    {");
        script.Add("          switch (testEnum)");
        script.Add("        {");
        for (int i = 0; i < categoryNames.Count; i++)
        {
            script.Add("            case " + enumNames[i] + " enum" + i + ":");
            script.Add("               return EnumUtility.GetIndex(" +"enum" + i + ");");
        }
        script.Add("        default:");
        script.Add("            return 0;");
        script.Add("        }");
        script.Add("    }");


        script.Add(" }");


        string writeFolder = (Application.dataPath + "/EnumGen/Generated");
        System.IO.File.WriteAllLines(writeFolder + "/" + GeneratedDictiornaryName + ".cs", script.ToArray());
    }


#endif
}








[System.Serializable]
public class EnumGenParent
{
    public string name;

    //needed for enumgen
    public override string ToString()
    {
        return name;
    }


}
}


