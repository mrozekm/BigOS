﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BigOS{
    public abstract class EnumGenBehavior : MonoBehaviour
    {
        public abstract void GenerateEnums();
    }
}