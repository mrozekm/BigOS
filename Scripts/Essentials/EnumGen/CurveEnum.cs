// AUTO-GENERATED SCRIPT
// See EnumGenerator.cs to see how it works
namespace BigOS{
    public enum CurveEnum
    {
        EaseInOut = 1334458179,
        JumpAppear = -1954608363,
    }
    
    public static partial class EnumUtil
    {
        public static int GetIndex(this CurveEnum a )
        {
        switch (a)
            {
                case CurveEnum.EaseInOut:
                    return 0;
                case CurveEnum.JumpAppear:
                    return 1;
                default:
                    return 0;
            }
        }

        public static CurveEnum ConvertTo_CurveEnum(int value)
        {
        switch (value)
            {
                case 0:
                    return CurveEnum.EaseInOut;
                case 1:
                    return CurveEnum.JumpAppear;
                default:
                    return 0;
            }
        }

        public static int GetCount(CurveEnum en)
        {
                return 2;
        }
    }
}