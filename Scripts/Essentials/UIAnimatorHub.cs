﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BigOS{
    public class UIAnimatorHub : MonoBehaviour
    {
        [SerializeField] bool active;
        [SerializeField] bool deltaActive;
        [SerializeField] bool disableOnLoad;
        [SerializeField] bool hubAcivationContributesToPause;
        //[SerializeField] bool disableOnStart;


        public UIAnimator[] animators;

        public bool Active
        {
            get { return active; }
            set {

                active = value; }
        }
        // Start is called before the first frame update
        void Start()
        {
            if (disableOnLoad)
            {
                active = false;
                gameObject.SetActive(false);
            }
        }

        public void Awake()
        {
        // gameObject.SetActive(true);
        }




        public void SetAnimators(bool value)
        {
        // if (disableOnStart && value == true && Time.timeSinceLevelLoad < 0.5) 

            foreach (UIAnimator a in animators)
            // UIAnimator.Set(a,value);
            {
                if (value == true)
                {
                    a.gameObject.SetActive(true);
                }
                if (a != null)
                    a.SetEnabled( value);
            // a.RunAnim();
            }
        }

        private void OnPreRender()
        {
        
        }

        // Update is called once per frame
        void Update()
        {
            if (deltaActive != active)
            {
                SetEnabled(active);
            };
            deltaActive = active;
        }

        public void SetEnabled(bool activeOut)
        {
        // if (active == activeOut) return;

            active = activeOut;
            SetAnimators(active);
            if (hubAcivationContributesToPause)
            {
                //TimeManager.Pause = TimeManager.Pause + (active ? 1 : -1);

            // if (active) TimeManager.IncrementPause(); else TimeManager.decrementPause();
            }
        }
    }
}