﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BigOS{
    public class UIAnimator : MonoBehaviour {

        public enum AnimationType {scale, transitionToRight, transitionToLeft, alpha, transitionFromDown }
        public AnimationType type;
        public AnimationType exitType;
        public bool active;
        bool deltaActive;
        [SerializeField] bool autoEnable;
        [SerializeField] bool setAlphaInCavasGroupToZero;
        [SerializeField] bool autoDisable;
        [SerializeField] bool autoDisableOnStart;
        [SerializeField] bool debugLog;
        [SerializeField] bool activationCausesPause;
        [SerializeField] CurveEnum curveEnum = CurveEnum.EaseInOut;
        [SerializeField] bool differentCurvesForEnterAndExit;
        [SerializeField] CurveEnum curveEnumExit = CurveEnum.EaseInOut;

        protected CanvasGroup canvasGroup;
        [SerializeField] float invisibleTime;
        protected Image thisImage;

        RectTransform rectTransform;
        Coroutine transitionAnim = null;
        Coroutine transitionAnim2 = null;
        // [SerializeField] protected RawImage rawImage;

        private void Awake()
        {
            AnimatorMaybeDisable();
        }


        // Use this for initialization
        void Start() {
            //  rawImage = GetComponentInChildren<RawImage>();

            InitAnimator();
        
                
        }

        public void AnimatorMaybeDisable()
        {
            if (setAlphaInCavasGroupToZero)
            {
                GetComponent<CanvasGroup>().alpha = 0;
            }


            if (autoDisableOnStart )// )
            {
            // if (debugLog) Debug.Log(gameObject.name + " auto disabled on start");
                active = false;
                gameObject.SetActive(false);
                deltaActive = false;
            }

        }



        public void SetEnabled(bool activeOutside)
        {
            // if (active == this.active) return;

            // if (active && activationCausesPause) TimeManager.IncrementPause();
            // else if (active == false && activationCausesPause) TimeManager.decrementPause();

            // Debug.Log("UI Animator in " + gameObject.name +" set enabled: " + activeOutside);


            if (gameObject.activeSelf == false)
            {

            }


            if (activeOutside == true && this.gameObject.activeSelf == false)
            {
                autoDisableOnStart = false;
            //   Debug.Log(gameObject.name + " UIAnimator turning on and object setting to enabled");
                this.gameObject.SetActive(true);
            }


            if (gameObject.activeInHierarchy == false)
            {
            // WE WILL JUST ENABLE IT FROM ON ENABLE

                active = activeOutside;
            //    if (debugLog) Debug.Log(gameObject.name + " setactive while object is disabled " + activeOutside);
                
            //  InitAnimator();
            }
            else
            {
                StartCoroutine(SetActiveNextFrame(activeOutside));
            }
        

            // if (debugLog) Debug.Log(gameObject.name + " setactive " + active);
            //  this.active = active;
            //  InitAnimator();
        }

        IEnumerator SetActiveNextFrame(bool activeOutside)
        {
            yield return new WaitForEndOfFrame(); 
            active = activeOutside;
        // if (debugLog) Debug.Log(gameObject.name + " setactive with one frame delay " + active);
            InitAnimator();

        }

        protected void InitAnimator()
        {



        // if (debugLog) Debug.Log(gameObject.name + " init animator ");

            canvasGroup = GetComponent<CanvasGroup>();  
            thisImage = GetComponent<Image>();


            rectTransform = GetComponent<RectTransform>();
            rectTransform.anchoredPosition = Vector2.zero;
            switch (type)
            {
                case AnimationType.transitionToLeft:

                    if (active == false)
                        rectTransform.anchoredPosition = new Vector3(-2000, 0f);
                    break;

                case AnimationType.transitionToRight:

                    if (active == false)
                        rectTransform.anchoredPosition = new Vector3(2000, 0f);
                    break;
                case AnimationType.alpha:
                    InitCanvasGroup();
                    break;
                case AnimationType.scale:
                    if (active == false)
                        rectTransform.localScale = Vector3.zero;
                    else
                        rectTransform.localScale = new Vector3(1f, 1f, 1f);
                    break;
                case AnimationType.transitionFromDown:
                    if (active == false)
                        rectTransform.anchoredPosition = new Vector3(0, -500);
                    break;
            }
        }

        public void InitCanvasGroup()
        {
            if (!canvasGroup)
            {
                // create canvas group if it doesnt exist
                if (canvasGroup == null)
                {
                //   canvasGroup = gameObject.AddComponent<CanvasGroup>();
                //   canvasGroup.alpha = 1f;
                }
                return;
            }

            if (active == false)
                canvasGroup.alpha = 0;
            else
                canvasGroup.alpha = 1f;
        }

        //TODO GET CANVAS SCALER IN ONE OF THE PARENTS

        [ContextMenu("StartAnim")]
        public void StartAnim()
        {
            RunAnim();
        }

        private void OnEnable()
        {

            if (Time.timeSinceLevelLoad > 0.3f && autoEnable)
            {
                active = true;
                deltaActive = false;
                InitAnimator();
            //  if (debugLog) Debug.Log(gameObject.name + " OnEnable()");
            }

        }

        private void OnDisable()
        {
            if (autoEnable)
            {
                //if (debugLog) Debug.Log(gameObject.name + " OnDisable(), active false");
                active = false;
            }
        }

        protected void StartAnim(IEnumerator anim)
        {
            if (!gameObject.activeSelf) return;

            if (transitionAnim != null)
                StopCoroutine(transitionAnim);
            transitionAnim = StartCoroutine(anim);
        }

        protected void StartAnim2(IEnumerator anim)
        {
            if (!gameObject.activeSelf) return;

            if (transitionAnim2 != null && transitionAnim != null)
                StopCoroutine(transitionAnim);
            transitionAnim2 = StartCoroutine(anim);
        }

        public virtual void RunAnim()
        {
            if (!gameObject.activeSelf) return;

            AnimationType t = type;
            if (!active && differentCurvesForEnterAndExit)
                t = exitType;



            switch (t)
            {
                case AnimationType.transitionToRight:
                    Vector2 start = new Vector2(0f, 0f);//transform.localScale;
                    Vector2 end = new Vector3(-2000, 0f);
                    StartAnim(Transition(start,end,!active));
                    break;
                case AnimationType.transitionToLeft:
                    start = new Vector2(0f, 0f);//transform.localScale;
                    end = new Vector3(2000, 0f);
                    StartAnim(Transition(start, end, !active));
                    break;
                case AnimationType.alpha:
                    StartAnim2(AlphaTransition (active));
                    break;
                case AnimationType.scale:
                    StartAnim(ScaleAnim());
                    break;
                case AnimationType.transitionFromDown:
                // if (active == false)
                    StartAnim2(AlphaTransition(active));
                    start = new Vector2(0f, 0f);//transform.localScale;
                    if (active == false)
                        end = new Vector3(0, -1000f);
                    else
                        end = new Vector3(0, 600f);

                    StartAnim(Transition(start, end, !active));
                    break;
            }   
        }


        public virtual IEnumerator Transition( Vector3 start, Vector3 end, bool reverse, float animTime = 0.5f)
        {
        
            var t = 0f;

        

            while (t < animTime)
            {
                t += Time.deltaTime;
                if (reverse)
                    rectTransform.anchoredPosition = Vector3.LerpUnclamped(start, end, EvaluateCurve(t, animTime));
                else
                    rectTransform.anchoredPosition = Vector3.LerpUnclamped(end,start, EvaluateCurve(t, animTime));

                if (!reverse)
                {

                    if (t < invisibleTime)
                    {
                        if (thisImage)
                            thisImage.enabled = false;
                    //  if (rawImage)
                    //     rawImage.enabled = false;
                    }
                    else
                    {
                        if (thisImage)
                            thisImage.enabled = true;
                    // if (rawImage)
                    //      rawImage.enabled = true;
                    }
                }

                yield return null;
            }

            transitionAnim = null;
            if (autoDisable && active == false) gameObject.SetActive(false);
        }

        public virtual IEnumerator AlphaTransition(bool fadeIn,  float animTime = 0.3f)
        {
        // yield return new WaitForEndOfFrame();

            if (canvasGroup == null) canvasGroup = GetComponent<CanvasGroup>();
            if (canvasGroup == null) canvasGroup = gameObject.AddComponent<CanvasGroup>();

                var t = 0f;
        //  Debug.Log("alpha transition on "+ gameObject.name );


            while (t < animTime)
            {
                t += Time.deltaTime;
                if (fadeIn)
                    canvasGroup.alpha = Mathf.LerpUnclamped(0f, 1f,  t/animTime);
                else
                    canvasGroup.alpha = Mathf.LerpUnclamped(1f, 0f, t / animTime);
                yield return null;
            }


            if (fadeIn)
            {
                // NOT ALWAYS FINISHING AS IT SHOULD
                canvasGroup.alpha = 1f;
            }

            transitionAnim2 = null;

                if (autoDisable && active == false)
            {
                if (debugLog)
                    Debug.Log("autoDisable && active == false on " + gameObject.name);
                    gameObject.SetActive(false);
            } 
        }



        public virtual IEnumerator ScaleAnim(float animTime = 1.15f)
        {
            var t = 0f;
    
            var startScale = new Vector3(0f, 0f, 0f);//transform.localScale;
            var endScale = new Vector3(1f, 1f, 1f);

            while (t < animTime)
            {
                t += Time.deltaTime;
                if (active)
                transform.localScale = Vector3.LerpUnclamped (startScale, endScale, EvaluateCurve(t,animTime));
                else
                    transform.localScale = Vector3.LerpUnclamped(endScale, startScale, EvaluateCurve(t, animTime));
                yield return null;

            }


        
            transitionAnim = null;
            if (autoDisable && active == false) gameObject.SetActive(false);
        }

        public float EvaluateCurve(float t, float animTime)
        {
            //  return UCurve.EaseInOut(t / animTime);
            float fraction = t / animTime;
            bool leaving = !active;

            return !leaving  ? EngineConfig.Curves.Evaluate(curveEnum, fraction) : EngineConfig.Curves.Evaluate(curveEnumExit, fraction);
        }

        public void ResetAnimator()
        {
            AnimatorMaybeDisable();
            InitAnimator();
        }






        [ContextMenu("Toggle")]
        public void Toggle()
        {
            if (activationCausesPause)
            {
                ToggleWithPause();
            }
            else
            {
                if (active)
                {
                    active = false;
                }
                else
                {
                    this.gameObject.SetActive(true);
                    active = true;
                }
            }

        }



        [ContextMenu("Toggle With Pause")]
        public void ToggleWithPause()
        {
            if (active)
            {
                active = false;
            //  TimeManager.pause--;
            }
            else
            {
                this.gameObject.SetActive(true);
                active = true;
            //   TimeManager.pause++;
            }

        }



        // Update is called once per frame
        void LateUpdate() {

            UpdateDelta();
        }

        protected void UpdateDelta()
        {
            if (active != deltaActive)
            {
                RunAnim();
            }
            deltaActive = active;
        }

        
    }
}