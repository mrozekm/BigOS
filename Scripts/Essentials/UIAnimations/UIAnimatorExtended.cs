﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace BigOS{
    public class UIAnimatorExtended : UIAnimator {

        public float enterTime;
        public float exitTime;
        public float extent = 1000f;

        // Use this for initialization
        void Start () {
        
            InitAnimator();

        }
        
        // Update is called once per frame
        void Update () {
            UpdateDelta();

        }

        [ContextMenu("RunAnimExtended")]
        public override void RunAnim()
        {
            if (!gameObject.activeSelf) return;


            switch (type)
            {
                case AnimationType.transitionToRight:
                    Vector2 start = new Vector2(0f, 0f);//transform.localScale;
                    Vector2 end = new Vector3(-extent, 0f);
                    StartAnim(Transition(start, end, !active, active ? enterTime : exitTime));
                    break;
                case AnimationType.transitionToLeft:
                    Vector2 start2 = new Vector2(0f, 0f);//transform.localScale;
                    Vector2 end2 = new Vector3(extent, 0f);
                    StartAnim(Transition(start2, end2, !active, active ? enterTime : exitTime));
                    break;

                case AnimationType.alpha:
                    StartAnim(AlphaTransition(active, active ? enterTime : exitTime));
                    break;
                case AnimationType.scale:
                    StartAnim(ScaleAnim(active ? enterTime : exitTime));
                    break;
            }
        }
    }
}
