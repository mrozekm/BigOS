﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace BigOS{
    public class ConstantPressButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {
        public UnityEvent theEvent;
        public bool pressed = false;

        // Start is called before the first frame update
        void Start()
        {
            
        }

        public void OnPointerDown (PointerEventData eventData)
        {
            pressed = true;
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            pressed = false;
        }

        // Update is called once per frame
        void Update()
        {
            if (pressed) theEvent.Invoke();
        }
    }
}