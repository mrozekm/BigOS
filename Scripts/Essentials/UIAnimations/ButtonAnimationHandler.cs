﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


namespace BigOS{
	public class ButtonAnimationHandler : MonoBehaviour,
		IPointerDownHandler, IPointerExitHandler, IPointerUpHandler, IPointerEnterHandler
	{
		public enum PassiveAnim { None, Pulse }

		[SerializeField]
		PassiveAnim passiveAnimType = PassiveAnim.None;

		bool isTouchingButton = false;
		bool initialized = false;
		Coroutine transitionAnim = null;
		Coroutine passiveAnim = null;

		Vector3 baseScale;

		void Awake() {
			Init();
		}

		void OnEnable() {
			if (passiveAnim != null)
				StopCoroutine(passiveAnim);

			if (passiveAnimType == PassiveAnim.Pulse)
				passiveAnim = StartCoroutine(PulseAnim());
		}

		public ButtonAnimationHandler Init() {
			if (initialized)
				return this;

			baseScale = transform.localScale;
			return this;
		}

		public void OnPointerEnter(PointerEventData eventData) {
			if (Input.GetMouseButton(0)) {
            Debug.Log("pointerdown");
            isTouchingButton = true;
				StartAnim(OnTouchAnim());
			}
		}

		public void OnPointerDown(PointerEventData eventData) {
			isTouchingButton = true;
    
			StartAnim(OnTouchAnim());
		}

		public void OnPointerExit(PointerEventData eventData) {
			isTouchingButton = false;
			StartAnim(ReturnToDefaultAnim());
		}

		public void OnPointerUp(PointerEventData eventData) {
			if (isTouchingButton) {
				StartAnim(OnClickAnim());
			}

			isTouchingButton = false;
		}

		void StartAnim(IEnumerator anim) {
			if (transitionAnim != null)
				StopCoroutine(transitionAnim);
			transitionAnim = StartCoroutine(anim);
		}

		IEnumerator OnTouchAnim() {

        Debug.Log("on touch anim");

        var t = 0f;
			var animTime = 0.15f;
			var startScale = transform.localScale;
			var endScale = baseScale;
			endScale.x *= 1.1f;
			endScale.y *= 0.9f;

			while (t < animTime) {
				t += Time.unscaledTime;
				transform.localScale = Vector3.Lerp(startScale, endScale, UCurve.EaseOut(t / animTime));
            Debug.Log("on touch anim loop " + t);
            yield return null;
			}

			transitionAnim = null;
		}

		IEnumerator OnClickAnim() {
			var t = 0f;
			var animTime = 0.05f;
			var startScale = transform.localScale;
			var endScale = baseScale;
			endScale.y *= 1.25f;
			endScale.x *= 0.75f;

			while (t < animTime) {
				t += Time.unscaledTime;
				transform.localScale = Vector3.Lerp(startScale, endScale, UCurve.EaseOut(t / animTime));
				yield return null;
			}

			t = 0f;
			animTime = 0.15f;
			startScale = endScale;
			endScale = baseScale;
			while (t < animTime) {
				t += Time.unscaledTime;
				transform.localScale = Vector3.Lerp(startScale, endScale, UCurve.EaseInOut(t / animTime));
				yield return null;
			}

			transitionAnim = null;
		}

		IEnumerator ReturnToDefaultAnim() {
			var t = 0f;
			var animTime = 0.15f;
			var startScale = transform.localScale;
			var endScale = baseScale;

			while (t < animTime) {
				t += Time.unscaledTime;
				transform.localScale = Vector3.Lerp(startScale, endScale, UCurve.EaseInOut(t / animTime));
				yield return null;
			}

			transitionAnim = null;
		}

		IEnumerator PulseAnim() {
			while (true) {
				if (transitionAnim == null && !isTouchingButton)
					transform.localScale = baseScale * (1 + (Mathf.Sin(Time.time) * 0.1f));
				yield return null;
			}
		}

		void OnDestroy() {
			if (transitionAnim != null)
				StopCoroutine(transitionAnim);
		}
	}

    // A Collection of Curve formulas
    public static class UCurve
    {

        public enum Curve
        {
            In, Out, InOut, Flat
        }

        public static float Ease(Curve curve, float t)
        {
            switch (curve)
            {
                case Curve.In:
                    return EaseIn(t);
                case Curve.Out:
                    return EaseOut(t);
                case Curve.InOut:
                    return EaseInOut(t);
                default:
                    return Flat(t);
            }
        }

        public static float EaseInOut(float t)
        {
            return Mathf.Sin((Mathf.Clamp01(t) + 1.5f) * Mathf.PI) * 0.5f + 0.5f;
        }

        public static float EaseIn(float t)
        {
            return Mathf.Sin((Mathf.Clamp01(t) * Mathf.PI * 0.5f) + Mathf.PI * -0.5f) + 1f;
        }

        public static float EaseOut(float t)
        {
            return Mathf.Sin(Mathf.Clamp01(t) * Mathf.PI * 0.5f);
        }

        public static float Flat(float t)
        {
            return Mathf.Clamp01(t);
        }
    }
}