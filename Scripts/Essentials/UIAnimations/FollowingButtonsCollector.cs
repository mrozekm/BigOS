﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

namespace BigOS{
    public enum OddEvenDistinction {odd, even}
    public class FollowingButtonsCollector : MonoBehaviour
    {
        [SerializeField] RectTransform selectionNest;
        [SerializeField] GameObject followerButtonPrefab;
        [SerializeField] GameObject buttonSpawnParent;
        [SerializeField] GameObject oddNestCollector;
        [SerializeField] GameObject evenNestCollector;
        [SerializeField] List<RectTransform> oddRects;
        [SerializeField] List<RectTransform> evenRects;
        [SerializeField] List<UIFollowScript> followButtons;





        // Start is called before the first frame update
        void Start()
        {
            Initialize();
        }

        

        public void Initialize()
        {
            oddRects = oddNestCollector.GetComponentsInChildren<RectTransform>(true).ToList();
            oddRects.RemoveAt(0);
            evenRects = evenNestCollector.GetComponentsInChildren<RectTransform>(true).ToList();
            evenRects.RemoveAt(0);
        }

        public void DirectFollowButton(ref UIFollowScript follow, int index, OddEvenDistinction oddEven)
        {
            List<RectTransform> selectedRects = (oddEven == OddEvenDistinction.odd) ? oddRects : evenRects;
            if (index >= selectedRects.Count)
            {
                Debug.LogError("button is out of range");
                return;
            }
            follow.targetRect = selectedRects[index];

        }

        public RectTransform ObtainRectTransform(int index, int maxIndex)
        {
            if (evenRects== null ||  evenRects.Count == 0) Initialize();

            if (maxIndex % 2 == 0) // is even
            {
                //Debug.Log("badges even " + index);
                return evenRects[index];
            }
            else
            {
            // Debug.Log("badges odd " + index);
                return oddRects[index];
            }
        }


    }
}