﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BigOS{
    public class RectLerpScript : MonoBehaviour
    {

        public RectTransform targetRect;
        public RectTransform thisRect;
        public RectTransform baseRect;
        [Header("Under the hood")]
        public OnOffCounter onOff;
        public GameObject newGameObject;

        private void Awake()
        {



            // thisRect = new RectTransform();
            //  if (GetComponent<RectTransform>() == null) Debug.Log("Haven't found rect transform");
            //  else Debug.Log(GetComponent<RectTransform>().ToString());
            //  RectTransform r = GetComponent<RectTransform>();


            //  thisRect.anchoredPosition = r.anchoredPosition;
            //  thisRect.anchorMax = r.anchorMax;
            //  thisRect.anchorMin = r.anchorMin;
            //  thisRect.offsetMax = r.offsetMax;
            //  thisRect.offsetMin = r.offsetMin;
            //  thisRect.pivot = r.pivot;

        }

        // Use this for initialization
        void Start()
        {
            newGameObject = Instantiate(this.gameObject);
            newGameObject.SetActive(false);
            BIGosUtils.DeleteAllChildren(newGameObject);
            baseRect = newGameObject.GetComponent<RectTransform>(); ;
           // baseRect.transform.parent = thisRect.transform.parent;
            thisRect = GetComponent<RectTransform>();
        }

        // Update is called once per frame
        void Update()
        {
            onOff.Update();
            RectUpdate();


        }

        public void RectUpdate()
        {
            if (!targetRect) return;
            thisRect.anchoredPosition = Vector2.Lerp(thisRect.anchoredPosition, targetRect.anchoredPosition, onOff.value);
            thisRect.anchorMax = Vector2.Lerp(baseRect.anchorMax, targetRect.anchorMax, onOff.value);
            thisRect.anchorMin = Vector2.Lerp(baseRect.anchorMin, targetRect.anchorMin, onOff.value);
            thisRect.offsetMax = Vector2.Lerp(baseRect.offsetMax, targetRect.offsetMax, onOff.value);
            thisRect.offsetMin = Vector2.Lerp(baseRect.offsetMin, targetRect.offsetMin, onOff.value);
            thisRect.pivot = Vector2.Lerp(baseRect.pivot, targetRect.pivot, onOff.value);
            thisRect.localScale = Vector3.Lerp(baseRect.localScale, targetRect.localScale, onOff.value);

        }
    }
}