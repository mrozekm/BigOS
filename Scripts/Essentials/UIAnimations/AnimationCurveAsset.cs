﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BigOS{
    [CreateAssetMenu(fileName = "Curve Asset", menuName = "BigOS/Config/Curve Asset", order = 1)]
    public class AnimationCurveAsset : ScriptableObject
    {
    
        public AnimationCurveEntry[] curveEntries;

    #if UNITY_EDITOR
        public void EnumGen(string name)
        {
            EnumGenerator.GenerateEnums(curveEntries, name, true);
        }
    #endif

        [System.Serializable]
        public class AnimationCurveEntry :EnumGenParent
        {
            public AnimationCurve curve;
        }

        public float Evaluate(CurveEnum curve, float fraction)
        {
            return curveEntries[EnumUtil.GetIndex(curve)].curve.Evaluate(fraction);
        }
    }
}


