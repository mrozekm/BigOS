﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BigOS{
    public class AutoEnableUIAnimatorIfInEditor : MonoBehaviour
    {
        bool jobDone = false;
        [SerializeField] UIAnimator targetAnimator;

        // Start is called before the first frame update
        void Start()
        {
            
        }

        // Update is called once per frame
        void Update()
        {
    #if UNITY_EDITOR
            if (!jobDone)
            {
                //Debug.Log("Entering editor");
                targetAnimator.SetEnabled(true);
                targetAnimator.gameObject.SetActive(true);
                //TimeManager.Pause++;
                jobDone = true;
            }
    #endif
        }
    }
}