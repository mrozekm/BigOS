﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BigOS{
    public class UIFollowScript : MonoBehaviour
    {

        public RectTransform targetRect;
        //[HideInInspector]
        public RectTransform thisRect;
        public bool  freeze = false;
        public float speed;

        // Start is called before the first frame update
        void Start()
        {
            thisRect = GetComponent<RectTransform>();

        }

        // Update is called once per frame
        void Update()
        {
            if (!targetRect || freeze) return;
            thisRect.anchoredPosition = BIGosUtils.SimpleTween(targetRect.anchoredPosition, thisRect.anchoredPosition,speed);
            thisRect.anchorMax = BIGosUtils.SimpleTween(targetRect.anchorMax, thisRect.anchorMax, speed);
            thisRect.anchorMin = BIGosUtils.SimpleTween(targetRect.anchorMin, thisRect.anchorMin, speed);
            thisRect.offsetMax = BIGosUtils.SimpleTween(targetRect.offsetMax, thisRect.offsetMax, speed);
            thisRect.offsetMin = BIGosUtils.SimpleTween(targetRect.offsetMin, thisRect.offsetMin, speed);
            thisRect.pivot = BIGosUtils.SimpleTween(targetRect.pivot, thisRect.pivot, speed);
            thisRect.localScale = BIGosUtils.SimpleTween(targetRect.localScale, thisRect.localScale, speed);

        }

        public void ForceGoToDestination()
        {
        // Debug.Log("forcing token " + gameObject.name + " to go somewhere");
        

            if (!thisRect) thisRect = GetComponent<RectTransform>();

            if (targetRect == null || thisRect == null)
                return;
            thisRect.anchoredPosition = targetRect.anchoredPosition;
            thisRect.anchorMax = targetRect.anchorMax;
            thisRect.anchorMin = targetRect.anchorMin;
            thisRect.offsetMax = targetRect.offsetMax;
            thisRect.offsetMin = targetRect.offsetMin;
            thisRect.pivot = targetRect.pivot;
            thisRect.localScale = targetRect.localScale;
        }
    }
}