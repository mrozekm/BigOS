﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BigOS{
    public class UIAnimatorExtendedWithDelay : UIAnimatorExtended {

        public float enterDelay;

        void Start()
        {
            InitAnimator();
            InitCanvasGroup();
        }

        void Update()
        {
            UpdateDelta();
        }

        public override void RunAnim()
        {
            if (!gameObject.activeSelf) return;

        // Debug.LogError("d");

            //InitCanvasGroup();
            if (active)
            {
                if (canvasGroup)
                canvasGroup.alpha = 0;
            // if (rawImage)
            //     rawImage.enabled = false;
            }

            Debug.Log("run delayed anim on " + gameObject.name);
            if (active)
                Invoke("RunAnim", enterDelay);
            else
                base.RunAnim();


        }


    }
}