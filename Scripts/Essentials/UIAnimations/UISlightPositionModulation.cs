﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BigOS{
    public class UISlightPositionModulation : MonoBehaviour
    {
        [SerializeField] AnimationCurve movementCurve;
        [SerializeField] Vector2 transformOffset;
        [SerializeField] float timeExtent;
        [SerializeField] RectTransform thisRectTransform;
        public OnOffCounter enableTween;

        // Start is called before the first frame update
        void Start()
        {
            thisRectTransform = GetComponent<RectTransform>();
        }

        // Update is called once per frame
        void Update()
        {
            enableTween.Update();
            float tyme = Time.timeSinceLevelLoad % timeExtent;
            tyme /= timeExtent; // tyme = 0 - 1;
            float value = movementCurve.Evaluate(tyme);
            //Debug.Log(tyme);
            Vector2 gotoPos = Vector2.Lerp(Vector2.zero, transformOffset, value);
            thisRectTransform.localPosition = Vector2.Lerp(Vector2.zero,gotoPos, enableTween.value);

        }
    }
}