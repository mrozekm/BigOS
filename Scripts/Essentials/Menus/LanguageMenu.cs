﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BigOS.Localization;
using UnityEngine.UI;
using Medicine;

namespace BigOS{
    [Register.Single]
    public class LanguageMenu : MonoBehaviour
    {
        [SerializeField] GameObject menuParent;
        [SerializeField] GameObject flagButtonPrefab;
        [SerializeField] Image[] flagImages;
        [SerializeField] Text seletedLanguageText;
        [Inject.Single]
        [SerializeField] LocalizationCore localizationCore { get; }
        [SerializeField] ParticleSystem swapButtonEffect;
    // [Inject.Single] LocalizationAccess locAccess { get; }
        public UIAnimator animator;
        public UIAnimator coverAnimator;

        // Start is called before the first frame update
        void Start()
        {
        // Debug.LogError("DUPA"); 
            flagButtonPrefab.SetActive(false);
            GenerateLanguageFlagButtons();
            Init(false);
        }
    
        public void Init(bool getLangFromSteam)
        {
            if (getLangFromSteam)
            {
                localizationCore.selectedLanguage = SwitchLanguageBasedOnSteamAtFirstRun.steamBaseLanguage;
                LocalizationReporterHolder.SwitchLanguageInAllUI();
            }

            seletedLanguageText.text = localizationCore.GiveMeLanguageNameInTheNameOfTheLanguage(localizationCore.selectedLanguage);
            SetLocFlags( localizationCore.availableLanguages.availableLanguages[localizationCore.selectedLanguage].flag);
        }

        public void GenerateLanguageFlagButtons()
        {
        // Debug.Log("Generating Flag Buttons");

            int i = 0;
            foreach (Language l in localizationCore.availableLanguages.availableLanguages)
            {
                GameObject newButton = Instantiate(flagButtonPrefab, menuParent.transform);
                newButton.name = "LanguageButton_" + l.name;
                FlagButton flagButtonComponent = newButton.transform.GetChild(0).GetComponent<FlagButton>();
                flagButtonComponent.Init( i);
                newButton.SetActive(true);
            i++;
            }
            menuParent.GetComponent<RectTransform>().sizeDelta = new Vector2(0f,(i+1)*130);
        }

        public void SelectLanguage(string language, Sprite sprite)
        {
            seletedLanguageText.text = language;
            SetLocFlags(sprite);
            if (swapButtonEffect != null)
            swapButtonEffect.Play();
        // AudioSourceCollection.PlaySound(AbilitySoundEnum.SelectLanguageSound);
        }

        public void SetLocFlags(Sprite flagSprite)
        {
            foreach (Image i in flagImages)
            {
                i.sprite = flagSprite;
            }
        }

        public void SetAnimator(bool active)
        {
            animator.SetEnabled(active);
            coverAnimator.SetEnabled(active);
        }


        
    }
}
