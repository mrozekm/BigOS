﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using BigOS.SaveSystem;
using BigOS.Localization;
using Medicine;

namespace BigOS{
    public class FlagButton : MonoBehaviour
    {
        [SerializeField] Image image;
        [SerializeField] Text languageText;
    // [SerializeField] LocEnum_Language buttonLanguage;
        [SerializeField] public int heldLanguage;
        [SerializeField] LanguageMenu lenguageMenu;
        [Inject.Single]
        LocalizationCore localizationCore { get; }



        // Start is called before the first frame update
        void Start()
        {
            
        }

        // Update is called once per frame
        void Update()
        {
            
        }

        public void Init(int incomingLanguage)
        {
            Language lang = localizationCore.availableLanguages.availableLanguages[incomingLanguage];
            // WHAT WE ARE DOING HERE IS WE ARE GETTING SPECIFICALLY THE LANGUAGE NAME IN THE RESPECTIVE LANGUAGE
            languageText.text = localizationCore.GiveMeLanguageNameInTheNameOfTheLanguage(incomingLanguage);
            image.sprite = lang.flag;
            heldLanguage = incomingLanguage;
            GetComponent<Button>().onClick.AddListener(SelectLanguage);
            
        }

        public void SelectLanguage()
        {
            Debug.Log("Selecting language: " + heldLanguage.ToString() + " text:" + languageText + "gameobject name =" + gameObject.name);
        // PersistenceManager.SetIntValue("Language", heldLanguage);
            PlayerPrefs.SetInt(SaveSystemCore.LANGUAGE_KEY, heldLanguage);
            localizationCore.selectedLanguage = heldLanguage;// EnumUtility.GetIndex(buttonLanguage);
            
            LocalizationReporterHolder.SwitchLanguageInAllUI();
            lenguageMenu.SelectLanguage(languageText.text, image.sprite);
        }
    }
}