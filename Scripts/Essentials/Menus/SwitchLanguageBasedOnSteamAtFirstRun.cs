﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BigOS.Localization;
using Steamworks;


namespace BigOS{
    public class SwitchLanguageBasedOnSteamAtFirstRun : MonoBehaviour
    {
        [SerializeField] LocalizationCore localizationCore;
        public static int steamBaseLanguage = 0;

        // Start is called before the first frame update
        void Start()
        {
        // if (!SteamManager.Initialized)
        //  {
        //      Debug.Log("Steam was not initialized to affect language change");
        //       return;
        //   }

        // if (PlayerPrefs.GetInt("LanguageFromSteamDiscovered", 0) == 0 &&  SteamManager.Initialized)
        //  {
        //     PlayerPrefs.SetInt("LanguageFromSteamDiscovered",1);
            
            string lang = SteamApps.GetCurrentGameLanguage();
                Debug.Log("Detected steam language as: " + lang);
                switch (lang)
                {
                    case "english":
                        Debug.Log("set language to english");
                    steamBaseLanguage = 0;
                        break;
                    case "polish":
                        Debug.Log("set language to polish");
                    steamBaseLanguage = 1;
                        break;
                    case "schinese":
                        Debug.Log("set language to chinese");
                    steamBaseLanguage = 2;
                        break;
                    case "tchinese":
                        Debug.Log("set language to chinese");
                    steamBaseLanguage = 2;
                        break;
                    case "japanese":
                        Debug.Log("set language to japanese");
                    steamBaseLanguage = 3;
                        break;
                    case "koreana":
                        Debug.Log("set language to korean");
                    steamBaseLanguage = 4;
                        break;
                    case "thai":
                        Debug.Log("set language to thai");
                    steamBaseLanguage = 5;
                        break;
                    case "russian":
                        Debug.Log("set language to russian");
                    steamBaseLanguage = 6;
                        break;
                    case "german":
                        Debug.Log("set language to german");
                    steamBaseLanguage = 7;
                        break;
                    case "french":
                        Debug.Log("set language to french");
                    steamBaseLanguage = 8;
                        break;
                    case "spanish":
                        Debug.Log("set language to spanish");
                    steamBaseLanguage = 9;
                        break;


                }
            //    PlayerPrefs.SetInt(PersistenceManager.LANGUAGE_KEY, manager.selectedLanguage);
        //       LocalizationReporterHolder.SwitchLanguageInAllUI();

            //}
        }

        // Update is called once per frame
        void Update()
        {
            
        }
    }
}