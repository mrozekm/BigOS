using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using BigOS;
using UnityEngine.Events;
//using BigOS.SaveSystem;

namespace BigOS
{

    public class BigOSMenusManager : MonoBehaviour
    {
        public enum UIState
        {
            StartMenu,
            IngameMenu
        }

        [SerializeField] private UIState uiState;
        [SerializeField] private BigOS.UIAnimator startMenuAnimator;
        [SerializeField] private BigOS.UIAnimator mainMenuAnimator;
        [SerializeField] private BigOS.UIAnimator settingsAnimator;
        [SerializeField] private BigOS.SaveSystem.SaveSlotViewModel saveSlotViewModel;
        [SerializeField] private BigOS.LanguageMenu languageMenuViewModel;
        public UnityAction startGameAction;


        // Start is called before the first frame update
        void Start()
        {
            //STARTING THE GAME NORMALLY
            startMenuAnimator.SetEnabled(true);
        }

        // Update is called once per frame
        void Update()
        {

        }

        public void StartGameFromStartMenu()
        {
            startMenuAnimator.SetEnabled(false);
            saveSlotViewModel.SetAnimator(true);
        }

        public void GoBackFromSaveSlotSelectionToStartMenu()
        {
            startMenuAnimator.SetEnabled(true);
            saveSlotViewModel.SetAnimator(false);
        }



        public void GoToSettingsFromStartMenu()
        {
            uiState = UIState.StartMenu;
            settingsAnimator.SetEnabled(true);
            startMenuAnimator.SetEnabled(false);
        }

        public void GoToLanguageMenuFromStartMenu()
        {
            uiState = UIState.StartMenu;
            startMenuAnimator.SetEnabled(false);
            languageMenuViewModel.SetAnimator(true);
        }

        public void GoToSettingsFromMainMenu()
        {
            uiState = UIState.IngameMenu;
            settingsAnimator.SetEnabled(true);
            mainMenuAnimator.SetEnabled(false);
        }

        public void GoToLanguageMenuFromMainMenu()
        {
            uiState = UIState.IngameMenu;
            languageMenuViewModel.SetAnimator(true);
            mainMenuAnimator.SetEnabled(false);
        }

        public void GoBackFromSettings()
        {
            settingsAnimator.SetEnabled(false);
            ReturnToStartOrMainMenu();
        }

        public void GoBackFromLanguageMenu()
        {
            languageMenuViewModel.SetAnimator(false);
            ReturnToStartOrMainMenu();

        }

        private void ReturnToStartOrMainMenu()
        {
            if (uiState == UIState.IngameMenu)
            {
                mainMenuAnimator.SetEnabled(true);
            }
            else if (uiState == UIState.StartMenu)
            {
                startMenuAnimator.SetEnabled(true);
            }
        }

        public void Quit()
        {
            Application.Quit();
        }

        public void QuitToTitleScreen()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        public virtual void GoBackToGameFromMainMenu()
        {
            mainMenuAnimator.SetEnabled(false);
        }

        public virtual void StartGameAfterSelectingSaveSlot()
        {
            saveSlotViewModel.SetAnimator(false);
            Debug.Log("Start game after selecting save slot");
            startGameAction.Invoke();
        }


    }
}