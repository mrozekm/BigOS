using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using System;

namespace BigOS.Localization{
    [CreateAssetMenu(fileName = "InterlocutorData", menuName = "Localization/InterlocutorData", order = 1)]
    public class InterlocutorData : ScriptableObject
    {
        public List<InterlocutorEntry> interlocutors;
        //public List<string> testList;

        [System.Serializable]
        public class InterlocutorEntry : EnumGenParent
        {
        // public LocEnum_GirlNames targetGirl;
        }
        public List<string> GetInterlocutrs(){
            List<string> list =new List<string>();
            foreach(InterlocutorEntry entry in interlocutors){
                list.Add(entry.name);
            }
            return list;
        }


    }
}

