﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BigOS.Localization{
    public class LocalizationReporter : MonoBehaviour
    {
        [HideInInspector]
        public int category;
        [HideInInspector]
        public int[] entries = new int[256];
        [HideInInspector]
        public LocalizationCore localizationCore;
        [SerializeField]
        Text text;
        [SerializeField]
        public string[] formattingData;
        public string currentTextStash;
        public bool formatActive;
        //  [SerializeField] bool ignoreFont;

        public void SetCategoryEntry(int category, int entry)
        {

            this.category = category;
            this.entries[category] = entry;
        }

        public void InitForEditor()
        {
            if (localizationCore == null) localizationCore = FindObjectOfType<LocalizationCore>();
        }

        public void PassLocDataToText()
        {

        // Debug.Log(gameObject.name);
            if (text == null) text = GetComponent<Text>();
            if (text == null) return;
            string recieved = LocalizationReporterHolder.GetTextOfSelectedLanguage(category, entries[category]);
            currentTextStash = recieved;
            text.text = recieved;
            if (formatActive) FormatText();



        }

    

        public void FormatText()
        {
            // if (text == null)
            //  text = GetComponent<Text>();



            text.text = string.Format(currentTextStash, formattingData);
            text.color = text.color;
            //     if (ignoreFont) return;
            //    text.font = text.font;
            //     text.fontSize = text.fontSize;
            //text.re
        }

        [ContextMenu("debug")]
        public void debug()
        {
            text.text = "debug";
        }

        private void Update()
        {
            //  if (formatActive)
            //      FormatText();
        }

        void Awake()
        {
            MakeSureCategoryAndEntryIsWithinBounds();
            LocalizationReporterHolder.ReportReporter(this);
            text = GetComponent<Text>();
            PassLocDataToText();

        }

        public void MakeSureCategoryAndEntryIsWithinBounds()
        {
            InitForEditor();
            if (category >= localizationCore.vocabularyCore.GetListOfGlobalCategories().Count) category = localizationCore.vocabularyCore.GetListOfGlobalCategories().Count-1;
        }


    }
}
