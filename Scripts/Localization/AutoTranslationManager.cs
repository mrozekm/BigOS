﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.IO;
using UnityEngine.UI;
#if UNITY_EDITOR
using UnityEditor;
#endif
using Newtonsoft.Json;
using System.Text;
using System;
using Medicine;




//public enum FontTypeEnum {Thin, Thick, Handwritten }
namespace BigOS.Localization
{
    
    [Register.Single]
    public class AutoTranslationManager : MonoBehaviour
    {
        //[SerializeField] LocalizationManager localizationManager;

       // static AutoTranslationManager instance;


      

        public TranslationEngine translationEngine;
        private int deltaLanguage;
        [Inject.Single]
        LocalizationCore LocalizationCore { get; }

        [Inject.Single] private Translate Translate { get; }
       //  static public Translate TranslateFromEditor
      //  {
        //    get
        //   {
         //       MakeSureInstanceIsNotNull();
        //        return instance.translate;
        //    }
       // }

        void Awake()
        {
          //  instance = this;
        }

        private void OnEnable()
        {

        }

        private void Start()
        {


        }

        public List<string> GetListOfLanguages()
        {
            List<string> entriesHere = new List<string>();
            foreach (Language l in LocalizationCore.availableLanguages.availableLanguages)
            {
                entriesHere.Add(l.name);
            }
            return entriesHere;
        }

        // Update is called once per frame
        void Update()
        {

        }


        public int AmountOfLanguages()
        {
            return LocalizationCore.availableLanguages.availableLanguages.Count;
        }

#if UNITY_EDITOR

        public string GetCode(int language, TranslationEngine translationEngine)
        {
            return translationEngine == TranslationEngine.google ? LocalizationCore.availableLanguages.availableLanguages[language].googleCode : LocalizationCore.availableLanguages.availableLanguages[language].yandexCode;
        }

#endif

       

      //  public static void MakeSureInstanceIsNotNull()
     //   {
     //       if (instance == null)
      //      {
       //         instance = GameObject.FindObjectOfType<AutoTranslationManager>();
       //     }
     //   }

        public int WhatIsTheIndexOfLanguage(string lang)
        {
           // MakeSureInstanceIsNotNull();
            return LocalizationCore.availableLanguages.availableLanguages.IndexOf(LocalizationCore.availableLanguages.availableLanguages.FirstOrDefault(p => p.name == lang));
        }

        public Language GetLanguage(string name)
        {
           // MakeSureInstanceIsNotNull();
            return LocalizationCore.availableLanguages.availableLanguages.FirstOrDefault(p => string.CompareOrdinal(name, p.name)==0);
        }

        public  List <Language> GetAllLanguages()
        {
         //   MakeSureInstanceIsNotNull();
            return LocalizationCore.availableLanguages.availableLanguages;
        }

        public int GetIndexOfLanguage(Language language){
            return LocalizationCore.availableLanguages.availableLanguages.IndexOf(language);
        }

    }










}

