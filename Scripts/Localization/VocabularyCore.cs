﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.IO;
using UnityEngine.UI;
#if UNITY_EDITOR
using UnityEditor;
using Newtonsoft.Json;
using System.Text;
using System;
//using  Medicine;

#endif

//public enum FontTypeEnum {Thin, Thick, Handwritten }
namespace BigOS.Localization
{

    public class VocabularyCore : MonoBehaviour
    {
        
        static VocabularyCore instance;



  [SerializeField] private LocalizationTree mainLocalizationTree;
       // [SerializeField] private List<LocalizationTree> campaignLocalizationTrees;

        public int selectedLanguage;
        public static int SelectedLanguage
        {
            get
            {
                MakeSureInstanceIsNotNull();
                return instance.selectedLanguage;
            }
            set => instance.selectedLanguage = value;
        }



        void Awake()
        {
            instance = this;
            //  MaybeLoadFromTextFiles();
        }



        private void Start()
        {


        }

      

        


        // Update is called once per frame
        void Update()
        {

        }

        public static string GiveMeLanguageNameInTheNameOfTheLanguage(int numberOfLanguage, int numberOfLanguageCategory)
        {
            // Debug.Log("Give language " + numberOfLanguage + " in category " + numberOfLanguageCategory);
            return instance.mainLocalizationTree.languageBranches[numberOfLanguage].categories[numberOfLanguageCategory].currentEntries[numberOfLanguage].GetString();
        }



        public static void MakeSureInstanceIsNotNull()
        {
            if (instance == null)
            {
                instance = GameObject.FindObjectOfType<VocabularyCore>();
            }
        }



        public static void GlobalSave()
        {
            MakeSureInstanceIsNotNull();
            instance.mainLocalizationTree.Save();
        }

        public static void GlobalLoad()
        {
            MakeSureInstanceIsNotNull();
            instance.mainLocalizationTree.Load();
        
        }

        //--------------------

        public string GetGlobalEntry(int language, int category, int entry)
        {
          //  if (language >= availableLanguages .Count)
         //   {
        //        Debug.LogError("Localization: GetEntry error: " + language);
       //     }

            if (category >= mainLocalizationTree.sourceBranch.categories.Count)
            {
                Debug.LogError("Localization: GetEntry error: " + language + " " + category);
            }
            if (entry >= mainLocalizationTree.languageBranches[language].categories[category].currentEntries.Count)
            {
                Debug.LogError("Localization: GetEntry error: " + language + " " + category + " " + entry);
            }


            return mainLocalizationTree.languageBranches[language].categories[category].currentEntries[entry].GetString();
        }

        public static string GetEntry(int category, int entry)
        {
            return instance.GetGlobalEntry(SelectedLanguage, category, entry);
        }



        public List<string> GetListOfGlobalCategories()
        {
            List<string> categoriesHere = new List<string>();
            foreach (EntryList c in mainLocalizationTree.sourceBranch.categories)
                categoriesHere.Add(c.name);
            return categoriesHere;

        }

        public List<string> GetListOfGlobalEntries(int inCategory)
        {
            List<string> entriesHere = new List<string>();



            foreach (CurrentEntry e in mainLocalizationTree.sourceBranch.categories[inCategory].currentEntries)
            {
                entriesHere.Add(e.name);
            }
            return entriesHere;
        }
#if UNITY_EDITOR
        public void GenerateEnums()
        {
            mainLocalizationTree.GenerateEnums();
           // campaignLocalizationTrees[0].GenerateEnums();
        }
#endif

        public LanguageBranch GetSourceBranch()
        {
            return mainLocalizationTree.sourceBranch;
        }

        public LanguageBranch GetLanguageBranch(int language)
        {
            return mainLocalizationTree.languageBranches[language];
        }
        public LanguageBranch GetLanguageBranch()
        {
            return mainLocalizationTree.languageBranches[selectedLanguage];
        }


    }



}

