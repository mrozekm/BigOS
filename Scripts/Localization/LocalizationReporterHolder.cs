﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Medicine;

namespace BigOS.Localization{
    [Register.Single]
    public class LocalizationReporterHolder : MonoBehaviour
    {
        [Inject.Single]
        public static LocalizationReporterHolder instance {get; }
        public List<LocalizationReporter> reporters;
        public List<ImageBasedOnCurrentLanguage> imagesBasedOnCurrentLanguage;
        [Inject.Single]
        public LocalizationCore localizationCore { get; }
        private int deltaLanguage;



        // Start is called before the first frame update
        void Start()
        {
            StartCoroutine(LateStart());
        }

        IEnumerator LateStart()
        {
            yield return new WaitForSeconds(0.1f);
            SwitchLanguageInAllUI();
        }

        public static void ReportImageBasedOnLanguage(ImageBasedOnCurrentLanguage imageBased)
        {
            if (instance.imagesBasedOnCurrentLanguage == null) instance.imagesBasedOnCurrentLanguage = new List<ImageBasedOnCurrentLanguage>();
            instance.imagesBasedOnCurrentLanguage.Add(imageBased);
            for (int i = instance.imagesBasedOnCurrentLanguage.Count - 1; i > 0; i--)
            {
                if (instance.imagesBasedOnCurrentLanguage[i] == null)
                {
                    instance.imagesBasedOnCurrentLanguage.RemoveAt(i);
                }
            }


        }

        public static void ReportReporter(LocalizationReporter reporter)
        {
            if (instance.reporters == null) instance.reporters = new List<LocalizationReporter>();

            instance.reporters.Add(reporter);
            instance.reporters.TrimExcess();
            // WELL THIS WORKS FINE - not slowing down
            for (int i = instance.reporters.Count - 1; i > 0; i--)
        {
                if (instance.reporters[i] == null)
                {
                    instance.reporters.RemoveAt(i);
                }
            }
        }

        public static string GetTextOfSelectedLanguage(int category, int entry)
        {
        // if (instance == null) instance = FindObjectOfType<LocalizationReporterHolder>();

            int currentLanguage = 0;
            currentLanguage = instance.localizationCore.selectedLanguage;
            return instance.localizationCore.vocabularyCore.GetGlobalEntry(currentLanguage, category, entry);
        }
        // Update is called once per frame
        void Update()
        {
            //if (deltaLanguage != LocalizationManager.SelectedLanguage)
            //    SwitchLanguageInAllUI();
            //deltaLanguage = LocalizationManager.SelectedLanguage;
        }

        public static void SwitchLanguageInAllUI()
        {
            //instance.localizationManagerDefinition.MaybeLoadFromTextFileForSelectedLanguage();

            //Debug.Log("refreshing reporters");
            foreach (LocalizationReporter reporter in instance.reporters)
            {
                if (reporter == null) continue;
            // Debug.Log(reporter.gameObject.name +  " reporter");
                reporter.PassLocDataToText();
            }

            //foreach (ImageBasedOnCurrentLanguage image in instance.imagesBasedOnCurrentLanguage)
            //{
            //    image.Check();
            //}
        }
    }
}
