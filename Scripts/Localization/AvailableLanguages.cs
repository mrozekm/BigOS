using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

namespace BigOS.Localization
{
    [CreateAssetMenu(fileName = "AvailableLanguages", menuName = "BigOS/Localization/AvailableLanguages", order = 1)]
    public class AvailableLanguages : ScriptableObject
    {
        public List<Language> availableLanguages;


       
    }

    [System.Serializable]
    public class Language : EnumGenParent
    {

        public string googleCode;
        public string yandexCode;
        public Sprite flag;
        public string languageNameInTheNameOfTheLanguage;
    }
}
