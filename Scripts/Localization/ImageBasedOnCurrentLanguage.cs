﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BigOS.Localization{
    public class ImageBasedOnCurrentLanguage : MonoBehaviour
    {
        [SerializeField] List<Sprite> spritesToSubstitute;

        bool substituted = false;
        // Start is called before the first frame update

        private void Awake()
        {
            LocalizationReporterHolder.ReportImageBasedOnLanguage(this);
        }

        void Start()
        {
            //Invoke("Check", 0.1f);
        }

        //public void Check()
        //{
            //Debug.Log("substituning texture in object " + gameObject.name + " with language number " + LocalizationManager.SelectedLanguage);
        //    GetComponent<Image>().sprite = spritesToSubstitute[LocalizationManager.SelectedLanguage];
        //}

        // Update is called once per frame
        void Update()
        {
            
        }
    }
}