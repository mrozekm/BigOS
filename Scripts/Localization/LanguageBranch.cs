using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using System;
using System.IO;
using System.Linq;
//using LocalizationSystem;

namespace BigOS.Localization
{
    [CreateAssetMenu(fileName = "LanguageBranch", menuName = "BigOS/Localization/LanguageBranch", order = 1)]
    public class LanguageBranch : ScriptableObject
    {
       // public string name;
        public TextAsset associatedTextAsset;

       // [NonReorderable]
        public List<EntryList> categories;
        public LocalizationTree parent;

        public void LoadFromTextFileMaybeDuringRuntimeInBuild()
        {
            List<string> fullSource = GetStringListOfLanguage(associatedTextAsset.name);
            if (fullSource != null)
            {
                LocalizationUtils.RemoveEmptyEntriesInStringList(ref fullSource);
                LocalizationUtils.PharseForLocalizationBranch(fullSource, out categories, parent.sourceBranch.categories);
                // WE DONT WANT TO OVERWRITE WHATEVER IS THERE
                for (int i = 0; i < categories.Count; i++)
                {
                    int amountOfEntires = categories[i].currentEntries.Count();
                    for (int j = 0; j < amountOfEntires; j++)
                    {
                        categories[i].currentEntries[j].name = categories[i].currentEntries[j].name;
                    }
                }
            }
        }

        #region from post processor

        public static string GetLocalizationFolder()
        {
#if UNITY_EDITOR
            return Application.dataPath + "/Resources/Localization/Custom";
#else
        return Application.dataPath + "/Localization/Custom"; 
#endif
        }

        public static List<string> GetStringListOfLanguage(string filename)
        {
            string filePath = GetLocalizationFolder() + "/" + filename + ".txt";
            Debug.Log("reading file: " + filePath);

            if (File.Exists(filePath))
            {
                Debug.Log("file exists: " + filePath);
                string[] textInput = File.ReadAllLines(filePath);
                return textInput.ToList();
            }
            else return null;
        }

        #endregion



#if UNITY_EDITOR

        public void Load()
        {
            // string finalPath = Path.Combine( LocalizationManager.saveFolderName, associatedTextAsset.name) + ".txt";
            List<string> fullSource = LocalizationUtils.GetRawStringListFromLocFile(associatedTextAsset);
            LocalizationUtils.RemoveEmptyEntriesInStringList(ref fullSource);
            LocalizationUtils.PharseForLocalizationBranch(fullSource, out categories, parent.sourceBranch.categories);
            EditorUtility.SetDirty(this);
            AssetDatabase.SaveAssets();

        }
        public void Save()
        {
            parent.SaveSpecific(associatedTextAsset.name);
            EditorUtility.SetDirty(this);
            AssetDatabase.SaveAssets();
        }
        public void Propagate()
        {
            parent.Propagate(false);
        }

        public void SaveThisSpecificTextAsset()
        {
            parent.SaveSpecificToHeldTextAsset(this);
        }


#endif


        public void GetString<T>  (T cat, T entry) where T : struct,  IConvertible
        {
            if (!typeof(T).IsEnum)
            {
                throw new ArgumentException("T must be an enumerated type");
            }
            // TODO: Do it someday
            //return categories[EnumUtility.GetIndex(LocalizationCategoriesEnum.ItemFunctions)].currentEntries[EnumUtility.GetIndex(LocEnum_ItemFunctions.IncreaseAbilityLevel)].value
        }

        public string GetString(int cat, int entry)
        {
            return categories[cat].currentEntries[entry].autoTranslation;
        }

        public List <string> GetStringListOfCategory(int cat)
        {
            List<string> textList = new List<string>();
            foreach (CurrentEntry e in categories[cat].currentEntries)
            {
                textList.Add(e.name);
            }
            return textList;
        }

        public List<string> GetStringListOfCategoryValues(int cat)
        {
            List<string> textList = new List<string>();
            foreach (CurrentEntry e in categories[cat].currentEntries)
            {
                textList.Add(e.autoTranslation);
            }
            return textList;
        }

    }
}
