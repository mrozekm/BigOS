﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

namespace BigOS.Localization
{

    //--------------------------------------------------------------------------------------------
    [System.Serializable]
    public class Category
    {
        public string name;
    }


    [System.Serializable]
    public class EntryList
    {
        public string name;
        //[NonReorderable]
        public List<CurrentEntry> currentEntries;
#if UNITY_EDITOR
        [JsonConstructor]
#endif
        public EntryList()
        {

        }
    }

    [System.Serializable]
    public class CurrentEntryWrapper
    {
        public string language;
        public int categoryIndex;
        public int entryIndex;
        public CurrentEntry currentEntry;
    }


    [System.Serializable]
    public class CurrentEntry
    {

        public string name;
        [TextArea]
        [SerializeField]
        public string autoTranslation;
        [TextArea]
        [SerializeField]
        public string humanTranslation;
        public bool disableForHumanTranslation;

#if UNITY_EDITOR
        [JsonConstructor]
#endif
        public CurrentEntry()
        {

        }

        public CurrentEntry(string locString)
        {
            string[] strings = locString.Split("="[0]);
            if (strings.Length != 2) Debug.LogError(locString + " string entry in wrong format");
            name = strings[0].Trim();
            autoTranslation = strings[1].Trim();

        }


        public void DefineValue(string value)
        {
            this.autoTranslation = value;
        }

        public string GetString()
        {
            if (humanTranslation.Length == 0)
            {
                return autoTranslation;
            }
            else
            {
                return humanTranslation;
            }
        }

        

    }



}
