﻿using System.IO;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace BigOS.Localization{
    public class IDHelper : MonoBehaviour
    {


        //  public static readonly String DEFAULT_ALPHABET = "0123456789qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM$#";
        //public static readonly int HUMAN_BYTE_LENGTH = 6;

        private static readonly String DEFAULT_ALPHABET = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM";
        private static uint[] POWERS = new uint[]{380204032, 7311616, 140608, 2704, 52, 1};

        public static uint GetRandomInt32Number()
        {
            int baseInt = UnityEngine.Random.Range(int.MinValue, int.MaxValue);

            return (uint)baseInt;
        }
        
        [ContextMenu("Run")]
        void Main()
        {
            uint num = GetRandomInt32Number();
            //Debug.Log(num);
            string sig = ToSignature(num);
            //Debug.Log(sig);
            uint num2 = ToUint(sig);
            //Debug.Log(num2);

        }

        public static string ToSignature(uint num)
        {
            uint[] QuinquagiDuoNotation = new uint[6]; //Better name for a 52-decimal notation?
            uint moduloRest = num;
            for (int i = 0; i < 6; i++)
            {
                QuinquagiDuoNotation[i] = moduloRest / POWERS[i];
                moduloRest = moduloRest % POWERS[i];
            //   Debug.Log(QuinquagiDuoNotation[i] + " / " + moduloRest);
            }

            string outString = "";
            for (int i = 0; i < 6; i++)
            {
                outString = outString + DEFAULT_ALPHABET[(int)QuinquagiDuoNotation[i]];
            }
            return outString;
        }

        public static uint ToUint(string sig)
        {
            if (sig.Length != 6)
            {
                Debug.LogError("wrong signature length, should be 6 and is: " + sig.Length);
            // return 0;
            }
            uint[] QuinquagiDuoNotation = new uint[6]; //Better name for a 52-decimal notation?
            uint finalUint = 0;
            for (int i = 0; i < 6; i++)
            {
                QuinquagiDuoNotation[i] = (uint)DEFAULT_ALPHABET.IndexOf(sig[i]);
                //Debug.Log(QuinquagiDuoNotation[i]);
                finalUint += QuinquagiDuoNotation[i] * POWERS[i];
            }
            return finalUint;
        }

        public static uint IntToUInt(int value)
        {
            return System.Convert.ToUInt32(value + int.MaxValue);
        }

        public static int UIntToInt(uint value)
        {
            return System.Convert.ToInt32(value) - int.MaxValue;
        }

    }
}

