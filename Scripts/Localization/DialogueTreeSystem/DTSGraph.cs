using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BigOS.XNode;
using BigOS.Localization.DialogueSystem;

namespace BigOS.Localization.DialogueSystem{
    
    [CreateAssetMenu(fileName = "DialogueGraph", menuName = "BigOS/Localization/DialogueGraph", order = 1)]
    public class DTSGraph:NodeGraph
    {
        private List<Node> startNodes;

        public List<Node> GetStarts(){
            startNodes=new List<Node>();
            foreach(Node node in this.nodes){
                if(node is Start){
                    startNodes.Add(node);
                }
            }
            return startNodes;
        }
        

    }
}

