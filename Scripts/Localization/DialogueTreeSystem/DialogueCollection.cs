using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BigOS.Localization.DialogueSystem;
using UnityEditor;


[CreateAssetMenu(fileName = "DialogueCollection", menuName = "Config/DialogueCollection", order = 1)]
public class DialogueCollection : ScriptableObject
{
 public List<DTSGraph> graphs;
}
