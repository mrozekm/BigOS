using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using System;
using BigOS.Localization;

namespace BigOS.Localization{
    [CreateAssetMenu(fileName = "DialogueSource", menuName = "Config/DialogueSource", order = 1)]

    public class DialogueSource : ScriptableObject
    {
        public string lastUpdate;
        //[SerializeField]
        public List<DialLocEntry> sourceEntries;
    }
}
