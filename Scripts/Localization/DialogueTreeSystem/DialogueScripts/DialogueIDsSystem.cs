﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
#if GoogleFairy
using BigOS.GoogleFairy;
#endif
using Newtonsoft.Json;
using BigOS.XNode;
using BigOS.SimpleJSON;
using System.Linq;
using Medicine;
using UnityEngine.Serialization;


namespace BigOS.Localization.DialogueSystem
{
    [Register.Single]
    public class DialogueIDsSystem : MonoBehaviour
    {
        /*
        private static DialogueIDsSystem instance;
        public static DialogueIDsSystem Instance 
        {
            get
            {
                if(instance==null)
                    instance=(DialogueIDsSystem)FindObjectOfType(typeof(DialogueIDsSystem));
                return instance;
            }
        }
        */
#if GoogleFairy
        [Inject.Single]public GoogleFairyCore googleFairyCore {get;}
#endif
        [Header("Setup")] public string languageLocationInProject = "Assets/config/Localization";
        public DialogueSource source;
        public List<DialogueLanguageData> dialogueLanguageCollection;
        public string tableName;
        [SerializeField] Translate translate;
        [FormerlySerializedAs("dialogueTreeSystem")] public VocabularyCore vocabularyCore;
        //public Interlo
        //public string lastUpdate;
        public string testJSON;


        public static Dictionary<uint,DialLocEntry> IDsDictionary = new Dictionary<uint, DialLocEntry>();

        //This method is returns text contnet (messages and answers) by ID and choosen language. It is also selecting translation
        //using hierarchy: human translated->machine translated->dialogue source
        public string GetContentByIdAndLanguage(uint id,int language){
            string toReturn="";
            if(dialogueLanguageCollection[language].humanTranslated.Any(x=>x.id==id)){
                if(dialogueLanguageCollection[language].humanTranslated.Where(x=>x.id==id).SingleOrDefault().content!=""){
                    toReturn=dialogueLanguageCollection[language].humanTranslated.Where(x=>x.id==id).SingleOrDefault().content;
                    return toReturn;
                }    
            }
            if(dialogueLanguageCollection[language].googleTranslated.Any(x=>x.id==id)){
                if(dialogueLanguageCollection[language].googleTranslated.Where(x=>x.id==id).SingleOrDefault().content!=""){
                    toReturn=dialogueLanguageCollection[language].googleTranslated.Where(x=>x.id==id).SingleOrDefault().content;
                    return toReturn;
                }
            }
            toReturn=source.sourceEntries.Where(x=>x.id==id).SingleOrDefault().content;
            return toReturn;
        }
        
        public static bool IsContentRepeated(string content){
            //Debug.Log(IDsDictionary.Count +" "+content);
            foreach(KeyValuePair<uint,DialLocEntry> entry in IDsDictionary){
                //Debug.Log(entry.Value.content);
                if(String.Compare(entry.Value.content,content)==0){
                    return true;
                }
            }
            return false;
        }
#if GoogleFairy
        public void GetUpdateDate(bool runtime){
            //Debug.Log("GetUpdateDate");
            Drive.responseCallback += HandleDriveResponse;
            Drive.GetUpdateDate(tableName,googleFairyCore.driveConnectionLocalization.connectionData.spreadsheetId,runtime);
        }
        
        public void CreateSpreadsheet(){
            Debug.Log("CreateSpreadsheet");

            Drive.responseCallback += HandleDriveResponse;
            string[] fieldNames = new string[2];
                fieldNames[0] = "ID";
                fieldNames[1] = "content";
            Drive.CreateTable(fieldNames,tableName, false);
        }


        public void ExportToSpreadsheet(){
            Debug.Log("ExportToSpreadsheet");

            Drive.responseCallback += HandleDriveResponse;
            List<DialogueCloudEntry> dialogueCloudEntries=new List<DialogueCloudEntry>();

            foreach(DialLocEntry entry in source.sourceEntries){
                dialogueCloudEntries.Add(new DialogueCloudEntry{ID=IDHelper.ToSignature(entry.id),content=entry.content});
            }
            string jsonClound=JsonConvert.SerializeObject(dialogueCloudEntries);
            //Debug.Log(jsonClound);
            BraveNewJson.FormatForCloud(ref jsonClound);
            Debug.Log(jsonClound);
            
            Drive.UpdateList(jsonClound,tableName,googleFairyCore.driveConnectionLocalization.connectionData.spreadsheetId,false);
            //Drive.UpdateList(testJSON, tableName, false);
        }

        public void ImportFromSpreadsheet(bool runtime){
            //Debug.Log("ImportFromSpreadsheet");

            Drive.responseCallback += HandleDriveResponse;
            Drive.GetTable(tableName,runtime);
        }


        private void HandleDriveResponse(Drive.DataContainer dataContainer)
        {
            if (dataContainer.objType != tableName)
                return;
    
            if (dataContainer.QueryType == Drive.QueryType.getTable)
            {
                string rawJSon = dataContainer.payload;
                //Debug.Log("Data from Google Drive received.");
                var parsedJSon=JSON.Parse(rawJSon);
                StoreHumanTranslationData(parsedJSon);
                //Debug.Log(rawJSon); 
            }
            if(dataContainer.QueryType == Drive.QueryType.getUpdateDate)
            {
                string rawJSon = dataContainer.value;
                //Debug.Log("Update date: "+rawJSon);
                if(String.Compare(rawJSon,source.lastUpdate)!=0){
                    source.lastUpdate=rawJSon;
                    bool runtime=false;
                    if(Application.isPlaying){
                        runtime=true;
                    }
                    ImportFromSpreadsheet(runtime);
                    Debug.Log("Dialogues has been updated");
                }else{
                    Debug.Log("Dialogues up-to-date");
                } 
            }

            if (dataContainer.QueryType != Drive.QueryType.createTable || dataContainer.QueryType != Drive.QueryType.createObjects || dataContainer.QueryType != Drive.QueryType.getUpdateDate)
            {
                //Debug.Log(dataContainer.msg);
            }

            Drive.responseCallback -= HandleDriveResponse;
        }
#endif      

        public static uint GenerateID(){
            System.Random random=new System.Random();
            int intRand=random.Next();
            uint uintRand=(uint)intRand;
            while(IDsDictionary.ContainsKey((uintRand)) || intRand == 0 || uintRand == 0 ) // WE DON'T WANT TO HAVE AN ID THAT'S 0 BECAUSE IF WE SEE THAT AN ID IS 0 WE ASSUME IT'S NOT THERE IN PERSISTENCE
            {
                System.Random random_temp=new System.Random();
                intRand=random_temp.Next();
                uintRand=(uint)intRand;
            }
            IDsDictionary.Add(uintRand,new DialLocEntry());
            return uintRand;
        }

#if UNITY_EDITOR
        //DOMINIK - APPARENTLY THIS IS EDITOR ONLY?
        public void StoreAutomaticTranslationData(){
            for(int i=0;i<dialogueLanguageCollection.Count;i++){
                dialogueLanguageCollection[i].googleTranslated=new List<DialLocEntry>();
                for(int j=0;j<source.sourceEntries.Count;j++){
                    DialLocEntry tempEntry=new DialLocEntry();
                    tempEntry.id=source.sourceEntries[j].id;
                    //LocalizationManager.Instance.
                    translate.TranslateEntry(tempEntry.DefineEntry,source.sourceEntries[j].content,i,translate.localzationManager.translationEngine);
                    dialogueLanguageCollection[i].googleTranslated.Add(tempEntry);
                }
            }
        }

#endif

        private void StoreHumanTranslationData(JSONNode parsedJSon){
            foreach(DialogueLanguageData dialogueLanguageData in dialogueLanguageCollection){
                dialogueLanguageData.humanTranslated=new List<DialLocEntry>();
            }
            foreach(var field in parsedJSon){
                var value = field.Value;
                for(int i=0;i<dialogueLanguageCollection.Count;i++){
                    dialogueLanguageCollection[i].humanTranslated.Add(new DialLocEntry{id=IDHelper.ToUint(value[0]),content=value[i+2]});
                }
            }
        }

        public void AddDialLocEntryToDialogueSource(Message message){
            DialLocEntry dialLocEntry=new DialLocEntry();
            dialLocEntry.id=message.GetID();
            dialLocEntry.content=message.content;
            foreach(DialLocEntry entry in source.sourceEntries){
                if(entry.id==message.GetID())
                {
                    return;
                }
            }
            source.sourceEntries.Add(dialLocEntry);
        }
        public void AddDialLocEntryToDialogueSource(Answer answer){
            DialLocEntry dialLocEntry=new DialLocEntry();
            dialLocEntry.id=answer.GetID();
            dialLocEntry.content=answer.content;
            foreach(DialLocEntry entry in source.sourceEntries){
                if(entry.id==answer.GetID())
                {
                    return;
                }
            }
            source.sourceEntries.Add(dialLocEntry);
        }
        public void AddToIDsDictionary(Message message){
            if(DialogueIDsSystem.IDsDictionary.ContainsKey(message.GetID())){
                DialLocEntry dialLocEntry=new DialLocEntry();
                dialLocEntry.id=message.GetID();
                dialLocEntry.content=message.content;
                DialogueIDsSystem.IDsDictionary[message.GetID()] =dialLocEntry;
            }else{
                Debug.Log("IDsDictionary key error");
            }
        }
        public void AddToIDsDictionary(Answer answer){
            if(DialogueIDsSystem.IDsDictionary.ContainsKey(answer.GetID())){
                DialLocEntry dialLocEntry=new DialLocEntry();
                dialLocEntry.id=answer.GetID();
                dialLocEntry.content=answer.content;
                DialogueIDsSystem.IDsDictionary[answer.GetID()] =dialLocEntry;
            }else{
                Debug.Log("IDsDictionary key error");
            }
        }
        public void RecreateIDsDictionary(bool debug, List<DTSGraph> graphsList){
            DialogueIDsSystem.IDsDictionary=new Dictionary<uint, DialLocEntry>();
            foreach(DTSGraph graph in graphsList){
                foreach(Node node in (graph as NodeGraph).nodes){
                    if(node is Message){
                        DialLocEntry dialLocEntry=new DialLocEntry();
                        dialLocEntry.id=(node as Message).GetID();
                        dialLocEntry.content=(node as Message).content;
                        DialogueIDsSystem.IDsDictionary.Add((node as Message).GetID(), dialLocEntry);
                    }
                    if(node is Answer){
                        DialLocEntry dialLocEntry=new DialLocEntry();
                        dialLocEntry.id=(node as Answer).GetID();
                        dialLocEntry.content=(node as Answer).content;
                        DialogueIDsSystem.IDsDictionary.Add((node as Answer).GetID(), dialLocEntry);
                    }
                }
            }
            if(debug)Debug.Log("IDsDictionary count:"+ DialogueIDsSystem.IDsDictionary.Count);
        }
        

        public void ClearIDsDictionary(){
            DialogueIDsSystem.IDsDictionary=new Dictionary<uint, DialLocEntry>();
        }
        public void ClearDialogueSource(){
            if (source == null) return;
            source.sourceEntries=new List<DialLocEntry>();
        }

     
    }

    
    [Serializable]
    public class DialogueCloudEntry{
        public string ID;
        public string content;
    }

    [Serializable]
    public class Empty { }
}
