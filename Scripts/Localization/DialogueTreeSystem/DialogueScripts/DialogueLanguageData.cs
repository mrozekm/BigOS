using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using BigOS.Localization;

namespace BigOS.Localization{
    [CreateAssetMenu(fileName = "DialogueLanguageData", menuName = "Localization/DialogueLanguageData", order = 1)]
    public class DialogueLanguageData : ScriptableObject
    {
        //public string language;
        public List<DialLocEntry> googleTranslated;
        public List<DialLocEntry> humanTranslated;
    }
}
