using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BigOS;
using BigOS.XNode;
using System;
using System.IO;
#if GoogleFairy
using BigOS.GoogleFairy;
#endif
using Newtonsoft.Json;
using System.Linq;
#if UNITY_EDITOR
using UnityEditor;
#endif
using Medicine;

namespace BigOS.Localization.DialogueSystem
{

    
    [Register.Single]
    public class DialogueCore : MonoBehaviour
    {


        [Inject.Single]
        ProgressManager progressManager { get; }
        private static DialogueCore instance; //{ get; }
        
           public static DialogueCore Instance 
           {
               get
               {
                   if(instance==null)
                       instance=(DialogueCore)FindObjectOfType(typeof(DialogueCore));
                   return instance;
               }
           }
           
        //[Header("Setup")]
        //public List<Interlocutor> interlocutors;
        //[SerializeField] DialogueSource dialogueSource;
        //public string tableName;
        //[SerializeField] List<DialogueLanguageData> dialogueLanguageDatas;
        //[SerializeField] List<HumanTranslation> translationData;

        //[Header("Dependencies")]
        [Inject.Single]
        DialogueIDsSystem dialogueIDsSystem { get; }
        
        public List<Node> startNodes;
        public List<DTSGraph> dtsGraphs = DTSGraphs;
     //   public InterlocutorData interlocutorData;

        private void Awake()
        {
            GenerateDialoguesFromGraph();
        }


        public List<string> GetParametersList()
        {
            List<string> tmpList = new List<string>();
            foreach (ProgressBool param in progressManager.progressBools)
            {
                tmpList.Add(param.name);
            }
            return tmpList;
        }

        public bool GetParameterStatus(string parameter)
        {
            //bool toReturn=true;
            ProgressBool pb = progressManager.progressBools.FirstOrDefault(x => x.name == parameter);
            return pb.status;
        }

        public void GetAllGraphs()
        {
            DTSGraphs = new List<DTSGraph>(Resources.FindObjectsOfTypeAll<DTSGraph>());
            dtsGraphs = DTSGraphs;
        }

     //   public InterlocutorData GetInterlocutorData()
   //     {
     //       return instance.interlocutorData;
     //   }

        public void GenerateDialoguesFromGraph()
        {
Debug.Log("Generate Dialogues From Graph");
            GetAllGraphs();
            startNodes = new List<Node>();
            foreach (DTSGraph graph in DTSGraphs)
            {
                #if UNITY_EDITOR
                            EditorUtility.SetDirty(graph);
                #endif
                startNodes.AddRange(graph.GetStarts());
                #if UNITY_EDITOR
                           AssetDatabase.SaveAssets();
                #endif
            }

            if (startNodes.Count > 0)
            {
                dialogueIDsSystem.RecreateIDsDictionary(false, dtsGraphs);
                dialogueIDsSystem.ClearDialogueSource();
                foreach (Node node in startNodes)
                {
                    GetDataFromGraph(node.GetOutputPort("exit"), 0);
                }
            }
            else
            {
                Debug.Log("No Start Nodes in graph");
            }
            Debug.Log("Dialogue structure generated");

        }
#if GoogleFairy
        public void ImportDataFromSpreadsheet(bool runtime)
        {
            dialogueIDsSystem.ImportFromSpreadsheet(runtime);

        }
        public void MaybeImportDataFromSpreadsheet(bool runtime)
        {
            dialogueIDsSystem.GetUpdateDate(runtime);

        }
#endif
        public void DebugDialogueStructure()
        {
            string dialoguetxt = "";
            foreach (Node node in startNodes)
            {
                GetDataFromGraphAndSource(node.GetOutputPort("exit"), 0, ref dialoguetxt);
            }
            Debug.Log(dialoguetxt);

        }

        private void GetDataFromGraphAndSource(NodePort port, int level, ref string dialoguetxt)
        {
            List<NodePort> connections = port.GetConnections();
            foreach (NodePort nodeport in connections)
            {
                if (nodeport.node is Message)
                {
                    foreach (DialLocEntry entry in dialogueIDsSystem.source.sourceEntries)
                    {
                        if ((nodeport.node as Message).GetID() == entry.id)
                        {
                            dialoguetxt = dialoguetxt + GetIndentation(level) + "[M]" + (nodeport.node as Message).content + "\n";
                        }
                    }
                    NodePort outPort = nodeport.node.GetOutputPort("exit");
                    if (outPort.ConnectionCount > 0)
                    {
                        GetDataFromGraphAndSource(outPort, level + 1, ref dialoguetxt);
                    }
                }
                if (nodeport.node is Answer)
                {
                    foreach (DialLocEntry entry in dialogueIDsSystem.source.sourceEntries)
                    {
                        if ((nodeport.node as Answer).GetID() == entry.id)
                        {
                            dialoguetxt = dialoguetxt + GetIndentation(level) + "[A]" + (nodeport.node as Answer).content + "\n";
                        }
                    }
                    NodePort outPort = nodeport.node.GetOutputPort("exit");
                    if (outPort.ConnectionCount > 0)
                    {
                        GetDataFromGraphAndSource(outPort, level + 1, ref dialoguetxt);
                    }
                }
                if (nodeport.node is EmptyMessage)
                {
                    NodePort outPort = nodeport.node.GetOutputPort("exit");
                    dialoguetxt = dialoguetxt + GetIndentation(level) +   "\n";
                    if (outPort.ConnectionCount > 0)
                    {
                        GetDataFromGraphAndSource(outPort, level + 1, ref dialoguetxt);
                    }
                }
                if (nodeport.node is Thru)
                {
                    NodePort outPort = nodeport.node.GetOutputPort("exit");
                    dialoguetxt = dialoguetxt + GetIndentation(level) + "[THRU]" + (nodeport.node as Thru).content;
                    if (outPort.ConnectionCount > 0)
                    {
                        GetDataFromGraphAndSource(outPort, level, ref dialoguetxt);
                    }
                }
                if (nodeport.node is If)
                {
                    NodePort outPortTrue = nodeport.node.GetOutputPort("exitTrue");
                    NodePort outPortFalse = nodeport.node.GetOutputPort("exitFalse");
                    dialoguetxt = dialoguetxt + GetIndentation(level) + "[IF]";
                    if (outPortTrue.ConnectionCount > 0)
                    {
                        //string dialoguetxt_true=dialoguetxt+"[TRUE]";
                        GetDataFromGraphAndSource(outPortTrue, level, ref dialoguetxt);
                    }
                    if (outPortFalse.ConnectionCount > 0)
                    {
                        //dialoguetxt=dialoguetxt+"[TRUE]";
                        GetDataFromGraphAndSource(outPortFalse, level, ref dialoguetxt);
                    }
                }
            }
        }


        private string GetIndentation(int level)
        {
            string intendation = "";
            for (int i = 0; i < level; i++)
            {
                intendation = intendation + "-";
            }
            return intendation;
        }

        private void GetDataFromGraph(NodePort port, int level)
        {
            List<NodePort> connections = port.GetConnections();
            foreach (NodePort nodeport in connections)
            {

                if (nodeport.node is Start) { Debug.Log("start"); }
                else if (nodeport.node is Message)
                {
                    //Debug.Log("[level: "+level+"] MESSAGE: "+(nodeport.node as Message).content);
                    dialogueIDsSystem.AddToIDsDictionary(nodeport.node as Message);
                    dialogueIDsSystem.AddDialLocEntryToDialogueSource(nodeport.node as Message);
                    NodePort outPort = nodeport.node.GetOutputPort("exit");
                    if (outPort.ConnectionCount > 0)
                    {
                        GetDataFromGraph(outPort, level + 1);
                    }
                }
                else if (nodeport.node is Answer)
                {
                    //Debug.Log("[level: "+level+"] ANSWER: "+(nodeport.node as Answer).content);
                    dialogueIDsSystem.AddToIDsDictionary(nodeport.node as Answer);
                    dialogueIDsSystem.AddDialLocEntryToDialogueSource(nodeport.node as Answer);
                    NodePort outPort = nodeport.node.GetOutputPort("exit");
                    if (outPort.ConnectionCount > 0)
                    {
                        GetDataFromGraph(outPort, level + 1);
                    }
                }
                else if (nodeport.node is EmptyMessage)
                {
                    NodePort outPort = nodeport.node.GetOutputPort("exit");
                    if (outPort.ConnectionCount > 0)
                    {
                        GetDataFromGraph(outPort, level + 1);
                    }
                }
                else if (nodeport.node is Thru)
                {
                    NodePort outPort = nodeport.node.GetOutputPort("exit");
                    if (outPort.ConnectionCount > 0)
                    {
                        GetDataFromGraph(outPort, level + 1);
                    }
                }
                else if (nodeport.node is If)
                {
                    NodePort outPortTrue = nodeport.node.GetOutputPort("exitTrue");
                    NodePort outPortFalse = nodeport.node.GetOutputPort("exitFalse");
                    if (outPortTrue.ConnectionCount > 0)
                    {
                        GetDataFromGraph(outPortTrue, level + 1);
                    }
                    if (outPortFalse.ConnectionCount > 0)
                    {
                        GetDataFromGraph(outPortFalse, level + 1);
                    }
                }
                else if (nodeport.node is DialogueNode)
                {
                    NodePort outPort = nodeport.node.GetOutputPort("exit");
                    if (outPort.ConnectionCount > 0)
                    {
                        GetDataFromGraph(outPort, level + 1);
                    }
                }
                else
                {
                    NodePort outPort = nodeport.node.GetOutputPort("exit");
                    if (outPort.ConnectionCount > 0)
                    {
                        GetDataFromGraph(outPort, level + 1);
                    }
                }
            }
        }
        public void DebugIDsDictionary()
        {
            dialogueIDsSystem.RecreateIDsDictionary(false, dtsGraphs);
            Debug.Log("IDsDictionary count:" + DialogueIDsSystem.IDsDictionary.Count);
            foreach (KeyValuePair<uint, DialLocEntry> entry in DialogueIDsSystem.IDsDictionary)
            {
                Debug.Log("[ID:" + entry.Key + "] " + entry.Value.content);
            }
        }


        //-------STATIC METHODS
        public static List<DTSGraph> DTSGraphs = new List<DTSGraph>();
        //public static InterlocutorData InterlocutorData = instance.interlocutorData;
    }
}
//}

//[Serializable]
//public class Interlocutor
//{
//    public string name;
//    public Texture texture;
//}



