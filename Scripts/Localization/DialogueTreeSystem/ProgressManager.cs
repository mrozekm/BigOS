using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Medicine;


namespace BigOS.Localization.DialogueSystem
{
    [Register.Single]
    public class ProgressManager : MonoBehaviour
    {
        public List<ProgressBool> progressBools;
        void Update(){}
    }

    [Serializable]
    public class ProgressBool{
        public string name;
        public bool status;
    }
}
