using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BigOS.XNode;
using BigOS.Localization.DialogueSystem;

[NodeTint(205, 0, 51)]
public class AddDecorator : DialogueNode
{
    [TextArea(1,15)]
    public string content;
    [Output] public string add;

    
}