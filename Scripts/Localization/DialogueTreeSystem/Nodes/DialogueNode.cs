﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BigOS.XNode;

namespace BigOS.Localization.DialogueSystem
{

    public abstract class DialogueNode : Node
    {
        [Input] public Empty enter;
        [Output] public Empty exit;

        public DialogueNode GetPreviousDialogueNode()
        {
            NodePort enterPort = GetInputPort("enter");
            List<NodePort> connections = enterPort.GetConnections();
            if (!enterPort.IsConnected) {
                return null;
            }else
            {
                return connections[0].node as DialogueNode;
            }
        }
        
        public DialogueNode GetNextDialogueNode()
        {
            NodePort exitPort = GetOutputPort("exit");
            List<NodePort> connections = exitPort.GetConnections();
            if (!exitPort.IsConnected) {
                return null;
            }else
            {
                return connections[0].node as DialogueNode;
            }
        }
    }
}
