﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BigOS.XNode;

using Medicine;

namespace BigOS.Localization.DialogueSystem
{

    [NodeTint(38, 38, 38)]
    public class Message : DialogueIDNode, ISerializationCallbackReceiver
    {
        [HideInInspector] public bool led;
      

        public enum InterlocutorSide
        {
            Left,
            Right,
            Narration
        }


       // public string ids;

        public static List<string> TMPList;
        [HideInInspector] public List<string> interlocutorList;
        [ListToPopup(typeof(Message), "TMPList")]
        public InterlocutorSide interlocutorSide;
        public int context;

        [TextArea(5, 25)]
        public string content;
        [HideInInspector]
        public bool contentRepeated;

      //  [Input] public Empty enter;
       // [Output] public Empty exit;
        // [Input] public string add;
        //    [Inject.Single]
        //  DialogueTreeSystem dialogueTreeSystem { get; }

        public void OnBeforeSerialize()
        {
            if (DialogueCore.Instance != null)
            {
                //if (DialogueCore.Instance.GetInterlocutorData() == null) return;
               // interlocutorList = DialogueCore.Instance.GetInterlocutorData().GetInterlocutrs();
              //  TMPList = interlocutorList;
            }

            //if (dialogueTreeSystem != null)
            // {
            //     interlocutorList = dialogueTreeSystem.GetInterlocutorData().GetInterlocutrs();
            //     TMPList = interlocutorList;
            // }

        }
        public void OnAfterDeserialize()
        {


            if (DialogueIDsSystem.IsContentRepeated(content))
            {
                contentRepeated = true;
            }
            else
            {
                contentRepeated = false;
            }
            //}
        }
        public void RepraceEntry()
        {
            Debug.Log("replace");
        }
        public override object GetValue(NodePort port) {
            return null;
        }

    }
}