using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BigOS.XNode;
using BigOS.Localization.DialogueSystem;

namespace BigOS.Localization.DialogueSystem
{

    [NodeTint(205, 0, 127)]
    public class TeleportOut : Node
    {
        public int teleportId;
        [Output] public Empty enter;
    }
}
