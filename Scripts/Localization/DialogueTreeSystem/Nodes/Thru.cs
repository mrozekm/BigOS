using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BigOS.XNode;
using BigOS.Localization.DialogueSystem;

namespace BigOS.Localization.DialogueSystem
{

    [NodeTint(255, 153, 51)]
    public class Thru : DialogueNode
    {
        [Input] public Empty enter;
        [TextArea(1, 15)]
        public string content;
        [Output] public Empty exit;
    }
}