﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BigOS.XNode;

namespace BigOS.Localization.DialogueSystem
{
    public abstract class DialogueIDNode : DialogueNode
    {
        [HideInInspector]
        public uint id; // = DialogueIDsSystem.GenerateID();

        
        
        protected override void Init()
        {
            base.Init();
            if (id == 0)
            {
                id = DialogueIDsSystem.GenerateID();
            }

        }

        public override void JustMadeADuplicate()
        {
            base.JustMadeADuplicate();
            id = DialogueIDsSystem.GenerateID();
        }

        public string GetIDS()
        {
            return IDHelper.ToSignature(id);
        }

        public uint GetID()
        {
            return id;
        }
        
    
    }
}
