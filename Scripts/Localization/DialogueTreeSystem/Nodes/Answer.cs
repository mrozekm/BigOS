﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BigOS.XNode;
using BigOS.Localization.DialogueSystem;

namespace BigOS.Localization.DialogueSystem{
    [NodeTint(239, 77, 0)]
    public class Answer : DialogueIDNode, ISerializationCallbackReceiver
    {
       // [Input] public Empty enter;

        
    
        [TextArea(5,15)]
        public string content;
        [HideInInspector]
        public bool contentRepeated;
        
     //   [Output] public Empty exit;
        //[Input] public string add;

    

        public void OnBeforeSerialize(){}
        public void OnAfterDeserialize(){
            
            if(DialogueIDsSystem.IsContentRepeated(content)){
                contentRepeated=true;
            }else{
                contentRepeated=false;
            }
            
        }

        public void RepraceEntry(){
            Debug.Log("replace");
        }
        public override object GetValue(NodePort port) {
            return null;
        }
    }
}
