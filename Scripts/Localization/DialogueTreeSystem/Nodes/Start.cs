﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BigOS.XNode;
using BigOS.Localization.DialogueSystem;
using System;
using Medicine;

namespace BigOS.Localization.DialogueSystem{
    [NodeTint(223, 255, 235)]
    public class Start : DialogueNode, ISerializationCallbackReceiver
    {
        public Action onStateChange;
       //[Output] public Empty exit;

        //public Texture myTextue;
        //[Header("Settings")]
        //[HideInInspector]
        //public bool sameInterlocutor;
        //[HideInInspector]
        //public InputField iField;

        [Inject.Single]
        DialogueCore dialogueCore { get; }
        public void OnBeforeSerialize(){
            //if(sameInterlocutor){
            //    Debug.Log("aaa");
            //    SetOneInterlocutor();
            //}
        }
        public void OnAfterDeserialize(){
            
        }

        public void GenerateStructure(){
            dialogueCore.GenerateDialoguesFromGraph();
            //DialogueTreeSystem.Instance.GenerateDialoguesFromGraph();
        }

        /*
        
        public void SetOneInterlocutor(){
            
            DTSGraph dTSGraph = graph as DTSGraph;
            NodePort exitPort = GetOutputPort("exit");
            List<NodePort> connections = exitPort.GetConnections();
            string firstInterlocutor="";
            
            if (!exitPort.IsConnected) {
                Debug.LogWarning("Node isn't connected");
                return;
            }else{
                if(connections.Count>0){
                    if(connections[0].node is Message){
                        //Debug.Log((connections[0].node as Message).Interlocutor);
                        firstInterlocutor=(connections[0].node as Message).Interlocutor;
                    }
                    if(connections[0].node is EmptyMessage){
                        //Debug.Log((connections[0].node as EmptyMessage).Interlocutor);
                        firstInterlocutor=(connections[0].node as EmptyMessage).Interlocutor;
                    }
                }   
            }
            foreach(Node node in dTSGraph.nodes){
                if(node is Message){
                    (node as Message).Interlocutor=firstInterlocutor;
                }
                if(node is EmptyMessage){
                    (node as EmptyMessage).Interlocutor=firstInterlocutor;
                }
            }
        }
        */
        
        public override object GetValue(NodePort port) {
            return null;
        }
    }
}


