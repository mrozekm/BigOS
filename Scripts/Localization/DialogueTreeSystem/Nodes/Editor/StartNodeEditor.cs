﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using BigOS.XNodeEditor;
using System;
namespace BigOS.Localization.DialogueSystem
{
	[CustomNodeEditor(typeof(Start))]
	public class StartNodeEditor : NodeEditor
	{
		
		public override void OnHeaderGUI() {
			Start node = target as Start;
			DTSGraph graph = node.graph as DTSGraph;
			string title = target.name;
			GUILayout.Label(title, NodeEditorResources.styles.nodeHeader, GUILayout.Height(30));
			GUI.color = Color.white;
		}
		public override void OnBodyGUI() {
				
			base.OnBodyGUI();
			Start node = target as Start;
			
			//if (GUILayout.Button("Same interlocutor")) node.SetOneInterlocutor();
			if (GUILayout.Button("Generate Structure")) node.GenerateStructure();
			
			
				
		}
		
	}
}