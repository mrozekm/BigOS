﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BigOS.XNodeEditor;
using UnityEditor;

namespace BigOS.Localization.DialogueSystem{
	[CustomNodeEditor(typeof(Answer))]
	public class AnswerNodeEditor : NodeEditor
	{
		public override void OnHeaderGUI() {
			//GUI.color = Color.white;
			GUI.color = Color.black;
			Answer node = target as Answer;
			DTSGraph graph = node.graph as DTSGraph;
			string title = target.name;
			GUILayout.Label(title, NodeEditorResources.styles.nodeHeader, GUILayout.Height(30));
			GUI.color = Color.white;
		}

		public override void OnBodyGUI() {

			GUI.color = Color.white;
			Answer node = target as Answer;
			object obj = node.GetIDS();

			base.OnBodyGUI();
			if (obj != null) EditorGUILayout.LabelField(obj.ToString());


			if(node.contentRepeated){
				if (GUILayout.Button("Content repeated - Clone")) node.RepraceEntry();
			}
		}
	}
}