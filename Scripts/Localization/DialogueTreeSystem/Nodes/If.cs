﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BigOS.XNode;
using BigOS.Localization.DialogueSystem;
//using Medicine;

namespace BigOS.Localization.DialogueSystem
{

    [NodeTint(255, 100, 151)]
    public class If : DialogueNode, ISerializationCallbackReceiver
    {
        [Input] public Empty enter;

        public static List<string> TMPList;
        [HideInInspector] public List<string> statsList;
        [ListToPopup(typeof(If), "TMPList")]
        public string Parameter;
        // [Inject.Single]
        // DialogueTreeSystem dialogueTreeSystem { get; }

        [Output] public Empty exitTrue;
        [Output] public Empty exitFalse;

        public void OnBeforeSerialize()
        {
            // if(dialogueTreeSystem!= null)
            //{
            //     statsList= dialogueTreeSystem.GetParametersList();
            //    statsList.Insert(0,"Random");
            //    TMPList=statsList;
            // }

            if (DialogueCore.Instance != null)
            {
                statsList = DialogueCore.Instance.GetParametersList();
                statsList.Insert(0, "Random");
                TMPList = statsList;
            }

        }
        public void OnAfterDeserialize() { }
    }
}