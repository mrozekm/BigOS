﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BigOS.XNode;
using UnityEditor;
using BigOS.XNodeEditor;

namespace BigOS.Localization.DialogueSystem
{

    [CustomNodeGraphEditor(typeof(DTSGraph))]
    public class DTSGraphEditor : NodeGraphEditor
    {
        Color a = Color.white;
        Color b = Color.white;


        /// <summary> Controls graph noodle colors </summary>
        public override Gradient GetNoodleGradient(NodePort output, NodePort input)
        {


            Color finalColor = Color.white;


            DialogueNode node = output.node as DialogueNode;

            switch (node)
            {
                case Message mess:

                    finalColor = Color.white; //new Color(239f / 255f, 77f/255f,0);
                    break;

                case Answer ans:
                    finalColor = new Color(239f / 255f, 77f / 255f, 0);
                    break;


                default:

                    break;


            }


            Gradient baseGradient = base.GetNoodleGradient(output, input);

            baseGradient.SetKeys(
                    new GradientColorKey[] { new GradientColorKey(finalColor, 0f), new GradientColorKey(finalColor, 1f) },
                    new GradientAlphaKey[] { new GradientAlphaKey(1f, 0f), new GradientAlphaKey(1f, 1f) }
                );
            return baseGradient;
        }

       
    }
}