﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace BigOS.Localization{
    [CustomEditor(typeof(DialogueLanguageData))]
    public class DialogueLanguageDataEditor : Editor
    {
        
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            DialogueLanguageData t = (DialogueLanguageData)target;
            //if(EditorGUI.EndChangeCheck()){
                EditorUtility.SetDirty(target);
            //}
        }
    }
}