﻿#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Medicine;
#if GoogleFairy
using BigOS.GoogleFairy;
#endif

namespace BigOS.Localization.DialogueSystem
    {
    [CustomEditor(typeof(DialogueCore))]
    public class DialogueTreeSystemEditor : Editor
    {
        [Inject.Single]
        DialogueSystem.DialogueIDsSystem dialogueIDsSystem { get; }
        
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            DialogueCore t = (DialogueCore)target;
            EditorGUILayout.LabelField("Generate", EditorStyles.boldLabel);
            if (GUILayout.Button("Generate dialogue structure"))
            {
                t.GenerateDialoguesFromGraph();
                dialogueIDsSystem.StoreAutomaticTranslationData();
            }
            
        // if(GUILayout.Button("Export to Google Spreadsheet")){t.dialogueIDsSystem.ExportToSpreadsheet();}
#if GoogleFairy
            EditorGUILayout.LabelField("Google Sheets", EditorStyles.boldLabel);
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Export to Google Spreadsheet")) { dialogueIDsSystem.ExportToSpreadsheet(); }
            if (GUILayout.Button("Import Human Translation")){t.ImportDataFromSpreadsheet(false);}
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            if(GUILayout.Button("Import Human Translation if is outdated")){t.MaybeImportDataFromSpreadsheet(false);}
            if (GUILayout.Button("Get Spreadsheet update date")) { dialogueIDsSystem.GetUpdateDate(false); }
            GUILayout.EndHorizontal();
            
           
          
#endif

            EditorGUILayout.LabelField("Debugging", EditorStyles.boldLabel);
            if(GUILayout.Button("Debug IDs Dictionary")){t.DebugIDsDictionary();}
            if(GUILayout.Button("Debug Dialogue Source Structure")){t.DebugDialogueStructure();}
            //EditorUtility.SetDirty(target);
        }
    }
}
#endif
