﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace BigOS.Localization.DialogueSystem{
    [CustomEditor(typeof(DialogueIDsSystem))]
    public class DialogueIDsSystemEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            DialogueIDsSystem t = (DialogueIDsSystem)target;

            EditorUtility.SetDirty(target);
        }
    }
}
