﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace BigOS.Localization{
    [CustomEditor(typeof(DialogueSource))]
    public class DialogueSourceEditor : Editor
    {
        
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            DialogueSource t = (DialogueSource)target;
            //if(EditorGUI.EndChangeCheck()){
                EditorUtility.SetDirty(target);
            //}
        }
    }
}
