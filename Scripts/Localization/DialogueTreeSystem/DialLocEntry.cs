using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BigOS.Localization
{
    [System.Serializable]
    public class DialLocEntry 
    {
        public uint id;
        //public string idString;
        public string content;
        // SOURCE is king
        // if source changes all other entries with same id become depreciated
        public bool depreciated = false;

        public void DefineEntry(string content)
        {
            this.content = content;
        }
    }
}
