﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BigOS.Localization
{
    /// <summary>
    /// This brave Json doesn't afraid of unserialized data
    /// 
    /// </summary>
    public class BraveNewJson : MonoBehaviour
    {
        public static string ToJson(List<DialLocEntry> entries){
            string jsonData="";
            string idCellName="ID";
            string contentCellName="content";
            char quoteReplacement='*';
            //string quoteReplacementString="\"";
            
            string prefix="{\"array\":[";
            string suffix="]}";

            jsonData+=prefix;
            int index=0;
            foreach(DialLocEntry entry in entries){
                if(index<entries.Count-1){
                    jsonData+="{\""+idCellName+"\":\""+IDHelper.ToSignature(entry.id)+"\",\""+contentCellName+"\":\""+entry.content.Replace('"',quoteReplacement)+"\"},";
                }else{
                    jsonData+="{\""+idCellName+"\":\""+IDHelper.ToSignature(entry.id)+"\",\""+contentCellName+"\":\""+entry.content.Replace('"',quoteReplacement)+"\"}";
                }
                index++;
            }
            jsonData+=suffix;
            return jsonData;
        }
        public static void FormatForCloud(ref string json){
            string prefix="{\"array\":";
            string suffix="}";
            json=prefix+json+suffix;
        }
        //public static void For
    }
}
