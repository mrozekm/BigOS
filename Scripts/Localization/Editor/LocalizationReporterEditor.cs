#if UNITY_EDITOR
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

namespace BigOS.Localization{
    [CustomEditor(typeof(LocalizationReporter))]
    public class LocalizationReporterEditor : Editor
    {
        int deltaCategory = 0;
        int deltaEntry = 0;
    
        public override void OnInspectorGUI()
        {
    
            LocalizationReporter targetObject = (LocalizationReporter)target;
            // just to be safe

            //targetObject.MakeSureCategoryAndEntryIsWithinBounds();


            targetObject.InitForEditor();
            GUILayout.BeginHorizontal();
            targetObject.category = EditorGUILayout.Popup(targetObject.category, targetObject.localizationCore.vocabularyCore.GetListOfGlobalCategories().ToArray()); 
            targetObject.entries[targetObject.category] = EditorGUILayout.Popup(targetObject.entries[targetObject.category], targetObject.localizationCore.vocabularyCore.GetListOfGlobalEntries(targetObject.category).ToArray());
            GUILayout.EndHorizontal();
            GUILayout.Label(targetObject.localizationCore.vocabularyCore.GetGlobalEntry(0, targetObject.category, targetObject.entries[targetObject.category]));

            if (targetObject.entries[targetObject.category] != deltaEntry || deltaCategory != targetObject.category) ApplyLocalizationData();

            deltaCategory = targetObject.category;
            deltaEntry = targetObject.entries[targetObject.category];
            GUILayout.BeginHorizontal();
            //  if (GUILayout.Button("Refresh"))
            //  {
            //     
            //
            //   }
            targetObject.PassLocDataToText();
            EditorUtility.SetDirty(targetObject);
            Repaint();

            GUILayout.EndHorizontal();

            DrawDefaultInspector();
        }

        public void ApplyLocalizationData()
        {
            LocalizationReporter targetObject = (LocalizationReporter)target;
            //targetObject.PassLocDataToText();
        }

    
    }
}
#endif
