#if UNITY_EDITOR
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using Medicine;
using UnityEditor.Graphs;

namespace BigOS.Localization
{

    [CustomEditor(typeof(LocalizationTree))]
    public class LocalizationTreeEditor : Editor
    {
        [Inject.Single] private LocalizationCore LocalizationMaster { get; }

        public override void OnInspectorGUI()
        {

            LocalizationTree targetObject = (LocalizationTree) target;

            DrawDefaultInspector();
            DrawSeparator();
            GUILayout.Label("Google Sheets");
            if (GUILayout.Button("Create spreadsheet"))
            {
#if GoogleFairy
                targetObject.CreateSpreadsheet();
#endif
            }

            if (GUILayout.Button("Update Languages List"))
            {
#if GoogleFairy
                targetObject.UpdateLanguages();
#endif
            }

            if (GUILayout.Button("Export Source to Googlesheets"))
            {
#if GoogleFairy
                targetObject.Export();
#endif
            }

            if (GUILayout.Button("Import human translations"))
            {
#if GoogleFairy
                targetObject.Import();
#endif
            }

            DrawSeparator();
            GUILayout.Label("Localization viewer");
            DrawSeparator();

            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Save"))
            {
                targetObject.Save();
            }

            if (GUILayout.Button("Load"))
            {
                targetObject.Load();
            }

            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Fill with Dialogue(Out of order)"))
            {
                targetObject.FillMissingAndTrimUnusedEntries(true);
            }

            if (GUILayout.Button("Fill Light"))
            {
                targetObject.FillMissingAndTrimUnusedEntries(false);
            }

            if (GUILayout.Button("EnumGen"))
            {
                targetObject.GenerateEnums();
            }

            if (LocalizationMaster.GetAllLanguages().Count < 2)
            {

                return;
            }

            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            GUILayout.Label("Categories /Entries");
            GUILayout.EndHorizontal();


            GUILayout.BeginHorizontal();
            if (targetObject.GetListOfCategories() == null) return;

            targetObject.currentCategory = EditorGUILayout.Popup(targetObject.currentCategory,
                targetObject.GetListOfCategories().ToArray());
            List<string> entries = targetObject.GetListOfEntries(targetObject.currentCategory);
            if (entries != null && entries.Count != 0)
            {

                targetObject.currentEntries[targetObject.currentCategory] =
                    EditorGUILayout.Popup(targetObject.currentEntries[targetObject.currentCategory], entries.ToArray());
            }

            GUILayout.EndHorizontal();
            List<CurrentEntryWrapper> wraps = targetObject.GetAllLanguagesOfEntryInCategory(
                targetObject.currentCategory, targetObject.currentEntries[targetObject.currentCategory]);

            //-----SOURCE LANG
            DrawSeparator();
            GUILayout.BeginHorizontal();
            GUILayout.Label("Eng SOURCE", GUILayout.Width(85));

            // w.currentEntry.value = EditorGUILayout.TextField(w.currentEntry.value);

            int numberOfChosenEntry = targetObject.currentEntries[targetObject.currentCategory];

            if (targetObject.sourceBranch != null && targetObject.sourceBranch.categories != null &&
                targetObject.sourceBranch.categories.Count > 0 && numberOfChosenEntry >=
                targetObject.sourceBranch.categories[targetObject.currentCategory].currentEntries.Count)
            {
                targetObject.currentEntries[targetObject.currentCategory] = 0;
            }

            if (targetObject.sourceBranch != null)
            {
                if (targetObject.sourceBranch.categories != null && targetObject.sourceBranch.categories.Count > 0 &&
                    targetObject.sourceBranch.categories[targetObject.currentCategory].currentEntries != null &&
                    targetObject.sourceBranch.categories[targetObject.currentCategory].currentEntries.Count > 0)
                    targetObject.sourceBranch.categories[targetObject.currentCategory]
                            .currentEntries[numberOfChosenEntry].autoTranslation =
                        EditorGUILayout.TextArea(targetObject.sourceBranch.categories[targetObject.currentCategory]
                            .currentEntries[numberOfChosenEntry]
                            .autoTranslation); //, GUILayout.MaxHeight(375), GUILayout.MaxWidth(600));
            }

            GUILayout.EndHorizontal();
            DrawSeparator();

            //------

            foreach (CurrentEntryWrapper w in wraps)
            {
                GUILayout.BeginHorizontal();
                GUILayout.Label(w.language, GUILayout.Width(85));
                // w.currentEntry.value = EditorGUILayout.TextField(w.currentEntry.value);
                w.currentEntry.autoTranslation =
                    EditorGUILayout.TextArea(w.currentEntry
                        .autoTranslation); //, GUILayout.MinHeight(60 ), GUILayout.MaxHeight(365), GUILayout.MinWidth(60), GUILayout.MaxWidth(600));


                if (GUILayout.Button("Rev", GUILayout.Width(35)))
                {
                    // TranslateEntries(targetObject.currentCategory, targetObject.currentEntry);
                    targetObject.ReverseTranslate(w.language, w.currentEntry.autoTranslation);
                }



                if (GUILayout.Button("T", GUILayout.Width(25)))
                {
                    // TranslateEntries(targetObject.currentCategory, targetObject.currentEntry);
                    targetObject.TranslateEntry(w.language, w.categoryIndex, w.entryIndex);
                }

                GUILayout.EndHorizontal();
            }

            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Auto Translate For This Entry"))
            {
                TranslateEntries(targetObject.currentCategory,
                    targetObject.currentEntries[targetObject.currentCategory]);

            }

            if (GUILayout.Button("Clear Entries"))
            {
                for (int i = 0; i < wraps.Count; i++)
                {
                    wraps[i].currentEntry.autoTranslation = "";
                }
            }

            if (GUILayout.Button("Analyze Missing"))
            {
                targetObject.GetAllEmptyEntries();
            }

            if (GUILayout.Button("Translate All missing"))
            {
                targetObject.TranslateAllEmptyEntriesInTheProject();
            }

            GUILayout.EndHorizontal();

            // GUILayout.EndHorizontal();


        }


        private void DrawSeparator()
        {
            Rect rect = EditorGUILayout.GetControlRect(false, 2);

            rect.height = 2;

            EditorGUI.DrawRect(rect, new Color(0.5f, 0.5f, 0.5f, 1));
        }

        public void TranslateEntries(int category, int entry)
        {
            LocalizationTree targetObject = (LocalizationTree) target;
            int amountOfLang = LocalizationMaster.GetAllLanguages().Count;
            // TRANSLATE FROM ENGLISH
            string entryToTranslate = targetObject.GetEntry(0, category, entry);
            for (int i = 0; i < amountOfLang; i++)
            {
                if (targetObject.GetEntry(i, category, entry).Length == 0)
                {
                    targetObject.TranslateEntry(i, category, entry);
                }
            }
        }
    }
}
#endif
