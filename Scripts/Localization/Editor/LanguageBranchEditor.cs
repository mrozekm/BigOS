#if UNITY_EDITOR
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

namespace BigOS.Localization
{

    [CustomEditor(typeof(LanguageBranch))]
    public class LanguageBranchEditor : Editor
    {

        public override void OnInspectorGUI()
        {

            LanguageBranch targetObject = (LanguageBranch) target;


            GUILayout.BeginHorizontal();

            if (GUILayout.Button("LOAD"))
            {
                targetObject.Load();
            }

            if (GUILayout.Button("SAVE"))
            {

                if (targetObject.name.Contains("SOURCE"))
                {
                    targetObject.SaveThisSpecificTextAsset();
                }
                else
                {
                    targetObject.Save();
                }
            }

            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();

            if (targetObject != null && targetObject.name != null && targetObject.name.Contains("SOURCE"))
            {
                if (GUILayout.Button("PROPAGATE"))
                {
                    targetObject.Propagate();
                }
            }

            GUILayout.EndHorizontal();


            DrawDefaultInspector();
            EditorUtility.SetDirty(target);
        }

    }
}
#endif

