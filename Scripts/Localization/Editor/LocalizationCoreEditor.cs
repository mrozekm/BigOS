#if UNITY_EDITOR
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using System.Linq;
#if GoogleFairy
using BigOS.GoogleFairy;
#endif

namespace BigOS.Localization{
    [CustomEditor(typeof(LocalizationCore))]
    public class LocalizationCoreEditor : Editor
    {
    
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            LocalizationCore targetObject = (LocalizationCore)target;
            GUILayout.Label("Selected Language");
            targetObject.SelectedLanguageEditor = EditorGUILayout.Popup("Selected Language", targetObject.SelectedLanguageEditor, targetObject.GetListOfLanguages().ToArray());

            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Analyze Missing"))
            {

                //TODO: Everything
                
            }

            if (GUILayout.Button("Fill Missing & EnumGen"))
            {
                //TODO: Everything

            }

            if (GUILayout.Button("Auto Translate All"))
            {
                //TODO: Everything
            
            }

            GUILayout.EndHorizontal();

#if GoogleFairy
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Export to Sheets"))
            {

                //TODO: Everything
                VocabularyCore.GlobalSave();
            }

            if (GUILayout.Button("Import from Sheets"))
            {
                //TODO: Everything
                VocabularyCore.GlobalLoad();
            }

            GUILayout.EndHorizontal();
#endif
#if GoogleFairy
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Show Google Sheets"))
            {
            string adress = GameObject.FindObjectOfType<DriveConnection>().connectionData.spreadsheetId;
                Application.OpenURL("https://docs.google.com/spreadsheets/d/" + adress + "/edit#gid=43685529");

            }
            GUILayout.EndHorizontal();
#endif
        }

        private void DrawSeparator()
        {
            Rect rect = EditorGUILayout.GetControlRect(false, 2);

            rect.height = 2;

            EditorGUI.DrawRect(rect, new Color(0.5f, 0.5f, 0.5f, 1));
        }

        
    
    }
}
#endif
