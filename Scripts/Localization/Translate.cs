

// We need this for parsing the JSON, unless you use an alternative.
// You will need SimpleJSON if you don't use alternatives.
// It can be gotten hither. http://wiki.unity3d.com/index.php/SimpleJSON
using BigOS.SimpleJSON;
//using Newtonsoft.Json;
using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using BigOS.Localization;

#if UNITY_EDITOR
using UnityEditor;
#endif

[System.Serializable]
public enum TranslationEngine { yandex, google };

public class Translate : MonoBehaviour {

#if UNITY_EDITOR

    // Should we debug?
    public bool isDebug = false;

    public AutoTranslationManager localzationManager;
    string yandexKey = "trnsl.1.1.20190516T033456Z.67def286c3cddfdd.854f02eedc94c6635fac3f9ea359384e04bf9ab4"; 

	// Here's where we store the translated text!

	private string translatedText = "";

	// This is only called when the scene loads.
	void Start () {
		// Strictly for debugging to test a few words!
		//if(isDebug)
		//	StartCoroutine (Process ("en","Bonsoir."));
	}

    [ContextMenu("Debug")]
    public void Debug()
    {
        //EasyLocalizationScript.StartCoroutine(Process("en", "Bonsoir."));
        // EditorCoroutine.start(Process("en", "Bonsoir."));

        string sourceLang = "auto";
        // Construct the url using our variables and googles api.
        string url = "https://translate.googleapis.com/translate_a/single?client=gtx&sl="
            + sourceLang + "&tl=" + "en" + "&dt=t&q=" + UnityWebRequest.EscapeURL("Bonsoir.");
        print(url);

        var www = new WWW(url);
        ContinuationManager.Add(() => www.isDone, () =>
        {
            if (!string.IsNullOrEmpty(www.error)) print("WWW failed: " + www.error);
            print("WWW result : " + www.text);
            if (www.isDone)
            {

                // Check to see if we don't have any errors.
                if (string.IsNullOrEmpty(www.error))
                {
                    // Parse the response using JSON.
                    var N = JSONNode.Parse(www.text);
                    // Dig through and take apart the text to get to the good stuff.
                    translatedText = N[0][0][0];
                    // This is purely for debugging in the Editor to see if it's the word you wanted.
                    if (isDebug)
                        print(translatedText);
                }
            }
        });
    }


    public void TranslateEntry(System.Action<string> callback, string textToTranslate, int targetLanguage, TranslationEngine engine)
    {
       
        string sourceCode = "en";
        string targetCode = localzationManager.GetCode(targetLanguage, engine);
        
        if (engine == TranslationEngine.google)
            CreateGoogleQuery(sourceCode,targetCode, textToTranslate, callback);
        if (engine == TranslationEngine.yandex)
            CreateYandexQuery(sourceCode, targetCode, textToTranslate, callback);

    }

    public void ChangeEntry(string entry)
    {

    }

    public void CreateYandexQuery(string sourceCode, string targetCode, string stringToTranslate, System.Action<string> callback)
    {

        if (stringToTranslate.Length == 0) return;
        string url = "https://translate.yandex.net/api/v1.5/tr.json/translate?key=" + yandexKey  +"&text=" + UnityWebRequest.EscapeURL(stringToTranslate) + "&lang=" + sourceCode + "-" + targetCode +"&[format=plain]";
        //print("MAKING YANDEX REQUEST ----------- " + url);
        var www = new WWW(url);
        ContinuationManager.Add(() => www.isDone, () =>
        {
            if (!string.IsNullOrEmpty(www.error)) print("WWW failed: " + www.error);
            print("WWW result : " + www.text);
            if (www.isDone)
            {

                // Check to see if we don't have any errors.
                if (string.IsNullOrEmpty(www.error))
                {
                    // Parse the response using JSON.
                    var N = JSONNode.Parse(www.text);
                    // Dig through and take apart the text to get to the good stuff.
                    translatedText = N[2][0];
                    // This is purely for debugging in the Editor to see if it's the word you wanted.
                    if (isDebug)
                        print(translatedText);
                    //Debug.Log(translatedText);

                    callback(translatedText);
                }
            }
        });
    }

    public void ReverseTranslateQuery(string sourceCode, string targetCode, string entryToTranslate)
    {
       // string entryToTranslate = localzationManagerDefinition.GetEntry(language, category, entry);
        string url = "https://translate.yandex.net/api/v1.5/tr.json/translate?key=" + yandexKey + "&text=" + UnityWebRequest.EscapeURL(entryToTranslate) + "&lang=" + sourceCode + "-" + targetCode + "&[format=plain]";
        print("MAKING YANDEX REQUEST (R) ----------- " + url);
        var www = new WWW(url);
        ContinuationManager.Add(() => www.isDone, () =>
        {
            if (!string.IsNullOrEmpty(www.error)) print("WWW failed: " + www.error);
            print("WWW result : " + www.text);
            if (www.isDone)
            {

                // Check to see if we don't have any errors.
                if (string.IsNullOrEmpty(www.error))
                {
                    // Parse the response using JSON.
                    var N = JSONNode.Parse(www.text);
                    // Dig through and take apart the text to get to the good stuff.
                    translatedText = N[2][0];
                    // This is purely for debugging in the Editor to see if it's the word you wanted.
                    if (isDebug)
                        print(translatedText);
                    TextPopup.Display("Translated: " + System.Environment.NewLine + entryToTranslate + System.Environment.NewLine + "as" + System.Environment.NewLine + translatedText);
                }
            }
        });
    }

    public void CreateGoogleQuery(string sourceCode, string targetCode, string stringToTranslate, System.Action<string> callback)
    {
       
        if (stringToTranslate.Length == 0) return;
        string url = "https://translate.googleapis.com/translate_a/single?client=gtx&sl="
           + sourceCode + "&tl=" + targetCode + "&dt=t&q=" + UnityWebRequest.EscapeURL(stringToTranslate);
       // print("MAKING GOOGLE REQUEST ----------- " + url);

        var www = new WWW(url);
        ContinuationManager.Add(() => www.isDone, () =>
        {
            if (!string.IsNullOrEmpty(www.error)) print("WWW failed: " + www.error);
            print("WWW result : " + www.text);
            if (www.isDone)
            {
               // print("MAKING GOOGLE REQUEST ----------- " + url);
                // Check to see if we don't have any errors.
                if (string.IsNullOrEmpty(www.error))
                {
                    // Parse the response using JSON.
                    var N = JSONNode.Parse(www.text);
                    // Dig through and take apart the text to get to the good stuff.
                    // translatedText = N[0][0][0];
                    //translatedText += N[0][1][0];
                    translatedText = "";
                    print("amount of entries : " + N[0].Count.ToString());
                    foreach (JSONNode n in N[0])
                     {
                         translatedText += n[0];
                     }

                    // This is purely for debugging in the Editor to see if it's the word you wanted.
                    if (isDebug)
                        print(translatedText);
                    callback( translatedText);
                }
            }
        });
    }

    /*

	// We have use googles own api built into google Translator.
	public IEnumerator Process (string targetLang, string sourceText)
    {
		// We use Auto by default to determine if google can figure it out.. sometimes it can't.
		string sourceLang = "auto";
		// Construct the url using our variables and googles api.
		string url = "https://translate.googleapis.com/translate_a/single?client=gtx&sl=" 
			+ sourceLang + "&tl=" + targetLang + "&dt=t&q=" + UnityWebRequest.EscapeURL(sourceText);
        print(url);




        using (UnityWebRequest www2 = UnityWebRequest.Get(url))
        {
            www2.SendWebRequest();

            while (www2.responseCode == -1)
            {
                //do something, or nothing while blocking
            }
            if (www2.isNetworkError)
            {
               print(www2.error);
            }
            else
            {
                //Show results as text
                print(www2.responseCode.ToString());
                //process downloadHandler.text
            }

            yield return www2;

            if (www2.isDone)
            {
               
                // Check to see if we don't have any errors.
                if (string.IsNullOrEmpty(www2.error))
                {
                    // Parse the response using JSON.
                    var N = JSONNode.Parse(www2.downloadHandler.text);
                    // Dig through and take apart the text to get to the good stuff.
                    translatedText = N[0][0][0];
                    // This is purely for debugging in the Editor to see if it's the word you wanted.
                    if (isDebug)
                        print(translatedText);
                }
            }
            else
            {
                print("ERROR WITH THE REQUEST "+ www2.error.ToString());
            }
        }

       







        /*


        // Put together our unity bits for the web call.
        WWW www = new WWW (url);
		// Now we actually make the call and wait for it to finish.
		yield return www;
        print("...2");
        // Check to see if it's done.
        if (www.isDone)
        {
            print("...3");
            // Check to see if we don't have any errors.
            if (string.IsNullOrEmpty(www.error))
            {
                // Parse the response using JSON.
                var N = JSONNode.Parse(www.text);
                // Dig through and take apart the text to get to the good stuff.
                translatedText = N[0][0][0];
                // This is purely for debugging in the Editor to see if it's the word you wanted.
                if (isDebug)
                    print(translatedText);
            }
        }
        else
        {
            print("...4");
        }
        */
        /*
    }

	// Exactly the same as above but allow the user to change from Auto, for when google get's all Jerk Butt-y
	public IEnumerator Process (string sourceLang, string targetLang, string sourceText) {
		string url = "https://translate.googleapis.com/translate_a/single?client=gtx&sl=" 
			+ sourceLang + "&tl=" + targetLang + "&dt=t&q=" + WWW.EscapeURL(sourceText);
		
		WWW www = new WWW (url);
		yield return www;
		
		if (www.isDone) {
			if(string.IsNullOrEmpty(www.error)){
				var N = JSONNode.Parse(www.text);
				translatedText = N[0][0][0];
				if(isDebug)
					print(translatedText);
			}
		}
	}
    */
    
}

public class TextPopup : EditorWindow
{
    private static string text;

    public static void Display(string msg)
    {
        text = msg;
        TextPopup window = GetWindow<TextPopup>(true, "Notification");
        window.position = new Rect(Screen.width / 2, Screen.height / 2, 550, 400);
        window.ShowPopup();
        // window.ShowAuxWindow();
        window.ShowPopup();
    }

    private void OnGUI()
    {
        EditorGUILayout.LabelField(text, EditorStyles.wordWrappedLabel);
        GUILayout.Space(30);
        if (GUILayout.Button("OK"))
        {
            Close();
        }
    }
#endif
}

