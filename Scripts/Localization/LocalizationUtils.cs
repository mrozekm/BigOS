using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace BigOS.Localization { 

public static class LocalizationUtils
{
    #region reading files

    /// <summary>
    /// recognizing enum by "=" attached to it
    /// </summary>
    public static void PharseForLocalizationBranch(List<string> source, out List<EntryList> category, List<EntryList> categoryOriginals)
    {
        category = new List<EntryList>();
        EntryList currentCat = new EntryList();
        int catEnum = -1;


        int currentLine = 0;
        //int lastAnchor = 0;
        //source = RemoveWhitespace(source);

        while (currentLine < source.Count)
        {
            // IF THE LINE IS JUST EMPTY - ignore it
            if (source[currentLine].Length < 2)
            {
                currentLine++;
            }
            // IF SOMETHING SLOPPY HAPPENED
            else if (source[currentLine].Length < 2 && source[currentLine][0].ToString() == " ")
            {
                currentLine++;
                Debug.LogError("messy space in line " + currentLine + " category " + currentCat);
            }
            // IF THE LINE IS A CATEGORY INCREMENT THE CATEGORY COUNTER
            else if (IsThisCategory(source[currentLine]))
            {
                catEnum++;
                currentCat = new EntryList();
                currentCat.currentEntries = new List<CurrentEntry>();
                // Debug.Log
                currentCat.name = categoryOriginals[catEnum].name;
                currentLine++;
                Debug.Log(currentCat.name);
                category.Add(currentCat);
            }
            // IF THE LINE HAS ENUM AND TEXT: PHARSE!
            else
            {
                string nameOfEnum;
                List<string> value = GetEnumAndValue(ref currentLine, source, out nameOfEnum);
                CurrentEntry forgedEntry = new CurrentEntry();
                forgedEntry.name = nameOfEnum;

                Debug.Log(nameOfEnum + "///" + value[0]);

                StringUtils.DeleteSpaceAndReturnInFrontandBack(ref forgedEntry.name, nameOfEnum);
                forgedEntry.autoTranslation = StringFromStringList(value);
                StringUtils.DeleteSpaceAndReturnInFrontandBack(ref forgedEntry.autoTranslation, value[0]);
                currentCat.currentEntries.Add(forgedEntry);
            }
        }
    }



    public static bool IsThisCategory(string checkedString)
    {
        if (checkedString[0] == '[')
        {
            return true;
        }
        else
            return false;
    }

    public static List<string> GetEnumAndValue(ref int i, List<string> sourceList, out string entryName)
    {
        List<string> returnValue = new List<string>();
        string firstValueOfReturn = GetStringAfterChar('=', sourceList[i]);
        returnValue.Add(firstValueOfReturn);
        entryName = GetStringBeforeChar('=', sourceList[i]);

        if (string.IsNullOrWhiteSpace(entryName))
        {
            entryName = "";
        }


        while (i < sourceList.Count)
        {
            i++; // WE START WITH SEARCHING THE NEXT LINE - THIS ONE IS NOT RELEVANT
            if (i < sourceList.Count)
            {
                if (DoesThisStringContainChar('=', sourceList[i]) || (sourceList[i].Length > 0 && sourceList[i][0] == '['))
                {
                    //WE HAVE FOUND 
                    break;
                }
                else
                {
                    returnValue.Add(sourceList[i]);
                }
            }
        }

        return returnValue;
    }


    public static bool DoesThisStringContainChar(char searchedChar, string searchedString)
    {
        if (searchedString.Contains(searchedChar)) return true;
        else return false;
    }

    public static string GetStringBeforeChar(char searchedChar, string testString)
    {
        if (!testString.Contains(searchedChar)) Debug.LogError("\"" + testString + "\"" + "does not contain " + searchedChar);
        return testString.Substring(0, testString.IndexOf(searchedChar));
    }

    public static string GetStringAfterChar(char searchedChar, string testString)
    {
        int index = testString.IndexOf(searchedChar);
        Debug.Log(testString);
        int length = (testString.Length) - (index + 1);
        if (length == 0)
        {
            Debug.LogWarning("entry empty: " + testString);
            return "";
        }
        else
            return testString.Substring(index + 1, length); //?
    }

    public static string RemoveWhitespace(this string str)
    {
        return string.Join("", str.Split(default(string[]), System.StringSplitOptions.RemoveEmptyEntries));
    }

    //TODO
    public static void RemoveWhiteSpaceFromList()
    {

    }

    public static List<string> GetRawStringListFromLocFile(TextAsset asset)
    {
        Debug.Log("text length = " + asset.text.Length.ToString());
        return asset.text.Split("\n"[0]).ToList();
    }

    public static List<string> GetStringListFromSting(string rawString)
    {
        Debug.Log("text length = " + rawString.Length.ToString());
        return rawString.Split("\n"[0]).ToList();
    }



    #endregion

    public static string StringFromStringArray(string[] stringArr)
    {

        string returnString = "";
        foreach (string str in stringArr)
        {
            returnString = returnString + "\n" + str;
        }

        return returnString;
    }

    public static string StringFromStringList(List<string> stringList)
    {

        string returnString = "";
        foreach (string str in stringList)
        {
            returnString = returnString + "\n" + str;
        }

        return returnString;
    }
    

    //----------

    public static void RemoveEmptyEntriesInStringList(ref List<string> stringList)
    {
        for (int i = stringList.Count - 1; i >= 0; i--)
        {
            if (IsEmpty(stringList[i]))
            {
                stringList.Remove(stringList[i]);
            }
        }
    }

    public static bool IsEmpty(string source) // todo make better empty detection
    {

        if (source.Length < 2)
            return true;
        else
            return false;
    }

        /// <summary>
        /// recognizing enum by "=" attached to it
        /// </summary>
        public static void PharseForLocalizationBranch(List<string> source, ref List<EntryList> category)
        {
            int currentLine = 0;
            //int lastAnchor = 0;
            //source = RemoveWhitespace(source);

            while (currentLine < source.Count)
            {
                string nameOfEnum;
                GetEnumAndValue(ref currentLine, source, out nameOfEnum);
            }
        }

    }
    }
