using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Newtonsoft.Json;
using BigOS.Localization.DialogueSystem;
using Medicine;
using UnityEngine.Serialization;

namespace BigOS.Localization
{
    [Register.Single]
    public class LocalizationCore : MonoBehaviour
    {
        // [SerializeField]
        public int selectedLanguage;
        /*
        public static int SelectedLanguage
        {
            get
            {
                MakeSureInstanceIsNotNull();
                return instance.selectedLanguage;
            }
            set => instance.selectedLanguage = value;
        }
        */
        public int SelectedLanguageEditor { get => selectedLanguage; set => selectedLanguage = value; }


        private int deltaLanguage;


        public AvailableLanguages availableLanguages;

        [FormerlySerializedAs("localizationTreeManager")] public VocabularyCore vocabularyCore;


    //  public static List<Language> AvailableLanguages
    // {
        
            /*
    #if UNITY_EDITOR
            get { return GameObject.FindObjectOfType<LocalizationMaster>().availableLanguages.availableLanguages; }
    #else
            get => instance.availableLanguages;
    #endif
    */
    //  }

        [FormerlySerializedAs("dialogueTreeSystem")] [SerializeField] public DialogueCore dialogueCore;



        // Start is called before the first frame update
        void Start()
        {
            
        }

        // Update is called once per frame
        void Update()
        {
            
        }

        private void OnEnable()
        {
            // Let's get the display based on the last language we selected - not based on slots
            selectedLanguage = PlayerPrefs.GetInt("Language", 0);
            if (selectedLanguage > availableLanguages.availableLanguages.Count - 1) selectedLanguage = 0;
            Debug.Log("Loaded Language from PlayerPrefs= " + availableLanguages.availableLanguages[selectedLanguage].name);
        }

        public List<string> GetListOfLanguages()
        {
            List<string> entriesHere = new List<string>();
            foreach (Language l in availableLanguages.availableLanguages)
            {
                entriesHere.Add(l.name);
            }
            return entriesHere;
        }

        public int AmountOfLanguages()
        {
            return availableLanguages.availableLanguages.Count;
        }

    #if UNITY_EDITOR

        public string GetCode(int language, TranslationEngine translationEngine)
        {
            return translationEngine == TranslationEngine.google ? availableLanguages.availableLanguages[language].googleCode : availableLanguages.availableLanguages[language].yandexCode;
        }

    #endif

    //  public static void MakeSureInstanceIsNotNull()
    //  {
    //      if (instance == null)
    //     {
    //         instance = GameObject.FindObjectOfType<LocalizationMaster>();
    //     }
    //  }

        public int WhatIsTheIndexOfLanguage(string lang)
        {
        // MakeSureInstanceIsNotNull();
            return availableLanguages.availableLanguages.IndexOf(availableLanguages.availableLanguages.FirstOrDefault(p => p.name == lang));
        }

        public Language GetLanguage(string name)
        {
        //   MakeSureInstanceIsNotNull();
            return availableLanguages.availableLanguages.FirstOrDefault(p => string.CompareOrdinal(name, p.name) == 0);
        }

        public List<Language> GetAllLanguages()
        {
        // MakeSureInstanceIsNotNull();
            return availableLanguages.availableLanguages;
        }


        public Language GetSelectedLanguage()
        {
            return availableLanguages.availableLanguages[selectedLanguage];
        }

        public int GetIndexOfLanguage(Language language)
        {
            return availableLanguages.availableLanguages.IndexOf(language);
        }

        public string GiveMeLanguageNameInTheNameOfTheLanguage(int numberOfLanguage)
        {
        return  availableLanguages.availableLanguages[numberOfLanguage].languageNameInTheNameOfTheLanguage;
        }


        public static void GlobalSave()
        {
            VocabularyCore.GlobalSave();
        }

        public static void GlobalLoad()
        {
            VocabularyCore.GlobalLoad();
        }

    }
}