using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using Newtonsoft.Json;
using System.Linq;
using System.IO;
#if GoogleFairy
using BigOS.GoogleFairy;
#endif
using System;
using Medicine;
using BigOS.SimpleJSON;

namespace BigOS.Localization
{

    [CreateAssetMenu(fileName = "LocalizationTree", menuName = "BigOS/Localization/LocalizationTree", order = 1)]
    public class LocalizationTree : ScriptableObject
    {
        [Header("Category Setup")]
      //  public List<Category> categories;
        [Header("Tree")]
        public LanguageBranch sourceBranch;
        public List<LanguageBranch> languageBranches;
        [Header("Settings")]
        [SerializeField] bool enableEnumGen;
        [SerializeField] bool enableOnlyCategoriesEnumGen;
        [SerializeField] string saveFolderName = "Assets/Resources/Localization/";
        [SerializeField] string nameOfCategoriesEnum = "LocalizationCategoriesEnum"; // DO  NOT CHANGE
        [Header("TranslationEngine")]
#if UNITY_EDITOR
        public TranslationEngine translationEngine;
#endif
        [SerializeField] string tableName;
        // EDITOR SPECIFIC
        [HideInInspector]
        public int currentCategory;
        [HideInInspector]
        public int[] currentEntries = new int[256];

        [Inject.Single] private AutoTranslationManager AutoTranslationManager { get; }
       // public Translate Translate { get; }
        
        [Inject.Single] private LocalizationCore LocalizationCore { get; }

        #region get string lists

        public List<string> GetListOfCategories()
        {
           


            List<string> categoriesHere = new List<string>();

            //if (categories != null)
            {
                foreach (EntryList c in sourceBranch.categories)
                    categoriesHere.Add(c.name);
            }
            return categoriesHere;

        }

        public List<string> GetListOfEntries(int inCategory)
        {
            if (sourceBranch == null || sourceBranch.categories == null) return null;

            List<string> entriesHere = new List<string>();
            if (sourceBranch.categories != null && sourceBranch.categories.Count != 0)
            foreach (CurrentEntry e in sourceBranch.categories[inCategory].currentEntries)
            {
                entriesHere.Add(e.name);
            }
            return entriesHere;
        }

        public List<CurrentEntry> GetSourceEntriesByCategory(int inCategory){
            if (sourceBranch == null || sourceBranch.categories == null) return null;

            List<CurrentEntry> entriesHere = new List<CurrentEntry>();
            if (sourceBranch.categories != null && sourceBranch.categories.Count != 0)
            foreach (CurrentEntry e in sourceBranch.categories[inCategory].currentEntries)
            {
                entriesHere.Add(e);
            }
            return entriesHere;
        }

       



        #endregion


        public List<CurrentEntryWrapper> GetAllLanguagesOfEntryInCategory(int categiryNr, int entryNr)
        {
            List<CurrentEntryWrapper> entries = new List<CurrentEntryWrapper>();
            if (languageBranches != null)
            {
                foreach (LanguageBranch trunk in languageBranches)
                {
                    if (trunk == null || trunk.categories == null || trunk.categories.Count == 0 || trunk.categories[0] == null)
                    {
                        continue;
                    }

                    CurrentEntryWrapper wrapper = new CurrentEntryWrapper();
                    wrapper.language = trunk.name;
                    if (!trunk) Debug.LogError(1);
                    if (trunk.categories == null) Debug.LogError(2);
                    if (categiryNr >= trunk.categories.Count) Debug.LogError("dddd " + categiryNr + " / " + trunk.categories.Count + " name " + trunk.name);
                    if (trunk.categories[categiryNr].currentEntries == null) Debug.LogError("dddd-- " + categiryNr);
                    if (entryNr >= trunk.categories[categiryNr].currentEntries.Count) entryNr = 0; //
                    if (!trunk || trunk.categories == null || categiryNr >= trunk.categories.Count ||
                        entryNr >= trunk.categories[categiryNr].currentEntries.Count)
                    {
                        string error = " could not find localization entry for all languages in category: " + categiryNr + " / " + trunk.categories.Count + " " + entryNr + " / " + trunk.categories[categiryNr].currentEntries.Count + " in language " + trunk.name;
                        Debug.LogError(error);
                    }
                    else
                    {
                        wrapper.currentEntry = trunk.categories[categiryNr].currentEntries[entryNr];
                        entries.Add(wrapper);
                    }
                }
            }
            return entries;
        }



        public List<CurrentEntryWrapper> GetAllEmptyEntries()
        {
            List<CurrentEntryWrapper> entries = new List<CurrentEntryWrapper>();
            foreach (LanguageBranch trunk in languageBranches)
            {
                for (int i = 0; i < sourceBranch.categories.Count; i++)
                {
                    int amountOfEntires = sourceBranch.categories[i].currentEntries.Count;
                    for (int j = 0; j < amountOfEntires; j++)
                    {
                        if (trunk == null || trunk.categories == null || trunk.categories.Count == 0) continue;

                        CurrentEntry checkedEntry = trunk.categories[i].currentEntries[j];
                        if (checkedEntry.autoTranslation == null || checkedEntry.autoTranslation.Length == 0) // TODO: will it work?
                        {
                            CurrentEntryWrapper wrapper = new CurrentEntryWrapper();
                            wrapper.language = trunk.name;
                            wrapper.currentEntry = trunk.categories[i].currentEntries[j];
                            wrapper.categoryIndex = i;
                            wrapper.entryIndex = j;
                            entries.Add(wrapper);
                        }
                    }
                }
            }
            LogReportOnGatheredEntryWrappers(entries, "Gathered ALL Empty Entries IN THE PROJECT");
            return entries;
        }



        public void TranslateAllEmptyEntriesInTheProject()
        {
#if UNITY_EDITOR
            List<CurrentEntryWrapper> entries = GetAllEmptyEntries();
            foreach (CurrentEntryWrapper e in entries)
            {
                TranslateEntry(e.language, e.categoryIndex, e.entryIndex);
            }
#endif
        }



        public static void LogReportOnGatheredEntryWrappers(List<CurrentEntryWrapper> wrappers, string description)
        {
            if (wrappers.Count == 0)
            {
                Debug.Log("---NOTHING FOUND---");
                return;
            }

            List<string> report = new List<string>();
            report.Add(description);
            report.Add(wrappers.Count + " Entries");

            foreach (CurrentEntryWrapper c in wrappers)
            {
                report.Add(c.language + " " + c.currentEntry.name);
            }
            report.Add("");
            report.Add("END");
            report.Add("");
            Debug.Log(string.Join(System.Environment.NewLine, report.ToArray()));
        }




        public string GetEntry(int language, int category, int entry)
        {
            if (language >= languageBranches.Count)
            {
                Debug.LogError("Localization: GetEntry error: " + language);
            }

            if (category >= languageBranches[language].categories.Count)
            {
                Debug.LogError("Localization: GetEntry error: " + language + " " + category);
            }
            if (entry >= languageBranches[language].categories[category].currentEntries.Count)
            {
                Debug.LogError("Localization: GetEntry error: " + language + " " + category + " " + entry);
            }


            return languageBranches[language].categories[category].currentEntries[entry].autoTranslation;
        }

     


#if UNITY_EDITOR

        public void SetEntry(int language, int category, int entry, string value)
        {
            languageBranches[language].categories[category].currentEntries[entry].autoTranslation = value;
        }

        public void TranslateEntry(int lang, int cat, int entr)
        {
            if (lang == 0)
            {
                Debug.Log(sourceBranch.categories[cat].currentEntries[entr].name + " copied to english language");
                languageBranches[0].categories[cat].currentEntries[entr].autoTranslation = sourceBranch.categories[cat].currentEntries[entr].autoTranslation;
                return;
            }

// MAKE BETTER SOMEDAY
            GameObject.FindObjectOfType<Translate>().TranslateEntry(languageBranches[lang].categories[cat].currentEntries[entr].DefineValue, sourceBranch.categories[cat].currentEntries[entr].autoTranslation,lang, translationEngine);
        }




        public void TranslateEntry(string lang, int cat, int entr)
        {


            int indexOfLanguage = LocalizationCore.WhatIsTheIndexOfLanguage(lang);
            // Debug.Log("index of language: " + indexOfLanguage);
            if (indexOfLanguage == 0)
            {
                Debug.Log(sourceBranch.categories[cat].currentEntries[entr].name + " copied to english language");
                languageBranches[0].categories[cat].currentEntries[entr].autoTranslation = sourceBranch.categories[cat].currentEntries[entr].autoTranslation;
                //sourceBranch.categories[cat].currentEntries[entr].value = languageBranches[0].categories[cat].currentEntries[entr].value;
                return;
            }

            //translate.TranslateEntry(indexOfLanguage, cat, entr, translationEngine);
            GameObject.FindObjectOfType<Translate>().TranslateEntry(languageBranches[indexOfLanguage].categories[cat].currentEntries[entr].DefineValue, sourceBranch.categories[cat].currentEntries[entr].autoTranslation, indexOfLanguage, translationEngine);
        }

        public void ReverseTranslate(string language, string entryToTranslate)
        {
            string sourceCode = LocalizationCore.GetLanguage(language).yandexCode;
            string targetCode = "en";
            GameObject.FindObjectOfType<Translate>().ReverseTranslateQuery(sourceCode, targetCode, entryToTranslate);
        }


#endif

#if UNITY_EDITOR

        public void GenerateEnums()
        {
            if (enableOnlyCategoriesEnumGen)
            {
                List<string> categoryList2 = new List<string>();
                foreach (EntryList c in sourceBranch.categories) categoryList2.Add(c.name);
                EnumGenerator.GenerateEnums(categoryList2, nameOfCategoriesEnum, false);

                return;
            }

            if (!enableEnumGen) return;



            List<string> languageList = new List<string>();
            List<string> categoryList = new List<string>();
            foreach (EntryList c in sourceBranch.categories) categoryList.Add(c.name);
            foreach (Language l in LocalizationCore.GetAllLanguages()) languageList.Add(l.name);
            EnumGenerator.GenerateEnums(languageList, "LocalizationLanguagesEnum", false);
            EnumGenerator.GenerateEnums(categoryList, nameOfCategoriesEnum, false);
            foreach (EntryList cat in sourceBranch.categories)
            {
                string enumName = "LocEnum_" + cat.name.Replace(" ", "_");
                List<string> allNames = new List<string>();
                foreach (CurrentEntry ent in cat.currentEntries)
                {
                    allNames.Add(ent.name);
                }
                EnumGenerator.GenerateEnums(allNames, enumName, false);
            }

            // DICTIONARY
            List<string> allEnums = new List<string>();
            foreach (EntryList cat in sourceBranch.categories)
            {
                allEnums.Add("LocEnum_" + cat.name.Replace(" ", "_"));
            }
            EnumGenerator.GenerateLocalizationCategoryDictionary(categoryList, allEnums);
        }
        
#endif
        public bool AreNameRepetitionsInSouce(){
            // NEW!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            // CHECI IF THERE ARE ANY REPETITION OF NAMES IN ENTRIES
            List<string> catList=GetListOfCategories();
            List<string> names=new List<string>();
            foreach (string cat in catList)
            {
                List<CurrentEntry> entryList=GetSourceEntriesByCategory(catList.IndexOf(cat));
                foreach(CurrentEntry entry in entryList){
                    if(names.Contains(entry.name)){
                        Debug.LogError("Name: "+entry.name+" already exist. Change the name to avoid export issues!");
                        return true;
                    }else{
                        //Debug.Log("Name: "+entry.name+" already exist. Change the name to avoid export issues!");
                            names.Add(entry.name);
                    }
                }
            }
            return false;
        }

        [ContextMenu("Fill Missing")]
        public void FillMissingAndTrimUnusedEntries(bool fillSubcategories)
        {
#if UNITY_EDITOR
            


            // CREATE TREE IN ACCORDANCE TO LANGUAGE AND CATEGORIES

            foreach (Language lang in LocalizationCore.GetAllLanguages())
            {
                LanguageBranch treeOfAGivenType = languageBranches.FirstOrDefault(p => p.name == lang.name);
                if (treeOfAGivenType == null)
                {
                    treeOfAGivenType = new LanguageBranch();
                    treeOfAGivenType.name = lang.name;
                    treeOfAGivenType.categories = new List<EntryList>();



                    languageBranches.Add(treeOfAGivenType);
                }
                // IF count of categories is smaller then the amount of categories we expect

                if (treeOfAGivenType.categories.Count <= sourceBranch.categories.Count)
                {
                    // TODO: maybe add deletion some day
                }


                // CHECKING FOR CATEGORIES TO ADD

                for (int i = 0; i < sourceBranch.categories.Count; i++)
                {

                    if (i >= treeOfAGivenType.categories.Count)
                    {
                        Debug.Log("New Category is being propagated: \"" + sourceBranch.categories[i].name + "\"");
                        EntryList cat = new EntryList();
                        cat.currentEntries = new List<CurrentEntry>();
                        treeOfAGivenType.categories.Add(cat);
                    }

                    treeOfAGivenType.categories[i].name = sourceBranch.categories[i].name;
                }

                // IF ANY CATEGORY IS NULL

                foreach (EntryList cat in sourceBranch.categories)
                {
                    EntryList categoryOfAGivenType = treeOfAGivenType.categories.FirstOrDefault(p => p.name == cat.name);

                    if (categoryOfAGivenType == null)
                    {
                        categoryOfAGivenType = new EntryList();
                        categoryOfAGivenType.name = cat.name;
                        categoryOfAGivenType.currentEntries = new List<CurrentEntry>();
                        treeOfAGivenType.categories.Add(categoryOfAGivenType);
                    }


                }
                // MAKE SURE AMOUNT OF ENTRIES ALWAYS IS THE SAME AS IN SOURCE BRANCH
                for (int i = 0; i < sourceBranch.categories.Count; i++)
                {

                    int amountOfEntires = sourceBranch.categories[i].currentEntries.Count();

                    // MAKE SURE TO REMOVE REDUNDANT ENTRIES
                    if (treeOfAGivenType.categories[i].currentEntries.Count > amountOfEntires)
                    {
                        List<CurrentEntry> entriesToDelete = treeOfAGivenType.categories[i].currentEntries.Except(sourceBranch.categories[i].currentEntries).ToList();
                        foreach (CurrentEntry e in entriesToDelete)
                        {
                            treeOfAGivenType.categories[i].currentEntries.Remove(e);
                            Debug.Log("Deleting entry: " + e.name + " in category " + i + " in language " + lang);
                        }
                    }


                    // MAKE SURE TO ADD NEW ENTRIES
                    while (treeOfAGivenType.categories[i].currentEntries.Count < amountOfEntires)
                    {
                        treeOfAGivenType.categories[i].currentEntries.Add(new CurrentEntry());
                        Debug.Log("Created entry in category " + i + " in language " + lang);
                    }


                }
                

                EditorUtility.SetDirty(treeOfAGivenType);


            }



            // FILL RESPECTIVE ENTRIES WITH CORRECT NAME LABELS (USED FOR ENUM NAMES)
            // ALSO GENERATE DIALOG SUBENTRIES MAYBE
            foreach (LanguageBranch trunk in languageBranches)
            {
                for (int i = 0; i < sourceBranch.categories.Count; i++)
                {
                    int amountOfEntires = sourceBranch.categories[i].currentEntries.Count();
                    for (int j = 0; j < amountOfEntires; j++)
                    {
                        trunk.categories[i].currentEntries[j].name = sourceBranch.categories[i].currentEntries[j].name;

                    }
                }
            }
#endif
        }
        /// <summary>
        /// This encapsulates all the stuff we want to do when adding new entries to source
        /// </summary>
        public void Propagate(bool dialogues)
        {
#if UNITY_EDITOR
            if(!AreNameRepetitionsInSouce()){
                FillMissingAndTrimUnusedEntries(dialogues);
                GenerateEnums();
                TranslateAllEmptyEntriesInTheProject();
                AssetDatabase.SaveAssets();
            

            AssetDatabase.SaveAssets();
            }
            //FillMissingAndTrimUnusedEntries(dialogues);
            //GenerateEnums();
            //TranslateAllEmptyEntriesInTheProject();

            //AssetDatabase.SaveAssets();
#endif

        }

        // ----------------------------- GOOGLE SHEETS
#if GoogleFairy
        public void UpdateLanguages(){
            Debug.Log("UpdateLanguages");

            Drive.responseCallback += HandleDriveResponse;
            int langCount=languageBranches.Count;
            string[] fieldNames = new string[3+langCount];
                fieldNames[0] = "Category";
                fieldNames[1] = "Name";
                fieldNames[2] = "Source";
            for(int i=0;i<langCount;i++){
                fieldNames[3+i] = languageBranches[i].name.ToString();
            }
            Drive.UpdateColumns(fieldNames,tableName, false);
        }
        public void Export()
        {
            Debug.Log("Google sheets export");
            Drive.responseCallback += HandleDriveResponse;
            List<LocalizationCloudEntry> localizationCloudEntries=new List<LocalizationCloudEntry>();
            List<string> catList=GetListOfCategories();
            foreach(string cat in catList){
                //Debug.Log(cat);
                List<CurrentEntry> entryList=GetSourceEntriesByCategory(catList.IndexOf(cat));
                foreach(CurrentEntry entry in entryList){
                    //Debug.Log(entry);
                    if(!entry.disableForHumanTranslation){
                        LocalizationCloudEntry locEntry=new LocalizationCloudEntry();
                        locEntry.Category=cat;
                        locEntry.Name=entry.name;
                        locEntry.Source=entry.autoTranslation;
                        localizationCloudEntries.Add(locEntry);
                    }
                }
            }
            string jsonClound=JsonConvert.SerializeObject(localizationCloudEntries);
            //Debug.Log(jsonClound);
            BraveNewJson.FormatForCloud(ref jsonClound);
            Debug.Log(jsonClound);
            Drive.UpdateLocalization(jsonClound, tableName, false);
        }

        public void CreateSpreadsheet(){
            Debug.Log("CreateSpreadsheet");

            Drive.responseCallback += HandleDriveResponse;
            int langCount=languageBranches.Count;
            string[] fieldNames = new string[3+langCount];
                fieldNames[0] = "Category";
                fieldNames[1] = "Name";
                fieldNames[2] = "Source";
            for(int i=0;i<langCount;i++){
                fieldNames[3+i] = languageBranches[i].name.ToString();
            }
            Drive.CreateTable(fieldNames,tableName, false);
        }

        public void Import()
        {
            Debug.Log("Google sheets import");
            Drive.responseCallback += HandleDriveResponse;
            Drive.GetTable(tableName, false);
        }

        private void HandleDriveResponse(Drive.DataContainer dataContainer)
        {
            if (dataContainer.objType != tableName)
                return;
    
            if (dataContainer.QueryType == Drive.QueryType.getTable)
            {
                string rawJSon = dataContainer.payload;
                //Debug.Log("Data from Google Drive received.");
                //Debug.Log(rawJSon);
                var parsedJSon=JSON.Parse(rawJSon);
                StoreHumanTranslation(parsedJSon);
                Debug.Log("Human translations imported");     
            }
            if(dataContainer.QueryType == Drive.QueryType.getUpdateDate)
            {
                string rawJSon = dataContainer.value;
                //Debug.Log("Update date: "+rawJSon);
                //if(String.Compare(rawJSon,source.lastUpdate)!=0){
                //    source.lastUpdate=rawJSon;
                //    bool runtime=false;
                //    if(Application.isPlaying){
                //        runtime=true;
                //    }
                //    ImportFromSpreadsheet(runtime);
                //    Debug.Log("Dialogues has been updated");
                //}else{
                //    Debug.Log("Dialogues up-to-date");
                //}
                
            }

            if (dataContainer.QueryType != Drive.QueryType.createTable || dataContainer.QueryType != Drive.QueryType.createObjects || dataContainer.QueryType != Drive.QueryType.getUpdateDate)
            {
                //Debug.Log(dataContainer.msg);
            }

            Drive.responseCallback -= HandleDriveResponse;
        }
#endif
        private void StoreHumanTranslation(JSONNode parsedJSon){
            
            List<string> keys = new List<string>();
            
            int index=0;
            foreach(var field in parsedJSon){
                if(index==0){
                    var pj=JSON.Parse(field.Value.ToString());
                    foreach(var fld in pj){
                        keys.Add(fld.Key);
                    }
                    break;
                }
            }

            foreach(LanguageBranch languageBranch in languageBranches){
                for(int i=3;i<keys.Count;i++){
                    if(String.Compare(languageBranch.name,keys[i])==0){
                        foreach(EntryList entryList in languageBranch.categories){
                            foreach(CurrentEntry entry in entryList.currentEntries){
                                foreach(var field in parsedJSon){
                                    //Debug.Log(field.Value);
                                    if(String.Compare(entry.name,field.Value[1])==0){
                                        //Debug.Log(field.Value[i]);
                                        if(String.Compare(field.Value[2],"Disabled for translation")==0){
                                            entry.disableForHumanTranslation=true;
                                        }else{
                                            entry.humanTranslation=field.Value[i];
                                        }
                                    }
                                }
                            }
                        }    
                    }
                } 
            }
            
        }

        // ----------------------------- SAVE / LOAD

        public void Save()
        {
            Presave();
            foreach (LanguageBranch trunk in languageBranches)
            {
                string finalPath ="unused";
                if (trunk.associatedTextAsset != null)
                {
                    finalPath = Path.Combine(saveFolderName, trunk.associatedTextAsset.name) + ".txt";
                }

#if UNITY_EDITOR
                EditorUtility.SetDirty(trunk);
#endif
                if (trunk.associatedTextAsset)
                {
                    Debug.Log("tree: " + trunk.name + "  " + finalPath);
                }
                    
                SaveLocalizationFile(trunk, finalPath);
            }
#if UNITY_EDITOR
            AssetDatabase.SaveAssets();
#endif

        }

        public void Presave()
        {
            if (!Directory.Exists(saveFolderName))
            {
                Directory.CreateDirectory(saveFolderName);
                Debug.Log("Created Directory: " + saveFolderName);

            }
        }
        public void SaveSpecific(string languege)
        {

            int index = languageBranches.IndexOf(languageBranches.FirstOrDefault(p => string.CompareOrdinal(p.associatedTextAsset.name, languege) == 0));
            Presave();
            string finalPath = Path.Combine(saveFolderName, languege) + ".txt";
            Debug.Log("tree: " + languege + "  " + finalPath + " index: " + index + ", current trees count: " + languageBranches.Count);
            SaveLocalizationFile(languageBranches[index], finalPath);
        }

        public void SaveSpecificToHeldTextAsset(LanguageBranch languageBranch)
        {
#if UNITY_EDITOR
            string path = AssetDatabase.GetAssetPath(languageBranch.associatedTextAsset);
            Debug.Log(path);

            //return;
            using (StreamWriter outFile =
                  new StreamWriter(path))
            {
                foreach (EntryList cat in languageBranch.categories)
                {
                    string brackets = ("[{0}]");
                    outFile.WriteLine(string.Format(brackets, cat.name));
                    outFile.WriteLine("");
                    foreach (CurrentEntry entry in cat.currentEntries)
                    {
                        outFile.WriteLine(entry.name + " = " + entry.autoTranslation );
                    }
                    outFile.WriteLine("");
                }
            }

#endif
        }
        public void SaveLocalizationFile(LanguageBranch currentTree, string genPath)
        {

            using (StreamWriter outFile =
                   new StreamWriter(genPath))
            {
                foreach (EntryList cat in currentTree.categories)
                {
                    string brackets = ("[{0}]");
                    outFile.WriteLine(string.Format(brackets, cat.name));
                    outFile.WriteLine("");
                    foreach (CurrentEntry entry in cat.currentEntries)
                    {
                        outFile.WriteLine(entry.name + " = " + entry.autoTranslation);// + (entry.helperText != null && entry.helperText.Length > 0 ? ("###"+ entry.helperText) : ""));
                    }
                    outFile.WriteLine("");
                }
            }

#if UNITY_EDITOR

            currentTree.associatedTextAsset = (TextAsset)AssetDatabase.LoadAssetAtPath(genPath, typeof(TextAsset));
#endif

        }

        public void Load()
        {
            for (int i = 0; i < languageBranches.Count; i++)
            {
                LoadSingle(i);
            }

        }


        public void LoadSingle(int i)
        {
            string finalPath = Path.Combine(saveFolderName, languageBranches[i].associatedTextAsset.name) + ".txt";
            List<string> fullSource = LocalizationUtils.GetRawStringListFromLocFile(languageBranches[i].associatedTextAsset);
            LocalizationUtils.RemoveEmptyEntriesInStringList(ref fullSource);
            LocalizationUtils.PharseForLocalizationBranch(fullSource, out languageBranches[i].categories, sourceBranch.categories);

        }







    }

    [Serializable]
    public class LocalizationCloudEntry{
        public string Category;
        public string Name;
        public string Source;
    }

}