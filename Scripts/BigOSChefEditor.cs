using UnityEngine;
using UnityEditor;
using UnityEditor.PackageManager.Requests;
using UnityEditor.PackageManager;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using BigOS;
using BigOS.SaveSystem;
using BigOS.Localization;
using BigOS.Localization.DialogueSystem;

[CustomEditor(typeof(BigOSChef))]
public class BigOSChefEditor : Editor
{
    //public SaveInitializeData initializeData;
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        BigOSChef t = (BigOSChef)target;
        
        GUILayout.BeginVertical("BigOS Core", "window");
        //Save system inspector region
            GUILayout.BeginHorizontal("box");
            GUILayout.BeginVertical();
            GUILayout.Label("SAVE SYSTEM");
            
            t.saveSystemCore.rijndaelEnabled=EditorGUILayout.Toggle("Rijndael Enabled",t.saveSystemCore.rijndaelEnabled);
            t.saveSystemCore.savedInFolder=EditorGUILayout.TextField("Saved in Folder",t.saveSystemCore.savedInFolder);
            t.saveSystemCore.tagOfObjectsWithPersistence=EditorGUILayout.TextField("Tag of Objects with Persistence",t.saveSystemCore.tagOfObjectsWithPersistence);
            t.saveSystemCore.initializeData=(SaveInitializeData)EditorGUILayout.ObjectField("Initialize Data",t.saveSystemCore.initializeData,typeof(SaveInitializeData),true);
            //t.saveSystemCore.diagnosticsScreen=(DiagnosticsScreen)EditorGUILayout.ObjectField("Initialize Data",t.saveSystemCore.diagnosticsScreen,typeof(DiagnosticsScreen),true);
            //t.saveSystemCore.currentSlot=EditorGUILayout.PropertyField(t.saveSystemCore.currentSlot,true);
            
            GUILayout.BeginHorizontal();
                if (GUILayout.Button("SAVE SLOT"))
                {
                    t.saveSystemCore.SaveSlot();
                }
                if (GUILayout.Button("LOAD SLOT"))
                {
                    t.saveSystemCore.LoadCurrentSlot();
                }
                if (GUILayout.Button("CLEAR SLOT"))
                {
                    t.saveSystemCore.ClearSlot();
                }
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
                if (GUILayout.Button("SELECT SLOT 0 AND ALLOW SAVING"))
                {
                    SaveSystemCore.SaveAllowed = true;
                }
                if (GUILayout.Button("GOTO SAVE FOLDER"))
                {
                    EditorUtility.RevealInFinder(t.saveSystemCore.GetPath());
                }
            GUILayout.EndHorizontal();
            
            
            GUILayout.EndVertical();
            GUILayout.EndHorizontal();
        //Localization inspector region
            GUILayout.BeginHorizontal("box");
            GUILayout.BeginVertical();
            GUILayout.Label("LOCALIZATION");
                t.localizationCore.selectedLanguage=EditorGUILayout.IntField("Selected Language",t.localizationCore.selectedLanguage);
                t.localizationCore.availableLanguages=(AvailableLanguages)EditorGUILayout.ObjectField("Available Languages",t.localizationCore.availableLanguages,typeof(AvailableLanguages),true);
                t.localizationCore.vocabularyCore=(VocabularyCore)EditorGUILayout.ObjectField("Vocabulary Core",t.localizationCore.vocabularyCore,typeof(VocabularyCore),true);
                t.localizationCore.dialogueCore=(DialogueCore)EditorGUILayout.ObjectField("Dialogue Core",t.localizationCore.dialogueCore,typeof(DialogueCore),true);
            GUILayout.EndVertical();
            GUILayout.EndHorizontal();

        //UI Animator inspector region
            GUILayout.BeginHorizontal("box");
            GUILayout.BeginVertical();
            GUILayout.Label("UI Animator - IN DEVELOPMENT");
            
            GUILayout.EndVertical();
            GUILayout.EndHorizontal();

        GUILayout.EndVertical();

        GUILayout.BeginVertical("BigOS Addons", "window");

        GUILayout.BeginHorizontal();
        GUILayout.Label("Name",GUILayout.MaxWidth(150));
        GUILayout.Label("Status");
        GUILayout.EndHorizontal();
        
        if(Type.GetType("BigOS.UnityTweety.UnityTweetyCore, rympow.unitytweety",false)!=null){
            GUILayout.BeginHorizontal("box");
            GUILayout.Label("UnityTweety",GUILayout.MaxWidth(150));
#if UnityTweety
            GUILayout.Label("OK");
#else
            if (GUILayout.Button("Connect"))
            {
                AddDefineSymbol("UnityTweety");
                t.CreateExtensionGameObject("UnityTweety");
            }
#endif
            GUILayout.EndHorizontal();
        }

        if(Type.GetType("BigOS.Paparazzi.PaparazziManager, rympow.paparazzi",false)!=null){
            GUILayout.BeginHorizontal("box");
            GUILayout.Label("Paparazzi",GUILayout.MaxWidth(150));
#if Paparazzi
            GUILayout.Label("OK");
#else
            if (GUILayout.Button("Connect"))
            {
                AddDefineSymbol("Paparazzi");
                t.CreateExtensionGameObject("Paparazzi");
            }
#endif
            GUILayout.EndHorizontal();
        }
        if(Type.GetType("BigOS.GoogleFairy.GoogleFairyCore, rympow.googlefairy",false)!=null){
            GUILayout.BeginHorizontal("box");
            GUILayout.Label("GoogleFairy",GUILayout.MaxWidth(150));
#if GoogleFairy
            GUILayout.Label("OK");
#else
            if (GUILayout.Button("Connect"))
            {
                AddDefineSymbol("GoogleFairy");
                t.CreateExtensionGameObject("GoogleFairy");
            }
#endif
            GUILayout.EndHorizontal();
        }
        
        GUILayout.EndVertical();
        if(t.saveSystemCore.initializeData!=null)EditorUtility.SetDirty(t.saveSystemCore.initializeData);
    }
    
    private void AddDefineSymbol(string symbol){
        string definesString = PlayerSettings.GetScriptingDefineSymbolsForGroup ( EditorUserBuildSettings.selectedBuildTargetGroup );
        List<string> allDefines = definesString.Split ( ';' ).ToList ();
        allDefines.Add(symbol);
        PlayerSettings.SetScriptingDefineSymbolsForGroup (
             EditorUserBuildSettings.selectedBuildTargetGroup,
             string.Join ( ";", allDefines.ToArray () ) );
        
    }
}
